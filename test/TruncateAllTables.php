<?php
	$OLD_DB_ID = 70;	//20~22, 50~52, 70~72, 80~82

	$val = $_SERVER['argv'];
	if ( count($val) !== 2 ) {
        echo "20~22, 50~52, 70~72, 80~82 입력\n";
        return;
    }

    $OLD_DB_ID = (int)$val[1];
    if ( 
    	!(
    		($OLD_DB_ID >= 20 && $OLD_DB_ID <= 22) 
    		|| ($OLD_DB_ID >= 50 && $OLD_DB_ID <= 52)
    		|| ($OLD_DB_ID >= 70 && $OLD_DB_ID <= 72)
    		|| ($OLD_DB_ID >= 80 && $OLD_DB_ID <= 82)
    	)
    ) {
        echo "20~22, 50~52, 70~72, 80~82 입력\n";
        return;
    }

    $LEGACY_DBNAME = "ddookdak".$OLD_DB_ID;

	
	function ProcessQuery($newPDO, &$processPointer, $query) {
		$result = $newPDO->query($query);
		$processPointer++;
		if ( $result == false ) {
			echo "Error!!\n".$query."\n".$processPointer;
		    return false;
		}
		echo "Process ".$processPointer." Comp\n";
		return true;
	}
	switch ($OLD_DB_ID) {
		case 20:case 21:case 22:
		case 70:case 71:case 72:
			$host = "mysql:host=172.27.100.3;";
			break;
		
		default:
			$host = "mysql:host=172.27.100.4;";
			break;
	}
	$PLATFORM_ID = $OLD_DB_ID%10;
	$legacyPDO = 	new PDO($host."dbname=".$LEGACY_DBNAME.";charset=utf8", "root", "eoqkrskwk12", array(PDO::ATTR_PERSISTENT => true));
	$newPDO = 		new PDO($host."dbname=hrd_db_game_".$OLD_DB_ID.";charset=utf8", "root", "eoqkrskwk12", array(PDO::ATTR_PERSISTENT => true));
	$processPointer = 0;





	$datas = array();

	$datas[] = array(
		"TRUNCATE TABLE alphaCoupon",
	);

	$datas[] = array(
		"TRUNCATE TABLE Event_7_5_5",
	);

	$datas[] = array(
		"TRUNCATE TABLE frdAbillity",
	);

	$datas[] = array(
		"TRUNCATE TABLE frdCharInfo",
	);

	$datas[] = array(
		"TRUNCATE TABLE frdClearCount",
	);

	$datas[] = array(
		"TRUNCATE TABLE frdCharInfo",
	);

	$datas[] = array(
		"TRUNCATE TABLE frdClimbTopData",
	);

	$datas[] = array(
		"TRUNCATE TABLE frdEffectForEtc",
	);

	$datas[] = array(
		"TRUNCATE TABLE frdEffectForEtcSP",
	);

	$datas[] = array(
		"TRUNCATE TABLE frdEffectForPreCheckRewards",
	);

	$datas[] = array(
		"TRUNCATE TABLE frdEffectForRewards",
	);

	$datas[] = array(
		"TRUNCATE TABLE frdEventAttend_JDK",
	);

	$datas[] = array(
		"TRUNCATE TABLE frdGoddess",
	);

	$datas[] = array(
		"TRUNCATE TABLE frdHavingItems",
	);

	$datas[] = array(
		"TRUNCATE TABLE frdHavingMagics",
	);

	$datas[] = array(
		"TRUNCATE TABLE frdHavingWeapons",
	);

	$datas[] = array(
		"TRUNCATE TABLE frdID",
	);

	$datas[] = array(
		"TRUNCATE TABLE frdLogBuy",
	);

	$datas[] = array(
		"TRUNCATE TABLE frdLogPlay",
	);

	$datas[] = array(
		"TRUNCATE TABLE frdMonthlyCard",
	);

	$datas[] = array(
		"TRUNCATE TABLE frdSkillLevels",
	);

	$datas[] = array(
		"TRUNCATE TABLE frdSkillLevels_back",
	);

	$datas[] = array(
		"TRUNCATE TABLE frdTemple",
	);

	$datas[] = array(
		"TRUNCATE TABLE frdTempleClearCount",
	);

	$datas[] = array(
		"TRUNCATE TABLE frdUserData",
	);

	$datas[] = array(
		"TRUNCATE TABLE frdUserPost",
	);

	$datas[] = array(
		"TRUNCATE TABLE frdEquipWeapons",
	);

	$datas[] = array(
		"TRUNCATE TABLE frdEquipMagics",
	);
	



	$len = count( $datas );
	for ( $i=0; $i<$len; $i++ ) {
		$data = $datas[$i];

		$newPDO->query($data[0]);
	}

	


	echo "\nComplete!\n";

?>
