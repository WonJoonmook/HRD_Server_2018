<?php
	require_once './updateResources.php';
	
	$data = array();
	PrepareGetResources		($data, 5000, 3000, 0.5);				//50%의 확률로 골드 3000 획득
	PrepareConsumeResources	($data, 5000, 450);						//골드 450 소모
	PrepareGetResources		($data, 5004, 20, 	0.5);				//50%의 확률로 보석 20 획득
	PrepareGetResources		($data, 5000, 7000, 0.35);				//35%의 확률로 골드 7000 획득

	PrepareAdd_ResourcesGet_PercentBonus($data, 5000, 0.2, 0.7);	//70%의 확률로 골드 획득 확률 +20% 		( 가연산 )
	PrepareMul_ResourcesGet_PercentBonus($data, 5000, 0.4, 0.6);	//60%의 확률로 골드 획득 확률 40% 증가 	( 곱연산 )

	PrepareAdd_ResourcesGet_CountBonus	($data, 5004, 10,  0.5);	//50%의 확률로 보석 획득량 +10	 		( 가연산 )
	PrepareMul_ResourcesGet_CountBonus	($data, 5000, 0.3, 0.4);	//40%의 확률로 골드 획득량 30% 증가 		( 곱연산 )




	$returnDatas = Process($data);									//획득 확률, 획득량 증가 등 계산 후 결과 산출
																	//$returnDatas['consume']
																	//$returnDatas['get']


	echo "\n---Result---\n";
	if ( array_key_exists("consume", $returnDatas) ) {
		echo "Consume -\n";
		while($pair = each($returnDatas["consume"]))
			echo $pair[0].", ".$pair[1]."\n";
		echo "\n";
	}
	if ( array_key_exists("get", $returnDatas) ) {
		echo "Get -\n";
		while($pair = each($returnDatas["get"]))
			echo $pair[0].", ".$pair[1]."\n";
	}
?>