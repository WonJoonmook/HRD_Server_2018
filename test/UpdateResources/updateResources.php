<?php

	function PrepareConsumeResources( &$datas, $resourceId, $resourceCount ) {
		if ( !array_key_exists("consume", $datas) )
			$datas["consume"] = array();

		if ( array_key_exists($resourceId, $datas["consume"]) )
			$datas["consume"][$resourceId] += $resourceCount;
		else
			$datas["consume"][$resourceId] = $resourceCount;
	}

	function PrepareGetResources( &$datas, $resourceId, $resourceCount, $getPercent ) {
		if ( !array_key_exists("get", $datas) )
			$datas["get"] = array();

		$datas["get"][$resourceId][] = array($getPercent, $resourceCount);
	}

	function PrepareAdd_ResourcesGet_PercentBonus( &$datas, $resourceId, $val, $applyPercent ) {
		if ( !array_key_exists("addBonusGetPer", $datas) )
			$datas["addBonusGetPer"] = array();

		$datas["addBonusGetPer"][$resourceId][] = array($applyPercent, $val);
	}

	function PrepareMul_ResourcesGet_PercentBonus( &$datas, $resourceId, $val, $applyPercent ) {
		if ( !array_key_exists("mulBonusGetPer", $datas) )
			$datas["mulBonusGetPer"] = array();

		$datas["mulBonusGetPer"][$resourceId][] = array($applyPercent, $val);
	}

	function PrepareAdd_ResourcesGet_CountBonus( &$datas, $resourceId, $val, $applyPercent ) {
		if ( !array_key_exists("addBonusGetCnt", $datas) )
			$datas["addBonusGetCnt"] = array();

		$datas["addBonusGetCnt"][$resourceId][] = array($applyPercent, $val);
	}

	function PrepareMul_ResourcesGet_CountBonus( &$datas, $resourceId, $val, $applyPercent ) {
		if ( !array_key_exists("mulBonusGetCnt", $datas) )
			$datas["mulBonusGetCnt"] = array();

		$datas["mulBonusGetCnt"][$resourceId][] = array($applyPercent, $val);
	}


	function ProcessBonus(&$datas, $keyName) {
		if ( array_key_exists($keyName, $datas) ) {
			$addBonuses = array();

			while($pair = each($datas[$keyName])) {
				$resourceName = $pair[0];
				$tempArr = $datas[$keyName][$resourceName];
				$count = count($tempArr);
				for ( $j=0; $j<$count; $j++ ) {
					$getPercent = $tempArr[$j][0];
					$val = $tempArr[$j][1];
					if ( rand(0,1000) < $getPercent*1000 ) {
						if ( array_key_exists($resourceName, $addBonuses) )
							$addBonuses[$resourceName] += $val;
						else
							$addBonuses[$resourceName] = $val;
					}
				}
			}

			if ( array_key_exists("get", $datas) ) {

				while($pair = each($addBonuses)) {
					$resourceName = $pair[0];
					$idx = array_search($resourceName, $datas["get"]);
					if ( $idx >= 0 ) {
						$tempArr = $datas["get"][$resourceName];
						$count = count($tempArr);
						for ( $j=0; $j<$count; $j++ ) {
							switch ($keyName) {
								case 'addBonusGetPer':
									$datas["get"][$resourceName][$j][0] += $pair[1];
									break;
								
								case 'mulBonusGetPer':
									$datas["get"][$resourceName][$j][0] *= (1+$pair[1]);
									break;

								case 'addBonusGetCnt':
									$datas["get"][$resourceName][$j][1] += $pair[1];
									break;

								case 'mulBonusGetCnt':
									$datas["get"][$resourceName][$j][1] *= (1+$pair[1]);
									break;
								default:
									break;
							}
							
						}
					}
				}

			}
		}
	}

	function Process( &$datas ) {
		
		$returnDatas = array();
		ProcessBonus($datas, "addBonusGetPer");
		ProcessBonus($datas, "mulBonusGetPer");
		ProcessBonus($datas, "addBonusGetCnt");
		ProcessBonus($datas, "mulBonusGetCnt");

		if ( array_key_exists("get", $datas) ) {
			$getDatas = array();
			while($pair = each($datas["get"])) {
				$resourceName = $pair[0];
				$tempArr = $datas["get"][$resourceName];
				$count = count($tempArr);
				for ( $j=0; $j<$count; $j++ ) {
					$getPercent = $tempArr[$j][0];
					$getCount = $tempArr[$j][1];
					echo $resourceName." Get Percent = ".$getPercent.", Get Count = ".$getCount."\n";
					if ( rand(0,1000) < $getPercent*1000 ) {
						if ( array_key_exists($resourceName, $getDatas) )
							$getDatas[$resourceName] += $getCount;
						else
							$getDatas[$resourceName] = $getCount;
					}
				}
			}
			$returnDatas["get"] = $getDatas;
		}

		if ( array_key_exists("consume", $datas) ) {
			$returnDatas["consume"] = $datas["consume"];
		}
		return $returnDatas;
	}
?>