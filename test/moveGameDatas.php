<?php
	$OLD_DB_ID = 70;	//20~22, 50~52, 70~72, 80~82

	$val = $_SERVER['argv'];
	if ( count($val) !== 2 ) {
        echo "20~22, 50~52, 70~72, 80~82 입력\n";
        return;
    }

    $OLD_DB_ID = (int)$val[1];
    if ( 
    	!(
    		($OLD_DB_ID >= 20 && $OLD_DB_ID <= 22) 
    		|| ($OLD_DB_ID >= 50 && $OLD_DB_ID <= 52)
    		|| ($OLD_DB_ID >= 70 && $OLD_DB_ID <= 72)
    		|| ($OLD_DB_ID >= 80 && $OLD_DB_ID <= 82)
    	)
    ) {
        echo "20~22, 50~52, 70~72, 80~82 입력\n";
        return;
    }

    $LEGACY_DBNAME = "ddookdak".$OLD_DB_ID;
    
	echo date("Y-m-d H:i:s")."\n";
	
	function ProcessQuery($newPDO, &$processPointer, $query) {
		$result = $newPDO->query($query);
		$processPointer++;
		if ( $result == false ) {
			echo "Error!!\n".$query."\n".$processPointer;
		    return false;
		}
		echo "Process ".$processPointer." Comp\n";
		return true;
	}
	switch ($OLD_DB_ID) {
		case 20:case 21:case 22:
		case 70:case 71:case 72:
			$host = "mysql:host=172.27.100.3;";
			break;
		
		default:
			$host = "mysql:host=172.27.100.4;";
			break;
	}
	$PLATFORM_ID = $OLD_DB_ID%10;
	$legacyPDO = 	new PDO($host."dbname=".$LEGACY_DBNAME.";charset=utf8", "root", "eoqkrskwk12", array(PDO::ATTR_PERSISTENT => true));
	$newPDO = 		new PDO($host."dbname=hrd_db_game_".$OLD_DB_ID.";charset=utf8", "root", "eoqkrskwk12", array(PDO::ATTR_PERSISTENT => true));
	$processPointer = 0;





	$datas = array();

	$datas[] = array(
		"Event_7_5_5",
		"userId, dailySeconds, totalSeconds",
	);

	$datas[] = array(
		"frdAbillity",
		"userId,openedSlot,cantUnlockSlot,bundle0,bundle1"
	);

	for ( $i=0; $i<8; $i++ ) {
		$datas[] = array(
			"frdCharInfo",	//hrd_db_game_20
			"userId, charId, exp",
			"frdCharacEvolveExps",
			"userId, bundleIdx*8 + $i, (exps >> ( 8*$i ))&255",
		);
	}

	$datas[] = array(
		"frdClearCount",
		"userId, stageId, clearCount",
	);

	$datas[] = array(
		"frdClimbTopData",
		"userId, bestRank, bestTotalUserCount, playCount, averageRank, avrgTotalUserCount, lastRank, lastTotalUserCount",
	);

	// $datas[] = array(
	// 	"frdEffectForEtc",
	// 	"userId, type, val",
	// );

	// $datas[] = array(
	// 	"frdEffectForPreCheckRewards",
	// 	"userId, type, subType, val, isSuccess",
	// );

	// $datas[] = array(
	// 	"frdEffectForRewards",
	// 	"userId, type, val",
	// );

	$datas[] = array(
		"frdEventAttend_JDK",
		"userId, pocketCount, last_login_date",
		"frdEventAttend_JDK",
		"userId, pocketCount, FROM_UNIXTIME(last_login_date)",
	);

	$datas[] = array(
		"frdGoddess",
		"userId, goddessId, level, exp",
	);

	$datas[] = array(
		"frdHavingItems",
		"userId, itemId, itemCount",
	);

	$datas[] = array(
		"frdHavingMagics",
		"userId, magicId, level, exp",
		"frdHavingArtifacts",
		"userId, itemId, itemLevel, itemExp",
		"where ".$LEGACY_DBNAME.".frdHavingArtifacts.itemId<600"
	);

	$datas[] = array(
		"frdHavingWeapons",	//hrd_db_game_20
		"userId, weaponId, level, exp, statId0, statId1, statId2, statId3, statVal0, statVal1, statVal2, statVal3",
		"frdHavingWeapons",
		($PLATFORM_ID == 0)
		?
		"
			userId, 
			weaponId, 
			".$LEGACY_DBNAME.".weaponExps.level,
			".$LEGACY_DBNAME.".frdHavingWeapons.exp - ".$LEGACY_DBNAME.".weaponLevels.exp,
			weaponVals.statId0, 
			weaponVals.statId1, 
			weaponVals.statId2, 
			weaponVals.statId3, 
			(weaponVals.statVal0/40), 
			(weaponVals.statVal1/40), 
			(weaponVals.statVal2/40), 
			(weaponVals.statVal3/40)
		"
		:
		"
			userId, 
			weaponId, 
			".$LEGACY_DBNAME.".weaponExps.level,
			".$LEGACY_DBNAME.".frdHavingWeapons.exp - ".$LEGACY_DBNAME.".weaponLevels.exp,
			statId0, statId1, statId2, statId3, statVal0, statVal1, statVal2, statVal3
		"
		,
		($PLATFORM_ID == 0)
		?
		"
			INNER JOIN ".$LEGACY_DBNAME.".weaponVals
			ON ".$LEGACY_DBNAME.".frdHavingWeapons.stat = ".$LEGACY_DBNAME.".weaponVals.preStat
					
			INNER JOIN ".$LEGACY_DBNAME.".weaponExps
			ON frdHavingWeapons.exp = ".$LEGACY_DBNAME.".weaponExps.exp	

			INNER JOIN ".$LEGACY_DBNAME.".weaponLevels
			ON ".$LEGACY_DBNAME.".weaponExps.level = ".$LEGACY_DBNAME.".weaponLevels.level

			where ".$LEGACY_DBNAME.".frdHavingWeapons.weaponId<390 or ".$LEGACY_DBNAME.".frdHavingWeapons.weaponId>=400
		"
		:
		"
			INNER JOIN ".$LEGACY_DBNAME.".weaponExps
			ON frdHavingWeapons.exp = ".$LEGACY_DBNAME.".weaponExps.exp

			INNER JOIN ".$LEGACY_DBNAME.".weaponLevels
			ON ".$LEGACY_DBNAME.".weaponExps.level = ".$LEGACY_DBNAME.".weaponLevels.level

			where ".$LEGACY_DBNAME.".frdHavingWeapons.weaponId<390 or ".$LEGACY_DBNAME.".frdHavingWeapons.weaponId>=400
		"
	);

	// $datas[] = array(
	// 	"frdID",
	// 	"privateId, googleId",
	// );

	$datas[] = array(
		"frdLogBuy",
		"userId, packageName, orderId, productId, purchaseTime, purchaseState, purchaseToken, signature, isComplete",
		"frdLogBuy",
		"id, packageName, orderId, productId, purchaseTime, purchaseState, purchaseToken, signature, isComplete",
	);

	$datas[] = array(
		"frdLogPlay",
		"userId, stageId, totalWave, totalSecs, playCount, ticketCount",
		"frdLogPlay",
		"id, stageId, totalWave, totalSecs, playCount, ticketCount",
	);

	$datas[] = array(
		"frdMonthlyCard",
		"userId, startTime, recvCnt, attendDate, attendCount, duration",
		"frdMonthlyCard",
		"
			frdMonthlyCard.userId, 
			FROM_UNIXTIME(frdMonthlyCard.startTime), 
			frdMonthlyCard.lastRecvDay,
			FROM_UNIXTIME(frdUserData.checkAttendanceVal), 
			frdUserData.checkAttendancePointer, 
			frdMonthlyCard.duration
		",
		" INNER JOIN ".$LEGACY_DBNAME.".frdUserData ON frdMonthlyCard.userId = ".$LEGACY_DBNAME.".frdUserData.privateId"
	);

	// for ( $i=0; $i<8; $i++ ) {
	// 	$datas[] = array(
	// 		"frdSkillLevels",	//hrd_db_game_20
	// 		"userId, skillId, exp",
	// 		"frdSkillLevels",
	// 		"userId, bundleIdx*8 + $i, (levels >> ( 8*$i ))&255",
	// 	);
	// }

	$datas[] = array(
		"frdSkillLevels_back",
		"userId, bundleIdx, levels",
		"frdSkillLevels",
		"userId, bundleIdx, levels",
	);

	$datas[] = array(
		"frdTemple",
		"userId, templeId, templeLevel, clearLevel, upState, lastRecvTime, startLvUpTime",
		"frdTemple",
		"
			userId, 
			templeId, 
			templeLevel, 
			clearLevel, 
			IF(endLvUpTime>0, 1, 0), 
		 	FROM_UNIXTIME(lastRecvTime), 
		 	FROM_UNIXTIME(startLvUpTime)
		"
	);

	$datas[] = array(
		"frdTempleClearCount",
		"userId, dailyCount, totalCount",
	);

	$datas[] = array(
		"frdUserData",
		"
			userId, name, blockState, tutoLevel, 
			userLevel, manaLevel, exp, 
			gold, jewel, powder, stageLevel, skillPoint, chapterReward, accuSkillPoint,
			ticket, heart, maxHeart, curPId, heartStartTime
		",
		"frdUserData",
		"
			privateId, name, 0, tutoLevel, 
			".$LEGACY_DBNAME.".userExps.level, manaLevel, ".$LEGACY_DBNAME.".frdUserData.exp - ".$LEGACY_DBNAME.".userLevels.exp,
			gold, jewel, powder, stageLevel/10000000, ".$LEGACY_DBNAME.".frdSkillPoints.accuSkillPoint, chapterReward, ".$LEGACY_DBNAME.".frdSkillPoints.accuSkillPoint,
			ticket, heart, ".$LEGACY_DBNAME.".userHearts.maxHeart, curPId, NOW()
		",
		"
			INNER JOIN ".$LEGACY_DBNAME.".userExps
			ON (exp >= ".$LEGACY_DBNAME.".userExps.min AND exp < ".$LEGACY_DBNAME.".userExps.max)

			INNER JOIN ".$LEGACY_DBNAME.".userLevels
			ON ".$LEGACY_DBNAME.".userExps.level = ".$LEGACY_DBNAME.".userLevels.level

			INNER JOIN ".$LEGACY_DBNAME.".frdSkillPoints
			ON ".$LEGACY_DBNAME.".frdSkillPoints.userId = privateId

			INNER JOIN ".$LEGACY_DBNAME.".userHearts
			ON ".$LEGACY_DBNAME.".userHearts.level = ".$LEGACY_DBNAME.".userExps.level
		"
	);

	$datas[] = array(
		"frdUserPost",
		"recvUserId, sendUserId, type, count, expire_time",
		"frdUserPost",
		"recvUserId, sendUserId, type, count, FROM_UNIXTIME(time)",
	);

	

	



	$query = "UPDATE ".$LEGACY_DBNAME.".frdHavingWeapons SET exp = 17440 WHERE exp > 17440";
	if ( ProcessQuery ($newPDO, $processPointer, $query) == false ) 
		return;

	$len = count( $datas );
	for ( $i=0; $i<$len; $i++ ) {
		$data = $datas[$i];
		$strCount = (int)count($data);
		switch ($strCount) {
			case 2:
				$idx = array(0,1,1,0);
				break;
			
			case 4:
				$idx = array(0,1,3,2);
				break;

			default:
				$idx = array(0,1,3,2,4);
				break;
		}


		$subQuery = ($strCount>4) ? $data[$idx[4]] : "";
		if (
			ProcessQuery (
				$newPDO, $processPointer,
				"INSERT INTO ".$data[$idx[0]]
				."(".$data[$idx[1]].") "
				."SELECT ".$data[$idx[2]]
				." FROM ".$LEGACY_DBNAME.".".$data[$idx[3]]
				." ".$subQuery
			) == false 
		) 
			return;
	}

	if (
		ProcessQuery (
				$newPDO, $processPointer,
				"INSERT INTO frdEquipWeapons (userId) SELECT userId FROM frdUserData"
			) == false 
		) 
		return;
	if (
		ProcessQuery (
				$newPDO, $processPointer,
				"INSERT INTO frdEquipMagics (userId) SELECT userId FROM frdUserData"
			) == false 
		) 
		return;

	$query = "	UPDATE hrd_db_game_".$OLD_DB_ID.".frdHavingWeapons AS HW
				INNER JOIN ".$LEGACY_DBNAME.".weaponOptions AS WO 
				ON HW.weaponId = WO.idx
				SET
					HW.statId0 = CASE WHEN WO.opt1 = \"\" THEN HW.statId0 ELSE WO.opt1 End,
					HW.statId1 = CASE WHEN WO.opt2 = \"\" THEN HW.statId1 ELSE WO.opt2 End,
					HW.statId2 = CASE WHEN WO.opt3 = \"\" THEN HW.statId2 ELSE WO.opt3 End,
					HW.statId3 = CASE WHEN WO.opt4 = \"\" THEN HW.statId3 ELSE WO.opt4 End
			";
	if ( ProcessQuery ($newPDO, $processPointer, $query) == false ) 
		return;
	

	echo "\n".date("Y-m-d H:i:s")."\nComplete!\n";

?>
