<?php
/*error_reporting(E_ALL);
  ini_set("display_errors", 1);*/
require_once './lib/Performance.php';

class Controller {
	public function __construct() {
	}

	public function write_log_to_file($cat, $mes) {

		$date = date('Y-m-d H:i:s');
		error_log("{$date}| {$cat} | {$mes}" . "\n", 3, "/home/log/perfor_" . date('Ymd') . '.log');
	}

	public function process($param) {

		// If Critical Problem in Server then use  this source

		/* DB OR REDIS DOWN!!
		   $ReturnName = $ApiName;
		   $result['Protocol'] = str_replace('Req', 'Res', $ReturnName);
		   $result['ResultCode'] = 1200; //FAIL CODE
		   return $result;
		 */
		$Perfor = new Performance();
		$Perfor -> timer_start('process');

		$ApiName = $param['Protocol'];

		$ApiResult = null;

		$RequestData = $param['Data'];

		if (isset($param['Data'])) {
			// Data For Using
			$Data = $param['Data'];
			$RequestData = $param['Data'];
		} else {
			$result = null;
			$ReturnName = $ApiName;
			$result['Protocol'] = str_replace('Req', 'Res', $ReturnName);
			$result['ResultCode'] = 1900;
			//FAIL CODE
			return $result;
		}

		$GLOBALS['Type'] = $param['Type'];
		if ($param['Sess'] != 'server') {
			if (!isset($param['Type'])) {// Not Combine Type  Error
				$result = null;
				$ReturnName = $ApiName;
				$result['Protocol'] = str_replace('Req', 'Res', $ReturnName);
				$result['ResultCode'] = 1700;
				//FAIL CODE
				return $result;
			} else {
				$market = $param['Type'];
				switch ($market) {
					case 'AS' :
						// Apple Store
						$SerVer = $GLOBALS['AS_Ver'];
						break;
					case 'PS' :
						// Google Market
						$SerVer = $GLOBALS['PS_Ver'];
						break;
					case 'OS' :
						// Google Market
						$SerVer = $GLOBALS['OS_Ver'];
						break;
					case 'YK' :
						// Google Market
						$SerVer = $GLOBALS['YK_Ver'];
						break;
					default :
						// FAIL CODE
						$result = null;
						$ReturnName = $ApiName;
						$result['Protocol'] = str_replace('Req', 'Res', $ReturnName);
						$result['ResultCode'] = 1600;
						return $result;
						break;
				}
			}
			// Version Check Logic
			$Ver = $param['Ver'];
			// Receive Client Version
			/*
			if ((version_compare($Ver, $SerVer)) < 0) {
				$result = null;
				$ReturnName = $ApiName;
				$result['Protocol'] = str_replace('Req', 'Res', $ReturnName);
				$result['ResultCode'] = 1901;
				//FAIL CODE
				return $result;
			}
			*/

		}

		if (isset($RequestData['userId'])) {
			$userId = $RequestData['userId'];
			$db_no = substr($userId, 0, 3);
			// Sharding For DB
			$GLOBALS['AccessNo'] = $db_no;
		} else {
			$result = null;
			$ReturnName = $ApiName;
			$result['Protocol'] = str_replace('Req', 'Res', $ReturnName);
			$result['ResultCode'] = 1200;
			//FAIL CODE
			return $result;
		}

		include_once './common/DB_Info.php';
		if ($ApiName != 'ReqUserInfo') {
			if ($param['Sess'] != 'server') {// Master Key 'Server'. All By Pass
				include_once './classes/SessManager.php';
				$SessManager = new SessManager();
				if ((isset($param['Sess'])) && ($param['Sess'])) {
					$Sess = $param['Sess'];
					$userId = $RequestData['userId'];
					$Sess = $SessManager -> check_sessionkey($userId, $Sess, $ApiName, $param['Data']);
					if ($Sess['ResultCode'] != 100) {
						$result['ResultCode'] = $Sess['ResultCode'];
						//FAIL CODE
						$ReturnName = $ApiName;
						$result['Protocol'] = str_replace('Req', 'Res', $ReturnName);
						return $result;
					}
				} else {
					$result['ResultCode'] = 367;
					//FAIL CODE
					$ReturnName = $ApiName;
					// Log For Test Server
					Controller::write_log_to_file("test", "-------FAIL---------");
					Controller::write_log_to_file("test", $Sess = $param['Sess']);
					$result['Protocol'] = str_replace('Req', 'Res', $ReturnName);
					return $result;
				}
			}
		}

		$GLOBALS['protocol'] = $ApiName;

		// API CALL
		switch ($ApiName) {
			case 'ReqLogin' :
				include_once "./api/LoginApi.php";
				$newLogin = new LoginApi();
				$ApiResult = $newLogin -> Login($RequestData);
				break;

			case 'ReqCoupon' :
				include_once "./api/CouponApi.php";
				$apiTemp = new CouponApi();
				$ApiResult = $apiTemp ->ReqCoupon($RequestData);
				break;

################# 기획 데이터 받아오기 ########################

			case 'ReqGetLobbyData' :
				include_once './api/SendDataApi.php';
				$RequestData['Type'] = $param['Type'];
				$SendDataApi = new SendDataApi();
				$ApiResult = $SendDataApi->ReqGetLobbyData($RequestData);
				break;
			case 'ReqGetDailyReward' :
				include_once './api/SendDataApi.php';
				$SendDataApi = new SendDataApi();
				$ApiResult = $SendDataApi->ReqGetDailyReward($RequestData);
				break;
			case 'ReqGetWeaponInfo' :
				include_once './api/SendDataApi.php';
				$SendDataApi = new SendDataApi();
				$ApiResult = $SendDataApi->ReqGetWeaponInfo($RequestData);
				break;
			case 'ReqGetMagicInfo' :
				include_once './api/SendDataApi.php';
				$SendDataApi = new SendDataApi();
				$ApiResult = $SendDataApi->ReqGetMagicInfo($RequestData);
				break;
			case 'ReqGetCharacData' :
				include_once './api/SendDataApi.php';
				$SendDataApi = new SendDataApi();
				$ApiResult = $SendDataApi->ReqGetCharacData($RequestData);
				break;
			case 'ReqGetCharacEvolveStatus' :
				include_once './api/SendDataApi.php';
				$SendDataApi = new SendDataApi();
				$ApiResult = $SendDataApi->ReqGetCharacEvolveStatus($RequestData);
				break;
			case 'ReqGetMonData' :
				include_once './api/SendDataApi.php';
				$SendDataApi = new SendDataApi();
				$ApiResult = $SendDataApi->ReqGetMonData($RequestData);
				break;
			case 'ReqGetCharacInfo' :
				include_once './api/SendDataApi.php';
				$SendDataApi = new SendDataApi();
				$ApiResult = $SendDataApi->ReqGetCharacInfo($RequestData);
				break;
###############################################################

############## UserInfo  #################
			case 'ReqUserInfo' :
				include_once "./api/UserInfoApi.php";
				$RequestData['Type'] = $param['Type'];
				$newUserInfo = new UserInfoApi();
				$ApiResult = $newUserInfo ->ReqUserInfo($RequestData);
				break;
			case 'ReqSuccessEffectPreCheck' :
				include_once "./api/UserInfoApi.php";
				$newUserInfo = new UserInfoApi();
				$ApiResult = $newUserInfo ->ReqSuccessEffectPreCheck($RequestData);
				break;
			case 'ReqGetEnterLobbyCheck' :
				include_once "./api/UserInfoApi.php";
				$newUserInfo = new UserInfoApi();
				$ApiResult = $newUserInfo ->ReqGetEnterLobbyCheck($RequestData);
				break;


			case 'ReqSetName' :
				include_once "./api/UserApi.php";
				$newUser = new UserApi();
				$ApiResult = $newUser ->ReqSetName($RequestData);
				break;
			case 'ReqTutoLevel' :
				include_once "./api/UserApi.php";
				$newUser = new UserApi();
				$ApiResult = $newUser ->ReqTutoLevel($RequestData);
				break;

############## Mail #################
			case 'ReqMailList' :
				include_once './api/MailApi.php';
				$MailApi = new MailApi();
				$ApiResult = $MailApi->ReqMailList($RequestData);
				break;
			case 'ReqMailReceive' :
				include_once './api/MailApi.php';
				$MailApi = new MailApi();
				$ApiResult = $MailApi->ReqMailReceive($RequestData);
				break;

############## Achieve #################
			case 'ReqAchieveList' :
				include_once './api/AchieveApi.php';
				$AchieveApi = new AchieveApi();
				$ApiResult = $AchieveApi->ReqAchieveList($RequestData);
				break;
			case 'ReqAchieveReceive' :
				include_once './api/AchieveApi.php';
				$AchieveApi = new AchieveApi();
				$ApiResult = $AchieveApi->ReqAchieveReceive($RequestData);
				break;

############## TEMPLE #################
			case 'ReqGetTempleInfo' :
				include_once './api/TempleApi.php';
				$TempleApi = new TempleApi();
				$ApiResult = $TempleApi->ReqGetTempleInfo($RequestData);
				break;
			case 'ReqTempleReward' :
				include_once './api/TempleApi.php';
				$TempleApi = new TempleApi();
				$ApiResult = $TempleApi->ReqTempleReward($RequestData);
				break;
			case 'ReqLVUpTemple' :
				include_once './api/TempleApi.php';
				$TempleApi = new TempleApi();
				$ApiResult = $TempleApi->ReqLVUpTemple($RequestData);
				break;
			case 'ReqEndTempleLVUP' :
				include_once './api/TempleApi.php';
				$TempleApi = new TempleApi();
				$ApiResult = $TempleApi->ReqEndTempleLVUP($RequestData);
				break;



########## GUERRILLASHOP #############
			case 'ReqOpenCheckGShop' :
				include_once './api/GShopApi.php';
				$GShopApi = new GShopApi();
				$ApiResult = $GShopApi->ReqOpenCheckGShop($RequestData);
				break;
			case 'ReqBuyGShop' :
				include_once './api/GShopApi.php';
				$GShopApi = new GShopApi();
				$ApiResult = $GShopApi->ReqBuyGShop($RequestData);
				break;
			case 'ReqResetGShop' :
				include_once './api/GShopApi.php';
				$GShopApi = new GShopApi();
				$ApiResult = $GShopApi->ReqResetGShop($RequestData);
				break;

########## DAILY QEUST #############
			case 'ReqDailyQuestComplete' :
				include_once './api/DailyQuestApi.php';
				$DApi = new DailyQuestApi();
				$ApiResult = $DApi->ReqDailyQuestComplete($RequestData);
				break;

########## ABILLITY #############
			case 'ReqApplyAbility' :
				include_once './api/AbilityApi.php';
				$AbilityApi = new AbilityApi();
				$ApiResult = $AbilityApi->ReqApplyAbility($RequestData);
				break;
			case 'ReqGetCanUnlock' :
				include_once './api/AbilityApi.php';
				$AbilityApi = new AbilityApi();
				$ApiResult = $AbilityApi->ReqGetCanUnlock($RequestData);
				break;
			case 'ReqEquipAbility' :
				include_once './api/AbilityApi.php';
				$AbilityApi = new AbilityApi();
				$ApiResult = $AbilityApi->ReqEquipAbility($RequestData);
				break;
			case 'ReqOpenSlot' :
				include_once './api/AbilityApi.php';
				$AbilityApi = new AbilityApi();
				$ApiResult = $AbilityApi->ReqOpenSlot($RequestData);
				break;
			case 'ReqResetAbility' :
				include_once './api/AbilityApi.php';
				$AbilityApi = new AbilityApi();
				$ApiResult = $AbilityApi->ReqResetAbility($RequestData);
				break;

########## ABYSS #############
			case 'ReqAbyssReward' :
				include_once './api/AbyssApi.php';
				$AbyssApi = new AbyssApi();
				$ApiResult = $AbyssApi->ReqAbyssReward($RequestData);
				break;
			case 'ReqAbyssRewardList' :
				include_once './api/AbyssApi.php';
				$AbyssApi = new AbyssApi();
				$ApiResult = $AbyssApi->ReqAbyssRewardList($RequestData);
				break;
			case 'ReqAbyssRanking' :
				include_once './api/AbyssApi.php';
				$AbyssApi = new AbyssApi();
				$ApiResult = $AbyssApi->ReqAbyssRanking($RequestData);
				break;


########## CHARACS #############
			case 'ReqBuyCharacs' :
				include_once './api/CharApi.php';
				$CharApi = new CharApi();
				$ApiResult = $CharApi->ReqBuyCharacs($RequestData);
				break;

########## CLIMB #############
			case 'ReqClimbReward' :
				include_once './api/ClimbApi.php';
				$ClimbApi = new ClimbApi();
				$ApiResult = $ClimbApi->ReqClimbReward($RequestData);
				break;
			case 'ReqGetRankNew' :
				include_once './api/ClimbApi.php';
				$ClimbApi = new ClimbApi();
				$ApiResult = $ClimbApi->ReqGetRankNew($RequestData);
				break;
			case 'ReqSearchRank' :
				include_once './api/ClimbApi.php';
				$ClimbApi = new ClimbApi();
				$ApiResult = $ClimbApi->ReqSearchRank($RequestData);
				break;
			case 'ReqNewClimbData' :
				include_once './api/ClimbApi.php';
				$ClimbApi = new ClimbApi();
				$ApiResult = $ClimbApi->ReqNewClimbData($RequestData);
				break;
			case 'ReqGetClimbRewardList' :
				include_once './api/ClimbApi.php';
				$ClimbApi = new ClimbApi();
				$ApiResult = $ClimbApi->ReqGetClimbRewardList($RequestData);
				break;

########## GODDESS #############
			case 'ReqGetGodAbility' :
				include_once './api/GoddessApi.php';
				$GoddessApi = new GoddessApi();
				$ApiResult = $GoddessApi->ReqGetGodAbility($RequestData);
				break;
			case 'ReqGodPresent' :
				include_once './api/GoddessApi.php';
				$GoddessApi = new GoddessApi();
				$ApiResult = $GoddessApi->ReqGodPresent($RequestData);
				break;
			case 'ReqGodQuest' :
				include_once './api/GoddessApi.php';
				$GoddessApi = new GoddessApi();
				$ApiResult = $GoddessApi->ReqGodQuest($RequestData);
				break;
			case 'ReqGodInform' :
				include_once './api/GoddessApi.php';
				$GoddessApi = new GoddessApi();
				$ApiResult = $GoddessApi->ReqGodInform($RequestData);
				break;

########## ITEMS #############
			case 'ReqChangeWeaponStat' :
				include_once './api/ItemApi.php';
				$ItemApi = new ItemApi();
				$ApiResult = $ItemApi->ReqChangeWeaponStat($RequestData);
				break;
			case 'ReqComposeRune' :
				include_once './api/ItemApi.php';
				$ItemApi = new ItemApi();
				$ApiResult = $ItemApi->ReqComposeRune($RequestData);
				break;
			case 'ReqSellItem' :
				include_once './api/ItemApi.php';
				$ItemApi = new ItemApi();
				$ApiResult = $ItemApi->ReqSellItem($RequestData);
				break;
			case 'ReqSpecialExpStone' :
				include_once './api/ItemApi.php';
				$ItemApi = new ItemApi();
				$ApiResult = $ItemApi->ReqSpecialExpStone($RequestData);
				break;
			case 'ReqUseItem' :
				include_once './api/ItemApi.php';
				$ItemApi = new ItemApi();
				$ApiResult = $ItemApi->ReqUseItem($RequestData);
				break;
########## magics #############
			case 'ReqBuyMana' :
				include_once './api/MagicApi.php';
				$MagicApi = new MagicApi();
				$ApiResult = $MagicApi->ReqBuyMana($RequestData);
				break;
			case 'ReqComposeMagicGold' :
				include_once './api/MagicApi.php';
				$MagicApi = new MagicApi();
				$ApiResult = $MagicApi->ReqComposeMagicGold($RequestData);
				break;
			case 'ReqSellMagic' :
				include_once './api/MagicApi.php';
				$MagicApi = new MagicApi();
				$ApiResult = $MagicApi->ReqSellMagic($RequestData);
				break;
			case 'ReqUpgradeMagicGold' :
				include_once './api/MagicApi.php';
				$MagicApi = new MagicApi();
				$ApiResult = $MagicApi->ReqUpgradeMagicGold($RequestData);
				break;
			case 'ReqGetMagicsInfo' :
				include_once './api/MagicApi.php';
				$MagicApi = new MagicApi();
				$ApiResult = $MagicApi->ReqGetMagicsInfo($RequestData);
				break;
			case 'ReqEquipMagic' :
				include_once './api/MagicApi.php';
				$MagicApi = new MagicApi();
				$ApiResult = $MagicApi->ReqEquipMagic($RequestData);
				break;
			case 'ReqSetEquipMagic' :
				include_once './api/MagicApi.php';
				$MagicApi = new MagicApi();
				$ApiResult = $MagicApi->ReqSetEquipMagic($RequestData);
				break;
			case 'ReqMagicExtraction' :
				include_once './api/MagicApi.php';
				$MagicApi = new MagicApi();
				$ApiResult = $MagicApi->ReqMagicExtraction($RequestData);
				break;





########## SHOP #############
			case 'ReqBuyChest' :
				include_once './api/ShopApi.php';
				$ShopApi = new ShopApi();
				$ApiResult = $ShopApi->ReqBuyChest($RequestData);
				break;
			case 'ReqBuyResource' :
				include_once './api/ShopApi.php';
				$ShopApi = new ShopApi();
				$ApiResult = $ShopApi->ReqBuyResource($RequestData);
				break;
			case 'ReqBuyCheck' :
				include_once './api/ShopApi.php';
				$ShopApi = new ShopApi();
				$ApiResult = $ShopApi->ReqBuyCheck($RequestData);
				break;
			case 'ReqGetShopData' :
				include_once './api/ShopApi.php';
				$ShopApi = new ShopApi();
				$ApiResult = $ShopApi->ReqGetShopData($RequestData);
				break;
			case 'ReqBuyCash' :
				include_once './api/ShopApi.php';
				$ShopApi = new ShopApi();
				$ApiResult = $ShopApi->ReqBuyCash($RequestData);
				break;
			case 'ReqBuyError' :
				include_once './api/ShopApi.php';
				$ShopApi = new ShopApi();
				$ApiResult = $ShopApi->ReqBuyError($RequestData);
				break;
			case 'ReqSetShopData' :
				include_once './api/ShopApi.php';
				$ShopApi = new ShopApi();
				$ApiResult = $ShopApi->ReqSetShopData($RequestData);
				break;
			case 'ReqBuyCashGoogle' :
				include_once './api/ShopApi.php';
				$RequestData['marketType'] = $param['Type'];
				$ShopApi = new ShopApi();
				$ApiResult = $ShopApi->ReqBuyCashGoogle($RequestData);
				break;
			case 'ReqCheckBuy' :
				include_once './api/ShopApi.php';
				$ShopApi = new ShopApi();
				$ApiResult = $ShopApi->ReqCheckBuy($RequestData);
				break;
			case 'ReqBuyCashYK' :
				include_once './api/ShopApi.php';
				$RequestData['marketType'] = $param['Type'];
				$ShopApi = new ShopApi();
				$ApiResult = $ShopApi->ReqBuyCashYK($RequestData);
				break;
			case 'ReqYKReceive' :
				include_once './api/ShopApi.php';
				$RequestData['marketType'] = $param['Type'];
				$ShopApi = new ShopApi();
				$ApiResult = $ShopApi->ReqYKReceive($RequestData);
				break;

########## SKILL #############
			case 'ReqResetSkillPoint' :
				include_once './api/SkillApi.php';
				$SkillApi = new SkillApi();
				$ApiResult = $SkillApi->ReqResetSkillPoint($RequestData);
				break;
			case 'ReqUpSkillLV' :
				include_once './api/SkillApi.php';
				$SkillApi = new SkillApi();
				$ApiResult = $SkillApi->ReqUpSkillLV($RequestData);
				break;

########## WEAPON #############
			case 'ReqWeaponExtraction' :
				include_once './api/WeaponApi.php';
				$WeaponApi = new WeaponApi();
				$ApiResult = $WeaponApi->ReqWeaponExtraction($RequestData);
				break;
			case 'ReqGradeUPWeapon' :
				include_once './api/WeaponApi.php';
				$WeaponApi = new WeaponApi();
				$ApiResult = $WeaponApi->ReqGradeUPWeapon($RequestData);
				break;
			case 'ReqLVUpWeapon' :
				include_once './api/WeaponApi.php';
				$WeaponApi = new WeaponApi();
				$ApiResult = $WeaponApi->ReqLVUpWeapon($RequestData);
				break;
			case 'ReqSellWeapon' :
				include_once './api/WeaponApi.php';
				$WeaponApi = new WeaponApi();
				$ApiResult = $WeaponApi->ReqSellWeapon($RequestData);
				break;
			case 'ReqEquipWeapon' :
				include_once './api/WeaponApi.php';
				$WeaponApi = new WeaponApi();
				$ApiResult = $WeaponApi->ReqEquipWeapon($RequestData);
				break;
			case 'ReqChangeWeaponGroup' :
				include_once './api/WeaponApi.php';
				$WeaponApi = new WeaponApi();
				$ApiResult = $WeaponApi->ReqChangeWeaponGroup($RequestData);
				break;
			case 'ReqTranscendWeapon' :
				include_once './api/WeaponApi.php';
				$WeaponApi = new WeaponApi();
				$ApiResult = $WeaponApi->ReqTranscendWeapon($RequestData);
				break;



########## STAGE #############
			case 'ReqEnterStage' :
				include_once './api/StageApi.php';
				$StageApi = new StageApi();
				$ApiResult = $StageApi->ReqEnterStage($RequestData);
				break;
			case 'ReqGetChapterReward' :
				include_once './api/StageApi.php';
				$StageApi = new StageApi();
				$ApiResult = $StageApi->ReqGetChapterReward($RequestData);
				break;
			case 'ReqGetStage' :
				include_once './api/StageApi.php';
				$StageApi = new StageApi();
				$ApiResult = $StageApi->ReqGetStage($RequestData);
				break;
			case 'ReqGetStageReward' :
				include_once './api/StageApi.php';
				$StageApi = new StageApi();
				$ApiResult = $StageApi->ReqGetStageReward($RequestData);
				break;
			case 'ReqGetMyClearData' :
				include_once './api/StageApi.php';
				$StageApi = new StageApi();
				$ApiResult = $StageApi->ReqGetMyClearData($RequestData);
				break;
			case 'ReqWebviewEvent' :
				include_once './api/WebviewApi.php';
				$WebviewApi = new WebviewApi();
				$ApiResult = $WebviewApi->ReqWebviewEvent($RequestData);
				break;
########## LOG #############
			case 'ReqSendPurchaseLog' :
				include_once './api/LogApi.php';
				$LogApi = new LogApi();
				$ApiResult = $LogApi->ReqSendPurchaseLog($RequestData);
				break;
			case 'ReqGPULog' :
				include_once './api/LogApi.php';
				$LogApi = new LogApi();
				$ApiResult = $LogApi->ReqGPULog($RequestData);
				break;


########## CHEAT #############
			case 'ReqCheat' :
				include_once './api/CheatApi.php';
				$CheatApi = new CheatApi();
				$ApiResult = $CheatApi->ReqCheat($RequestData);
				break;

			default :
				$result['ResultCode'] = 1000;
				//FAIL CODE
				$ReturnName = $ApiName;
				$result['Protocol'] = str_replace('Req', 'Res', $ReturnName);
				return $result;
		}

		$per_log = $Perfor -> timer_stop('process');
		Controller::write_log_to_file($ApiName, $per_log['process']);
		return $ApiResult;
	}

}
