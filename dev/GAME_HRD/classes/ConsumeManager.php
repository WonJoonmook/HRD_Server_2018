<?php
include_once './common/DB.php';
require_once './lib/Logger.php';

require_once './classes/AssetManager.php';
require_once './classes/LogDBSendManager.php';


class ConsumeManager {

    public function __construct() {
        $this -> logger = Logger::get();
    }

	public function setConsumeResource ($db, $userId, $idx, $type, $amount) {
		$resultFail['ResultCode'] = 300;


		$AssetManager = new AssetManager();
		switch ($type) {
			case 5000:
				$assetResult = $AssetManager->useGold($db, $userId, $amount);
				if ($assetResult['ResultCode'] != 100) {
					$this->logger->logError('ssetConsumeResource:: FAIL gold userId.'.$userId);
					return $resultFail;
				}
				$temp['curGold'] = $assetResult['gold'];
				$temp['addGold'] = 0 - $amount;
				$result['gold'] = $temp;
				break;
			case 5001:
				$assetResult = $AssetManager->useExp($db, $userId, $amount);
				if ($assetResult['ResultCode'] != 100) {
					$this->logger->logError('ssetConsumeResource:: FAIL expj userId.'.$userId);
					return $resultFail;
				}
				$temp['curExp'] = $assetResult['exp'];
				$temp['addExp'] = 0 - $amount;
				$result['exp'] = $temp;

				break;
			case 5002:
				$assetResult = $AssetManager->useTicket($db, $userId, $amount);
				if ($assetResult['ResultCode'] != 100) {
					$this->logger->logError('ssetConsumeResource:: FAIL ticket userId.'.$userId);
					return $resultFail;
				}
				$temp['curTicket'] = $assetResult['ticket'];
				$temp['addTicket'] = 0 - $amount;
				$result['ticket'] = $temp;

				break;
			case 5003:
				$assetResult = $AssetManager->useHeart($db, $userId, $amount);
				if ($assetResult['ResultCode'] != 100) {
					$this->logger->logError('ssetConsumeResource:: FAIL Heart userId.'.$userId);
					return $resultFail;
				}

				$temp['curHeart'] = $assetResult['curHeart'];
				$temp['addHeart'] = 0 - $amount;
				$result['heart'] = $temp;


				break;
			case 5004:
				$assetResult = $AssetManager->useJewel($db, $userId, $amount);
				if ($assetResult['ResultCode'] != 100) {
					$this->logger->logError('ssetConsumeResource:: FAIL Jewel userId.'.$userId);
					return $resultFail;
				}
				$temp['curJewel'] = $assetResult['jewel'];
				$temp['addJewel'] = 0 - $amount;
				$result['jewel'] = $temp;



				break;
			case 5005:
				$assetResult = $AssetManager->useMedalForAbil($db, $userId, $amount);
				if ($assetResult['ResultCode'] != 100) {
					$this->logger->logError('ssetConsumeResource:: FAIL SP userId.'.$userId);
					return $resultFail;
				}
				$temp['curMedal'] = $assetResult['skillPoint'];
				$temp['addMedal'] = 0 - $amount;
				$result['medal'] = $temp;


				break;
			case 5006:
				$assetResult = $AssetManager->usePowder($db, $userId, $amount);
				if ($assetResult['ResultCode'] != 100) {
					$this->logger->logError('ssetConsumeResource:: FAIL Powder userId.'.$userId);
					return $resultFail;
				}
				$temp['curPowder'] = $assetResult['powder'];
				$temp['addPowder'] = 0 - $amount;
				$result['powder'] = $temp;

				break;
			default :
				  if ( $type < 2000 ) {  // weapon
					// 여기서 왜 order by 를 하는지 알아야 한다.
					// 중요하다!!!!!!!!!!!!!!!!!!!!!!!!!! 위험
//					$sql = "select privateId from frdHavingWeapons where userId=%d and weaponId=%d and exp>=%d order by exp ASC"
					$sql = "SELECT * FROM frdHavingWeapons
						WHERE userId=:userId and weaponId=:typeVal order by level asc limit 0, 1";
					$db -> prepare($sql);
					$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
					$db -> bindValue(':typeVal', ($type-1000), PDO::PARAM_INT);
					$db -> execute();
					$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
					if (!$row) {
						$this->logger->logError('setConsumeResource : FAIL userId : ' . $userId . ", sql : " . $sql);
						return $resultFail;
					}
					$wt = $row;
					$temp['weaponTableId'] = $row['weaponTableId'];
					$temp['weaponId'] = $row['weaponId'];
					$temp['exp'] = $row['exp'];
					$result['weapons'] = $temp;


					$sql = "DELETE FROM frdHavingWeapons WHERE weaponTableId=:typeVal AND userId=:userId";
					$db -> prepare($sql);
					$db -> bindValue(':typeVal', $temp['weaponTableId'], PDO::PARAM_INT);
					$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
					$row = $db -> execute();
					if (!isset($row) || is_null($row) || $row == 0) {
						$this->logger->logError('setConsumeResource : FAIL userId : ' . $userId . ", sql : " . $sql);
						return $resultFail;
					}

					$logDBSendManager = new LogDBSendManager();
					$logDBSendManager->sendWeapon($userId, $wt['weaponId'], 2, 1, 0, $wt['statId0'], $wt['statId1'], $wt['statId2'], $wt['statId3'], $wt['statVal0'], $wt['statVal1'], $wt['statVal2'], $wt['statVal3']);


				} else if ( $type >= 10000 && $type < 11000 ) {       //magic
					$sql = "SELECT magicTableId, magicId, level FROM frdHavingMagics
						WHERE userId=:userId and magicId=:typeVal order by level asc";
					$db -> prepare($sql);
					$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
					$db -> bindValue(':typeVal', ($type-10000), PDO::PARAM_INT);
					$db -> execute();
					$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
					if (!$row) {
						$this->logger->logError('setConsumeResource : FAIL userId : ' . $userId . ", sql : " . $sql);
						return $resultFail;
					}
					$mt = $row;
					$magicTableId = $row['magicTableId'];
					$temp['magicTableId'] = $row['magicTableId'];
					$temp['magicId'] = $row['magicId'];
					$temp['level'] = $row['level'];
					$result['magics'] = $temp;


					$sql = "DELETE FROM frdHavingMagics WHERE magicTableId=:magicTableId and userId=:userId";
					$db -> prepare($sql);
					$db -> bindValue(':magicTableId', $magicTableId, PDO::PARAM_INT);
					$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
					$db -> execute();
					$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
					if (!isset($row) || is_null($row) || $row == 0) {
						$this->logger->logError('setConsumeResource : FAIL userId : ' . $userId . ", sql : " . $sql);
						return $resultFail;
					}

					$logDBSendManager = new LogDBSendManager();
					$logDBSendManager->sendMagic($userId, $mt['magicId'], 2, $mt['level']);

				} else if ( $type >= 100000 && $type < 200000 ) {       //inventory item
					$sql = "select itemCount from frdHavingItems where userId=:userId and itemId=:type";
					$db -> prepare($sql);
					$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
					$db -> bindValue(':type', $type, PDO::PARAM_INT);
					$db -> execute();
					$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
					if (!$row) {
						$this->logger->logError('setConsumeResource : FAIL userId : ' . $userId . ", sql : " . $sql);
						return $resultFail;
					}

					$resultCount = (int)$row["itemCount"] - $amount;
					if ( $resultCount < 0 ) {
						$this->logger->logError('setConsumeResource : need amount userId : ' . $userId . ", itemId : " . $type);
						return $resultFail;
					}
					$temp['itemId'] = $type;
					$temp['cur'] = $resultCount;
					$temp['add'] = 0 - $amount;
					$result['items'] = $temp;

					if ( $resultCount > 0) {
						$sql = "UPDATE frdHavingItems SET itemCount=:resultCount
							WHERE userId=:userId and itemId=:type";
						$db -> prepare($sql);
						$db -> bindValue(':resultCount', $resultCount, PDO::PARAM_INT);
						$db -> bindValue(':type', $type, PDO::PARAM_INT);
						$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
						$row = $db -> execute();
						if (!isset($row) || is_null($row) || $row == 0) {
							$this->logger->logError('setConsumeResource : FAIL userId : '.$userId.", sql : ".$sql);
							return $resultFail;
						}
					} else { // 수량이 0 이므로 삭제 대상이다.
						$sql = "DELETE from frdHavingItems where userId=:userId and itemId=:type";
						$db -> prepare($sql);
						$db -> bindValue(':type', $type, PDO::PARAM_INT);
						$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
						$row = $db -> execute();
						if (!isset($row) || is_null($row) || $row == 0) {
							$this->logger->logError('setConsumeResource : FAIL userId : '.$userId.", sql : ".$sql);
							return $resultFail;
						}
					}

					$logDBSendManager = new LogDBSendManager();
					$logDBSendManager->sendItem($userId, $type, $temp['cur']-$amount, $temp['cur'], 0-$amount);

				}
				break;
		}
		$result['ResultCode'] = 100;
		return $result;
	}
}
?>
