<?php

class TransactionManager {
    public function __construct() {
        $this -> logger = Logger::get();
    }

    public function init_transaction($db, $userId) {

        $resultFail['ResultCode'] = 300;

        $sql = 'START TRANSACTION';
        $db -> prepare($sql);
        $db -> execute();

        $sql = <<<SQL
        SELECT userId
        FROM check_transaction
        WHERE userId = :userId FOR UPDATE
SQL;

        $db -> prepare($sql);
        $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
        $row = $db -> execute();

        if (!isset($row) || is_null($row) || $row == 0) {
            $this->logger->logError(__FUNCTION__.' : SELECT FAIL userId : ' . $userId);
            $sql = 'ROLLBACK';
            $db -> prepare($sql);
            $db -> execute();
            return $resultFail;
        }

        $result['ResultCode'] = 100;

        return $result;
    }

    public function end_transaction($db, $userId) {

        $resultFail['ResultCode'] = 300;

        $sql = 'COMMIT';
        $db -> prepare($sql);
        $db -> execute();

        $result['ResultCode'] = 100;

        return $result;
    }

    public function rollback_transaction($db, $userId) {

        $resultFail['ResultCode'] = 300;

        $sql = 'ROLLBACK';
        $db -> prepare($sql);
        $db -> execute();

        $result['ResultCode'] = 100;

        return $result;
    }

}
?>
