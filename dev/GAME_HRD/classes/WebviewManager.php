<?php
include_once './common/DB.php';
require_once './lib/Logger.php';

class WebviewManager {

    public function __construct() {
        $this -> logger = Logger::get();
    }

	function WebviewEvent ($param) {
		$resultFail['ResultCode'] = 300;

		$attendanceReward = null;
		$userId = $param['userId'];
		$data = null;
		$data["e"]=0;
		$db = new DB();
		$sql = "SELECT UNIX_TIMESTAMP(start_date) as startTime,
			UNIX_TIMESTAMP(end_date) as endTime 
			FROM frdWebViewInfo
			WHERE
			TIMESTAMPDIFF(SECOND, start_date, NOW()) > 0 AND
			TIMESTAMPDIFF(SECOND, NOW(), end_date) > 0
				";
        $db -> prepare($sql);
        $db -> execute();
        $row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
        if ($row) {
			$data['startTime'] = $row['startTime'];	
			$data['endTime'] = $row['endTime'];
			switch ($GLOBALS['COUNTRY']) {
				case 'Korea':
					$data['url'] = 'http://base.hnjgames.xyz:3081/WebView_Page/main.php?userId='.$userId;
					break;
				
				case 'Korea_QA':
					$data['url'] = 'http://14.63.196.82:1081/WebView_Page/main.php?userId='.$userId;
					break;
				default:
					# code...
					break;
			}
		}

		if ( ($userId == 502001527) || ($userId == 800002186) ) {
			$sql = "SELECT UNIX_TIMESTAMP('2017-09-29 00:00:00') as startTime,
				UNIX_TIMESTAMP('2017-10-10 00:00:00') as endTime ";
			$db -> prepare($sql);
			$db -> execute();
			$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);

			$data['startTime'] = $row['startTime'];	
			$data['endTime'] = $row['endTime'];	

			$data['url'] = 'http://base.hnjgames.xyz:3081/WebView_Page/main.php?userId='.$userId;	
		}

		$result['Protocol'] = 'ResWebviewEvent';
		$result['ResultCode'] = 100;
		$result['Data'] = $data;

		return $result;
	}

	function WebviewEventAddItem($db, $userId) {
		$resultFail['ResultCode'] = 300;

		$nowDay = date('ymdH', time());
		$sql = "
			SELECT cnt, today,
				   DATEDIFF(now(), update_date) as diff
					   FROM frdWebViewEvent
					   WHERE userId = :userId";

		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$result = null;
		$Data = null;
		$Data['cnt'] = 0;
		$Data['today'] = 0;
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if ($row) {
			$Data['cnt'] = $row['cnt'];
			$Data['today'] = $row['today'];

			if ($row['diff'] > 0 ) {
				if ( $nowDay >  17100300 &&  $nowDay < 17100600) {
					$cnt = 3+1;
					$today = 1;
				}  else {
					$cnt = 1+1;
					$today = 1;
				}
				
				$sql = "UPDATE frdWebViewEvent SET cnt = cnt + :cnt, today = :today, update_date = now()
					WHERE userId = :userId";
				$db -> prepare($sql);
				$db -> bindValue(':cnt', $cnt, PDO::PARAM_INT);
				$db -> bindValue(':today', $today, PDO::PARAM_INT);
				$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
				$row = $db -> execute();
				if (!isset($row) || is_null($row) || $row == 0) {
					$result = null;
					$result['ResultCode'] = 101;
					$this -> logger -> logError(__FUNCTION__.' FAIL  userId.' . $userId. ' sql: '.$sql);
					return false;
				}
				$Data['cnt'] = $cnt;
				$Data['today'] = $today;
			} else {
				if ($Data['today']  < 5 ) {
					$sql = "UPDATE frdWebViewEvent SET cnt = cnt + 1, today = today + 1
						WHERE userId = :userId";
					$db -> prepare($sql);
					$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
					$row = $db -> execute();
					if (!isset($row) || is_null($row) || $row == 0) {
						$result = null;
						$result['ResultCode'] = 101;
						$this -> logger -> logError(__FUNCTION__.' FAIL  userId.' . $userId. ' sql: '.$sql);
						return false;
					}
				}
			}
		} else {
			// 시간제 무조건 확률증가, 획득 증가 로직
			if ( $nowDay >  17100300 &&  $nowDay < 17100600) {
				$cnt = 4;
				$today = 1;
			}  else {
				$cnt = 2;
				$today = 1;
			}

			$sql = " INSERT INTO frdWebViewEvent (userId, cnt , today, update_date) VALUE
				(:userId, :cnt, :today, now()) ";
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$db -> bindValue(':cnt', $cnt, PDO::PARAM_INT);
			$db -> bindValue(':today', $today, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$result = null;
				$result['ResultCode'] = 101;
				$this -> logger -> logError(__FUNCTION__.'   FAIL  userId.' . $userId. ' sql: '.$sql);
				return false;
			}
		}

		return true;
	}
}
?>
