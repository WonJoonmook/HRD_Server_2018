<?php
include_once './common/define.php';
require_once './lib/Logger.php';

class SkillEffect {
	public function __construct() {
		$this -> logger = Logger::get();
	}

	function getSkillEffect($db, $userId, $isClear) {
		$sql = "select type, val from frdEffectForRewardsSP where userId=:userId";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$sres = array();
		while ($temp = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
			$sres[] = $temp;
		}

		// DB 를 통해 얻은 내용을 재 가공 한다. 
		/*
		   203 습득의 천재 1   특성 포인트 증가 1
		   804 습득의 천재 2   특성 포인트 증가 2
		   903 습득의 천재 3   특성 포인트 증가 3

		   305 경험과 흡수 1   EXP 획득량 증가 1
		   504 경험과 흡수 2   EXP 획득량 증가 2

		   503 암표시장    티켓 획득 확률 증가

		   905 빛나는 영광1    게임 클리어 시, 2배의 보상 획득(유물,마법제외)
		   906 빛나는 영광2    게임 클리어 시, 낮은 확률로 다이아10개 획득
		   1002    자기장 흡수 게임 클리어 시 소모된 번개의 30% 회수
		   1209    룬 전문가   룬 2배 획득 확률 증가

		   1208    진리 도달   모든 스테이지에서 특성 포인트를 2배로 획득
		   1211    착실한 노력 퀘스트아이템 2배 획득 확률 증가

		   203,2,1000
		   804,2,1000
		   903,2,1000

		   305,1,1700
		   504,1,3000

		   503,6,
		   703,5,
		   912,5,2100

		   905,3,100
		   906,4,100
		   1002,7,30000
		   1101,5,8000
		   1208,8,10000
		   1209,9,1000
		   1211,10,1000

		*/
		$medalDoublePer = 0; // 203, 804, 903
		$expPer = 0; // 305, 504
		$ticketPer = 0; // 503
		$medalDouble = false;		 // 1208
		$runeDouble = false;		 // 1208
		$questItemDouble = false; // 1211
	
		$clearAssetDouble = 0; // 905
		$clearJewel10 = 0; // 906

		$clearHeart30PerReturn = false; // 1002
		$soulStoneDouble = false; // 1101

		foreach($sres as $rs) {
			switch($rs['type']){ 
				case 1: //  경험치 획득량 증가 
					$expPer = $expPer + $rs['val'];
					break;
				case 2:  // 특성포인트 증가 
					$medalDoublePer += $rs['val'];
					break;
				case 3: // 2배겟
//					$this->logger->logError(__FUNCTION__.' : 두배 진겟타  userId :'.$userId.'  :'.$rs['val'].' 확률'. $temp);
					if ( $isClear && rand(0,99999) < $rs['val'] ) {
						$clearAssetDouble = true;
					}
					break;
				case 4: // 다이아낮은 확률 10개 지급 
//					$this->logger->logError(__FUNCTION__.' : 보석 뻥   userId :'.$userId.'  :'.$rs['val'].' 확률'. $temp);
					if ( $isClear && rand(0,99999) < $rs['val'] ) {
						$clearJewel10 = true; // 906
					}
					break;
				case 5: //영혼석 2배
					if ( rand(0,99999) < $rs['val'] ) {
						$soulStoneDouble = true; 
					}
					break;
				case 6: //  티켓 획득 활률 증가 
					$ticketPer = $rs['val']*0.0001;
					break;
				case 7: // 소모된 하트 30% 회수 
					if ($isClear) {
						$clearHeart30PerReturn = true;
					}
					break;
				case 8:
					$medalDouble = true;
					break;
				case 9:
					$temp = rand(0,99999);
					if ( $temp < $rs['val']) {
						$runeDouble = true;
					}
//					$this->logger->logError(__FUNCTION__.' : 룬아이템 더블 확률 userId :'.$userId.'  :'.$rs['val'].' 확률'. $temp);
					break;
				case 10:
					$temp = rand(0,99999);
					if ( $temp < $rs['val']) {
						$questItemDouble = true;
					}
//					$this->logger->logError(__FUNCTION__.' : 퀘스트 아이템 더블 확률 userId :'.$userId.'  :'.$rs['val'].'확률'. $temp);
					break;
				default : 
					break;
			}
		}

        $result['medalDoublePer']         = $medalDoublePer;          
        $result['expPer']                 = $expPer;               
        $result['ticketPer']              = $ticketPer;
        $result['medalDouble']            = $medalDouble;          
        $result['runeDouble']             = $runeDouble;           
        $result['questItemDouble']        = $questItemDouble;      
        $result['clearAssetDouble']       = $clearAssetDouble;     
        $result['clearJewel10']           = $clearJewel10;         
        $result['clearHeart30PerReturn']  = $clearHeart30PerReturn;
        $result['soulStoneDouble']        = $soulStoneDouble;   

		return $result;
	}
}
?>
