<?php
require_once './lib/Logger.php';
include_once './common/define.php';
include_once './classes/AssetManager.php';
include_once './classes/AddManager.php';

class RewardManager {
    public function __construct() {
        $this -> logger = Logger::get();
    }
	function getReward($db, $rewardTypes, $rewardCounts, $userId, $weaponIdxs,$subWeaponIdxs ) {
		$resultExp = 0;
		$length = count($rewardTypes);

		$AssetManager = new AssetManager();

		$dataTotal = null;
		$gold = null;
		$jewel = null;
		$heart = null;
		$ticket = null;
		$charData = null;
		$AddManager = new AddManager();
		for ( $ii=0; $ii<$length; $ii++ ) {

			$rewardType = $rewardTypes[$ii];
			$rewardCount = $rewardCounts[$ii];
			$data = null;
			switch ($rewardType) {
				case 5000: // 골드 지급 
					$gold_result = $AssetManager->addGold($db, $userId, $rewardCount);
					if ($gold_result['ResultCode'] != 100 ) {
						return $gold_result;
					}	
					$gold["curGold"] = $gold_result['gold'];
					if (isset($gold['addGold'])) {
						$gold["addGold"] = $gold['addGold'] + $rewardCount;
					} else {
						$gold["addGold"] = $rewardCount;
					}
					break;
				case 5001: //경험치 지급
					$addedExp = (int)$rewardCount;
					$resultExp += $addedExp;
					$exp["curExp"] = $addedExp;
					break;
				case 5002: //티켓 지급
					$AssetResult = $AssetManager->addTicket($db, $userId, $rewardCount);
					if ($AssetResult['ResultCode'] != 100 ) {
						return $AssetResult;
					}	
					$ticket["curTicket"] = $AssetResult['curTicket'];
					if (isset($ticket["addTicket"])) {
						$ticket["addTicket"] +=  $rewardCount;
					} else {
						$ticket["addTicket"] =  $rewardCount;
					}

					break;
				case 5003: //행동력 지급
					$AssetResult = $AssetManager->addHeart($db, $userId, $rewardCount);
					if ($AssetResult['ResultCode'] != 100 ) {
						return $AssetResult;
					}	
					$heart["curHeart"] = $AssetResult['curHeart'];
					$heart["maxHeart"] = $AssetResult['maxHeart'];
					if (isset($ticket["addHeart"])) {
					$heart["addHeart"] += $rewardCount;
					} else {
					$heart["addHeart"] = $rewardCount;
					}
					break;
				case 5004: // 보석 지급 
					$AssetResult = $AssetManager->addJewel($db, $userId, $rewardCount);
					if ($AssetResult['ResultCode'] != 100 ) {
						return $AssetResult;
					}	
					$jewel["curJewel"] = $AssetResult['jewel'];
					if (isset($jewel["addJewel"])) {
						$jewel["addJewel"] += $rewardCount;
					} else {
						$jewel["addJewel"] = $rewardCount;
					}
					break;
				case 5005: // 메달 지급 
					$AssetResult = $AssetManager->addMedal($db, $userId, $rewardCount);
					if ($AssetResult['ResultCode'] != 100 ) {
						return $AssetResult;
					}	
					$skillPoint["curMedal"] = $AssetResult['skillPoint'];
					if (isset($skillPoint['addMedal'])) {
						$skillPoint["addMedal"] += $rewardCount;
					} else {
						$skillPoint["addMedal"] = $rewardCount;
					}
					break;
				default: 
					 if ( $rewardType < 1000 ) {        //고정 캐릭터 소울스톤 지급
						 if ( $rewardType > 500 ) {
							 $key_name = $rewardType - 500;
							 $apc_key = 'CharacIndex_'.$key_name;
							 $cd = apc_fetch($apc_key);
							 $charMin = $cd[0];
							 $charMax = $cd[1];
							 $rewardType = rand($charMin, $charMax);
						 }

						$CharResult = $AddManager->setAddHero($db, $userId, $rewardType, $rewardCount);
						if ($CharResult['ResultCode'] != 100 ) {
							$this->logger->logError(__FUNCTION__.__LINE__.': FAIL userId : '.$userId);
							break;
						}
						$charTemp = null;
						$charTemp['charTableId'] = $CharResult['charTableId'];
						$charTemp['charId'] = $CharResult['charId'];
						$charTemp['exp'] = $CharResult['curExp'];
//						$charTemp['addExp'] = $CharResult['addExp'];
						
						$charData[] = $charTemp;


					} else if ( $rewardType < 1510 ) {    //보구 지급
						$rewardType = $rewardType - 1000;
						if ( $rewardType > 500 ) {
							$array_pos = $rewardType - 500;
							$apc_key = 'WeaponInform_WeaponMinIdx';
							$weaponT = apc_fetch($apc_key);
							$minTemp = explode('/', $weaponT[1]);
							$weaponMin = $minTemp[$array_pos];

							$apc_key = 'WeaponInform_WeaponMaxIdx';
							$weaponT = apc_fetch($apc_key);
							$maxTemp = explode('/', $weaponT[1]);
							$weaponMax = $maxTemp[$array_pos];

							$rewardType = rand($weaponMin, $weaponMax);
						}

						$WeaponResult = $AddManager->addWeapon($db, $userId, $rewardType, null);
						if ($WeaponResult['ResultCode'] != 100) {
							$this->logger->logError(__FUNCTION__.__LINE__.': FAIL userId : '.$userId);
							break;
						}
						$weaponTemp = null;
						$weaponTemp['weaponTableId'] = $WeaponResult['weaponTableId'];
						$weaponTemp['weaponId'] = $WeaponResult['weaponId'];
						$weaponTemp['level'] = $WeaponResult['level'];
						$weaponTemp['statIds'] = $WeaponResult['statIds'];
						$weaponTemp['statVals'] = $WeaponResult['statVals'];
						$weaponTemp['exp'] = $WeaponResult['exp'];
						
						$weaponData[] = $weaponTemp;

					} else if ( $rewardType >= 10000 && $rewardType < 10510 ) {  //마법 지급 
						if ( $rewardType > 10500 ) {
							$key_name = $rewardType - 10500;
							$apc_key = 'MagicPowderValue_'.$key_name;
							$md = apc_fetch($apc_key);
							$magicMin = $md[2] + 10000;
							$magicMax = $md[3] + 10000;

							$rewardType = rand($magicMin, $magicMax);
						}
						$rewardType = $rewardType - 10000;

						$MagicResult = $AddManager->addMagic($db, $userId, $rewardType);
						if ($MagicResult['ResultCode'] != 100 ) {
							$this->logger->logError(__FUNCTION__.__LINE__.': FAIL userId : '.$userId);
							break;
						}
						$magicTemp = null;
						$magicTemp['magicTableId'] = $MagicResult['magicTableId'];
						$magicTemp['magicId'] = $MagicResult['magicId'];
						$magicTemp['exp'] = 0;
						
						$magicData[] = $magicTemp;
					} else if ( $rewardType >= 99999 && $rewardType < 200000 ) {   //아이템 지급
						$itemResult = $AddManager->addItem($db, $userId, $rewardType,$rewardCount);
						if ($itemResult['ResultCode'] != 100 ) {
							$this->logger->logError(__FUNCTION__.__LINE__.': FAIL userId : '.$userId);
							break;
						}
						$itemTemp = null;
						$itemTemp['itemTableId'] = $itemResult['itemTableId'];
						$itemTemp['itemId'] = $itemResult['itemId'];
						$itemTemp['cur'] = $itemResult['cnt'];
						$itemTemp['add'] = $rewardCount;
						
						$itemData[] = $itemTemp;
					} else {
						$this->logger->logError(__FUNCTION__.__LINE__.': ERROR userId : '.$userId);
						$this->logger->logError(__FUNCTION__.__LINE__.': ERROR type : '.$rewardType);
					}
					break;
			}
		}
		if (isset($gold)) {
			$dataTotal['gold'] = $gold;
		}
		if ( isset($ticket) ) {
			$dataTotal['ticket'] = $ticket;
		}
		if (isset($jewel)) {
			$dataTotal['jewel'] = $jewel;
		}
		if (isset($skillPoint)) {
			$dataTotal['medal'] = $skillPoint;
		}
		if (isset($heart)) {
			$dataTotal['heart'] = $heart;
		}

		if (isset($weaponData)) {
			$dataTotal['weapons'] = $weaponData;
		}
		if (isset($magicData)) {
			$dataTotal['magics'] = $magicData;
		}
		if (isset($itemData)) {
			$dataTotal['items'] = $itemData;
		}
		if (isset($charData)) {
			$dataTotal['characs'] = $charData;
		}

		if ($resultExp)  {
			if (count($weaponIdxs) > 0) {
				$wResult = $AddManager->addWeaponExp($db, $userId, $weaponIdxs, $resultExp, true);
				if ($wResult['ResultCode'] != 100) {
					//error return 
				}
				$dataTotal['weaponExp'] = $wResult['exp'];
				
			} 
			if (count($subWeaponIdxs) > 0) {
				$wResult = $AddManager->addWeaponExp($db, $userId, $subWeaponIdxs, (int)($resultExp*0.3), true);
				if ($wResult['ResultCode'] != 100) {
					//error return
				}

				if (count($weaponIdxs) > 0) {
				 	$wExp = array_merge($dataTotal['weaponExp'], $wResult['exp']);
				 	$dataTotal['weaponExp'] =  $wExp;
			
				}
			}

			$AssetResult = $AssetManager->addUserExp($db, $userId, $resultExp);
			if ($AssetResult['ResultCode'] != 100) {
				//error return 
			}
			$dataTotal['exp'] = $AssetResult["exp"];
		}

		$result['ResultCode'] = 100;
		$result['Data'] = $dataTotal;
		return $result;
	}
}
?>
