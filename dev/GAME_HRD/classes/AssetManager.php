<?php
include_once './common/DB.php';
include_once './common/define.php';
require_once './lib/Logger.php';
require_once './classes/AchieveManager.php';
require_once './classes/DailyQuestManager.php';
require_once './classes/LogDBSendManager.php';

class AssetManager {

    public function __construct() {
        $this -> logger = Logger::get();
    }

	public function addPowder ($db, $userId, $amount) {
		$resultFail['ResultCode'] = 300;

		$sql = <<<SQL
			SELECT powder
			FROM   frdUserData
			WHERE  userId = :userId
SQL;
        $db -> prepare($sql);
        $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
        $db -> execute();
        $row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
        if (!$row) {
            $this -> logger -> logError('AssetManager:: addPowder : SELECT FAIL userId :' . $userId);
            $result['ResultCode'] = 300;
            return $result;
        }

		if ( $amount > 1000000 ) {
    $this -> logger -> logError('AssetManager:: addPowder : Powder over amount userId :' . $userId . ' amount : ' .$amount);
            $result['ResultCode'] = 300;
            return $result;
		}
		$powder = $row['powder'];

		$sql = <<<SQL
			UPDATE frdUserData 
			SET    powder = powder + :amount, update_date = now()  
			WHERE  userId = :userId 
SQL;

		$db -> prepare($sql);
		$db -> bindValue(':amount', $amount, PDO::PARAM_INT);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this -> logger -> logError('AssetManager:: addPowder : over amount userId :' . $userId . ' amount : ' .$amount);
			return $resultFail;
		}

		$sql = "select achieveTableId, achieveId, cnt FROM frdAchieveInfo
			where userId=:userId and achieveId > 3400 and achieveId < 3500";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if ($row) {
			$achieveId = $row['achieveId'];
			if ($row['cnt'] != -1 ) {
				require_once './classes/AchieveManager.php';
				$AchieveManager = new AchieveManager();
				$AchieveManager->addAchieveData($db, $userId, $achieveId, $amount);
			}
		}

		$logDBSendManager = new LogDBSendManager();
		$logDBSendManager->sendItem($userId, Type_Powder, $powder, $powder+$amount, $amount);


		$result['ResultCode'] = 100;
		$result['powder'] = $powder + $amount;

		return $result;
	}

	public function usePowder ($db, $userId, $amount) {
        $resultFail['ResultCode'] = 300;

		$sql = <<<SQL
        SELECT powder
        FROM   frdUserData
        WHERE  userId = :userId
SQL;
        $db -> prepare($sql);
        $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
        $db -> execute();
        $row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
        if (!$row) {
            $this -> logger -> logError('AssetManager:: usePowder : SELECT FAIL userId :' . $userId);
            $result['ResultCode'] = 300;
            return $result;
        }

		$powder = $row['powder'];

		$myPowder = $powder - $amount;

		if ( $myPowder < 0 ) {
    $this -> logger -> logError('AssetManager:: usePowder : over my. userId :' . $userId . ' amount : ' .$amount);
            $result['ResultCode'] = 300;
            return $result;
		}

		$sql = <<<SQL
			UPDATE frdUserData 
			SET    powder = :myPowder, update_date = now()  
			WHERE  userId = :userId 
SQL;
		$db -> prepare($sql);
		$db -> bindValue(':myPowder', $myPowder, PDO::PARAM_INT);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this -> logger -> logError('AssetManager:: usePowder : updateFail userId :' . $userId . ' sql : ' .$sql);
			return $resultFail;
		}
	
		$logDBSendManager = new LogDBSendManager();
		$logDBSendManager->sendAsset($userId, Type_Powder, $powder, $myPowder, 0-$amount);


		$result['ResultCode'] = 100;
		$result['powder'] = $myPowder;

		return $result;

	}	

	public function addJewel ($db, $userId, $Amount) {
		$resultFail['ResultCode'] = 300;
		if ( ( $Amount > 1000000 ) || ($Amount < 0 ) ) {
			$this->logger->logError('addJewel : Jewel FAIL amount userId :' . $userId . ' amount : ' .$amount);
            $result['ResultCode'] = 300;
            return $result;
		}

		$sql = <<<SQL
			UPDATE frdUserData 
			SET    jewel = jewel + :Amount, update_date = now()  
			WHERE  userId = :userId 
SQL;

		$db -> prepare($sql);
		$db -> bindValue(':Amount', $Amount, PDO::PARAM_INT);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this -> logger -> logError('addJewel : FAIL UPDATE userId :' . $userId . ' Amount : ' .$Amount);
			return $resultFail;
		}

		$sql = <<<SQL
			SELECT jewel
			FROM   frdUserData
			WHERE  userId = :userId
SQL;
        $db -> prepare($sql);
        $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
        $db -> execute();
        $row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
        if (!$row) {
            $this -> logger -> logError('addJewel : SELECT FAIL userId :' . $userId);
            $result['ResultCode'] = 300;
            return $result;
        }		
		$jewel = $row['jewel'];

        $sql = "select achieveTableId, achieveId, cnt FROM frdAchieveInfo
            where userId=:userId and achieveId > 3500 and achieveId < 3600";
        $db->prepare($sql);
        $db->bindValue(':userId', $userId, PDO::PARAM_INT);
        $db -> execute();
        $row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
        if ($row) {
			$achieveId = $row['achieveId'];
			if ($row['cnt'] != -1 ) {
				$AchieveManager = new AchieveManager();
				$AchieveManager->addAchieveData($db, $userId, $achieveId, $Amount);
			}
		}

		$logDBSendManager = new LogDBSendManager();
		$logDBSendManager->sendAsset($userId, Type_Jewel, $jewel-$Amount, $jewel, $Amount);
		

		$result['ResultCode'] = 100;
		$result['jewel'] = $jewel;

		return $result;
	}

	public function useJewel ($db, $userId, $amount) {
        $resultFail['ResultCode'] = 300;

		$sql = "SELECT jewel  FROM   frdUserData WHERE  userId = :userId";
        $db -> prepare($sql);
        $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
        $db -> execute();
        $row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
        if (!$row) {
            $this -> logger -> logError(__FUNCTION__.': FAIL userId :'.$userId.' sql :'.$sql);
            return $resultFail;
        }

		$jewel = $row['jewel'];

		$myJewel = $jewel - $amount;

		if ( $myJewel < 0 ) {
    $this -> logger -> logError('AssetManager:: useJewel : over myJewel userId :' . $userId . ' amount : ' .$amount);
            $result['ResultCode'] = 300;
            return $result;
		}

		$sql = <<<SQL
			UPDATE frdUserData 
			SET    jewel = :myJewel, update_date = now()  
			WHERE  userId = :userId 
SQL;
		$db -> prepare($sql);
		$db -> bindValue(':myJewel', $myJewel, PDO::PARAM_INT);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this -> logger -> logError('AssetManager:: useJewel : updateFail userId :' . $userId . ' sql : ' .$sql);
			return $resultFail;
		}

		$logDBSendManager = new LogDBSendManager();
		$test = $logDBSendManager->sendAsset($userId, Type_Jewel, $jewel, $myJewel, 0-$amount);
		$this -> logger -> logError(__FUNCTION__.': test userId.' . $test);


		$result['ResultCode'] = 100;
		$result['jewel'] = $myJewel;

		return $result;

	}	

	public function addGold ($db, $userId, $amount) {
        $resultFail['ResultCode'] = 300;

		$sql = <<<SQL
        SELECT gold
        FROM   frdUserData
        WHERE  userId = :userId
SQL;
        $db -> prepare($sql);
        $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
        $db -> execute();
        $row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
        if (!$row) {
            $this -> logger -> logError('AssetManager:: addGold : SELECT FAIL userId :' . $userId);
            $result['ResultCode'] = 300;
            return $result;
        }

		if ( $amount > 5000000 ) {
    $this -> logger -> logError('AssetManager:: addGold : Gold over amount userId :' . $userId . ' amount : ' .$amount);
            $result['ResultCode'] = 300;
            return $result;
		}
		$gold = $row['gold'];

		$sql = <<<SQL
			UPDATE frdUserData 
			SET    gold = gold + :amount, update_date = now()  
			WHERE  userId = :userId 
SQL;

		$db -> prepare($sql);
		$db -> bindValue(':amount', $amount, PDO::PARAM_INT);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this -> logger -> logError('AssetManager:: addGold : Gold over amount userId :' . $userId . ' amount : ' .$amount);
			return $resultFail;
		}

		$sql = "select achieveTableId, achieveId, cnt FROM frdAchieveInfo
			where userId=:userId and achieveId > 3300 and achieveId < 3400";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if ($row) {
			$achieveId = $row['achieveId'];
			if ($row['cnt'] != -1 ) {
				$AchieveManager = new AchieveManager();
				$AchieveManager->addAchieveData($db, $userId, $achieveId, $amount);
			}
		}


		$logDBSendManager = new LogDBSendManager();
		$logDBSendManager->sendAsset($userId, Type_Gold, $gold, $gold+$amount, $amount);

		$result['ResultCode'] = 100;
		$result['gold'] = $gold + $amount;

		return $result;
	}

	public function useGold ($db, $userId, $amount) {
        $resultFail['ResultCode'] = 300;

		$sql = <<<SQL
        SELECT gold
        FROM   frdUserData
        WHERE  userId = :userId
SQL;
        $db -> prepare($sql);
        $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
        $db -> execute();
        $row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
        if (!$row) {
            $this -> logger -> logError('AssetManager:: useGold : SELECT FAIL userId :' . $userId);
            $result['ResultCode'] = 300;
            return $result;
        }

		$gold = $row['gold'];

		$myGold = $gold - $amount;

		if ( $myGold < 0 ) {
			$this->logger->logError('useGold : over mygold userId :' . $userId . ' amount : ' .$amount);
            $result['ResultCode'] = 300;
            return $result;
		}


		$sql = <<<SQL
			UPDATE frdUserData 
			SET    gold = :myGold, update_date = now()  
			WHERE  userId = :userId 
SQL;

		$db -> prepare($sql);
		$db -> bindValue(':myGold', $myGold, PDO::PARAM_INT);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this -> logger -> logError('AssetManager:: useGold : updateFail userId :' . $userId . ' sql : ' .$sql);
			return $resultFail;
		}

		$logDBSendManager = new LogDBSendManager();
		$logDBSendManager->sendAsset($userId, Type_Gold, $gold, $myGold, 0-$amount);
		

		$result['ResultCode'] = 100;
		$result['gold'] = $myGold;

		return $result;

	}	

    public function addTicket($db, $userId, $Amount) {
        $resultFail['ResultCode'] = 300;
		if (($Amount < 0 ) || ($Amount > 10000)) {
			$this -> logger -> logError('addTicket : FAIL Amount : '. $Amount. ' userId :' . $userId);
			return $resultFail;
		}

		$sql = <<<SQL
			UPDATE frdUserData 
			SET    ticket = ticket + :Amount, update_date = now()  
			WHERE  userId = :userId 
SQL;

		$db -> prepare($sql);
		$db -> bindValue(':Amount', $Amount, PDO::PARAM_INT);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this -> logger -> logError('addTicket : updateFail userId :' . $userId . ' sql : ' .$sql);
			return $resultFail;
		}

		$sql = <<<SQL
        SELECT ticket
        FROM   frdUserData
        WHERE  userId = :userId
SQL;
        $db -> prepare($sql);
        $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
        $db -> execute();
        $row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
        if (!$row) {
            $this -> logger -> logError('addTicket : SELECT FAIL userId :' . $userId);
            $result['ResultCode'] = 300;
            return $result;
        }

		$logDBSendManager = new LogDBSendManager();
		$logDBSendManager->sendAsset($userId, Type_Ticket, $row['ticket'] - $Amount, $row['ticket'], $Amount);


		$result['ResultCode'] = 100;
		$result['curTicket'] = $row['ticket'];
		$result['addTicket'] = $Amount;
	
        return $result;
    }

    public function addHeart($db, $userId, $Amount) {
        $resultFail['ResultCode'] = 300;
		if (($Amount < 0 ) || ($Amount > 10000)) {
			$this -> logger -> logError('addHeart : FAIL Amount : '. $Amount. ' userId :' . $userId);
			return $resultFail;
		}

		$sql = <<<SQL
			UPDATE frdUserData 
			SET    heart = heart + :Amount, update_date = now()  
			WHERE  userId = :userId 
SQL;

		$db -> prepare($sql);
		$db -> bindValue(':Amount', $Amount, PDO::PARAM_INT);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this -> logger -> logError('addHeart : updateFail userId :' . $userId . ' sql : ' .$sql);
			return $resultFail;
		}

		$sql = <<<SQL
        SELECT heart,maxHeart,TIMESTAMPDIFF(SECOND, heartStartTime, now()) as passTime
        FROM   frdUserData
        WHERE  userId = :userId
SQL;
        $db -> prepare($sql);
        $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
        $db -> execute();
        $row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
        if (!$row) {
            $this -> logger -> logError('addHeart : SELECT FAIL userId :' . $userId);
            $result['ResultCode'] = 300;
            return $result;
        }

		$result['ResultCode'] = 100;
		$result['curHeart'] = $row['heart'];
		$result['maxHeart'] = $row['maxHeart'];
		$result['passTime'] = $row['passTime'];
		if ($result['curHeart'] >= $result['maxHeart']) {
			$result['passTime'] = 0;
		}

		$logDBSendManager = new LogDBSendManager();
		$logDBSendManager->sendAsset($userId, Type_Heart, $row['heart'] - $Amount, $row['heart'], $Amount);

        return $result;
    }

	public function addMedal ($db, $userId, $Amount) {
		$resultFail['ResultCode'] = 300;
		if ( ( $Amount > 1000000 ) || ($Amount < 0 ) ) {
			$this->logger->logError('addMedal : Medal FAIL amount userId :' . $userId . ' amount : ' .$amount);
            $result['ResultCode'] = 300;
            return $result;
		}

		$sql = <<<SQL
			UPDATE frdUserData 
			SET    skillPoint = skillPoint + :Amount, 
			accuSkillPoint = accuSkillPoint + :Amount, update_date = now()  
			WHERE  userId = :userId 
SQL;
		$db -> prepare($sql);
		$db -> bindValue(':Amount', $Amount, PDO::PARAM_INT);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this -> logger -> logError('addMedal : FAIL UPDATE userId :' . $userId . ' Amount : ' .$Amount);
			return $resultFail;
		}

		$sql = <<<SQL
			SELECT skillPoint, accuSkillPoint
			FROM frdUserData 
			WHERE userId = :userId
SQL;
        $db -> prepare($sql);
        $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
        $db -> execute();
        $row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
        if (!$row) {
            $this -> logger -> logError('addMedal : SELECT FAIL userId :' . $userId);
            $result['ResultCode'] = 300;
            return $result;
        }
		$skillPoint = $row['skillPoint'];
		$accuSkillPoint = $row['accuSkillPoint'];

		$sql = "select achieveTableId, achieveId, cnt FROM frdAchieveInfo
			where userId=:userId and achieveId > 3600 and achieveId < 3700";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if ($row) {
			$achieveId = $row['achieveId'];
			if ($row['cnt'] != -1 ) {
				$AchieveManager = new AchieveManager();
				$AchieveManager->addAchieveData($db, $userId, $achieveId, $Amount);
			}
		}

		// 일일 퀘스트 	
		$DailyQuestManager = new DailyQuestManager();
		$DailyQuestManager->updateDayMission($db, $userId, 7, $Amount);

		$result['ResultCode'] = 100;
		$result['skillPoint'] = $skillPoint;
		$result['accuSkillPoint'] = $accuSkillPoint;

		$logDBSendManager = new LogDBSendManager();
		$logDBSendManager->sendAsset($userId, Type_Medal, $skillPoint -$Amount, $skillPoint, $Amount);

		return $result;
	}
    #######################################################################

    public function useHeart($db, $userId, $Amount) {
        $resultFail['ResultCode'] = 300;

        $result_check_SP = $this->checkHeart($db, $userId);
        if ($result_check_SP['ResultCode'] != 100) {
            $this -> logger -> logError(__FUNCTION__.' : checkHeart FAIL userId :'.$userId);
            return $resultFail;
        }

		$passTime = $result_check_SP['passTime'];
        $SP = $result_check_SP['heart'];
        $Max_SP = $result_check_SP['maxHeart'];

        if ((!isset($Amount)) || (!$Amount)) {
            $Amount = 1;
        }

        if ($SP < $Amount) {
            $resultFail['ResultCode'] = 360;
            $this -> logger -> logError(__FUNCTION__.' :  $SP < $Amount FAIL userId :'.$userId);
            $this -> logger -> logError(__FUNCTION__.' :  $SP :'.$SP.' $Amount:'. $Amount);
            return $resultFail;
        }

        if ($SP == $Max_SP) {
            $sql = <<<SQL
Update frdUserData SET heart = heart - :Amount, heartStartTime = now() ,update_date=now() 
	WHERE userId = :userId
SQL;
            $db -> prepare($sql);
            $db -> bindValue(':Amount', $Amount, PDO::PARAM_INT);
            $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
            $row = $db -> execute();
            if (!isset($row) || is_null($row) || $row == 0) {
                $this->logger->logError(__FUNCTION__.': update1 userId.'.$usreId .', sql : '.$sql);
                return $resultFail;
            }
			$passTime = 1;
        } else {
            if (($SP > $Max_SP - 1) && (($SP - $Amount) < $Max_SP)) {
                $sql = <<<SQL
Update frdUserData SET heart = heart - :Amount, heartStartTime = now(), update_date=now() 
WHERE userId = :userId 
SQL;
                $db -> prepare($sql);
                $db -> bindValue(':Amount', $Amount, PDO::PARAM_INT);
				$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
                $row = $db -> execute();
                if (!isset($row) || is_null($row) || $row == 0) {
					$this->logger->logError(__FUNCTION__.': update2 userId.'.$usreId .', sql : '.$sql);
                    $result['ResultCode'] = 300;
                    return $result;
                }
				$passTime = 1;
            } else {
                $sql = <<<SQL
Update frdUserData SET heart = heart - :Amount, update_date = now() 
WHERE userId = :userId 
SQL;
                $db -> prepare($sql);
                $db -> bindValue(':Amount', $Amount, PDO::PARAM_INT);
				$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
                $row = $db -> execute();
                if (!isset($row) || is_null($row) || $row == 0) {
					$this->logger->logError(__FUNCTION__.': update3 userId.'.$userId .', sql : '.$sql);
                    $result['ResultCode'] = 300;
                    return $result;
                }
            }
        }

		$logDBSendManager = new LogDBSendManager();
		$logDBSendManager->sendAsset($userId, Type_Heart, $SP, $SP-$Amount, 0-$Amount);

        $result['ResultCode'] = 100;

		$result['passTime'] = $passTime;
		$result['curHeart'] = $SP - $Amount;
		$result['maxHeart'] = $Max_SP;

		if ($result['curHeart'] >= $result['maxHeart']) {
			$result['passTime'] = 0;
		}

        return $result;
    }

    public function checkHeart($db, $userId) {
		// FIX DATA
        $check_pass_time = 180;
	
        $sql = <<<SQL
SELECT
TIMESTAMPDIFF(SECOND, heartStartTime, now()) as passTime, heart, maxHeart
FROM frdUserData WHERE userId = :userId 
SQL;
        $db -> prepare($sql);
        $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
        $db -> execute();
        $row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
        if (!$row) {
            $this -> logger -> logError(__FUNCTION__ . ': FAIL userId: ' . $userId . ', sql : ' . $sql);
            $result['ResultCode'] = 300;
            return $result;
        }
        $passTime = $row['passTime'];
        $curHeart = $row['heart'];
        $maxHeart = $row['maxHeart'];

		if ($curHeart >= $maxHeart) {
			$passTime = 0;
		}

        $TotSP = $curHeart;
        if ($passTime >= $check_pass_time) {
            $addHeart = $passTime / $check_pass_time;
            $addHeart = floor($addHeart);

            if (($addHeart + $curHeart) >= $maxHeart) {
                $remain_sec = 0;

                if ($curHeart >= $maxHeart) {
                    $TotSP = $curHeart;
                    $sql = <<<SQL
UPDATE frdUserData set heartStartTime = now(), update_date=now()  WHERE userId = :userId 
SQL;
                    $db -> prepare($sql);
                    $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
                    $row = $db -> execute();
                    if (!isset($row) || is_null($row) || $row == 0) {
						$this -> logger -> logError(__FUNCTION__ . ': FAIL userId: ' . $userId . ', sql : ' . $sql);
                        $result['ResultCode'] = 300;
                        return $result;
                    }
                } else {
                    $TotSP = $maxHeart;
                    $sql = <<<SQL
UPDATE frdUserData set heart = :TotSP, heartStartTime = now(), update_date=now()  WHERE userId = :userId 
SQL;
                    $db -> prepare($sql);
                    $db -> bindValue(':TotSP', $TotSP, PDO::PARAM_INT);
                    $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
                    $row = $db -> execute();
                    if (!isset($row) || is_null($row) || $row == 0) {
						$this -> logger -> logError(__FUNCTION__ . ': FAIL userId: ' . $userId . ', sql : ' . $sql);
                        $result['ResultCode'] = 300;
                        return $result;
                    }
                }
            } else {

                $TotSP = $curHeart + $addHeart;

                $addTime = $addHeart * $check_pass_time;

                $remain_sec = $passTime % $check_pass_time;

                $sql = <<<SQL
UPDATE frdUserData 
set heart = :user_cur_sp , heartStartTime = TIMESTAMPADD(SECOND, :addTime, heartStartTime), update_date=now() WHERE userId = :userId 
SQL;
                $db -> prepare($sql);
                $db -> bindValue(':user_cur_sp', $TotSP, PDO::PARAM_INT);
                $db -> bindValue(':addTime', $addTime, PDO::PARAM_INT);
                $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
                $row = $db -> execute();
                if (!isset($row) || is_null($row) || $row == 0) {
					$this -> logger -> logError(__FUNCTION__ . ': FAIL userId: ' . $userId . ', sql : ' . $sql);
                    $result['ResultCode'] = 300;
                    return $result;
                }
            }

			$logDBSendManager = new LogDBSendManager();
			$logDBSendManager->sendAsset($userId, Type_Heart, $curHeart, $curHeart-$addHeart, $addHeart);
			
        } else {
            if ($curHeart >= $maxHeart) {
                $remain_sec = 0;
            } else {
                $remain_sec = $passTime;
            }
        }

        $result['ResultCode'] = 100;
        $result['heart'] = $TotSP;
        $result['passTime'] = $remain_sec;
        $result['maxHeart'] = $maxHeart;

        return $result;
    }


	public function addUserExp ($db, $userId, $amount) {
		$resultFail['ResultCode'] = 300;

		$exp =  null;

		$sql = "select userLevel, exp, heart from frdUserData where userId=:userId";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$this -> logger -> logError('AssetManager:: addPowder : SELECT FAIL userId :' . $userId);
			$result['ResultCode'] = 300;
			return $result;
		}

		$curLevel = $row['userLevel'];
		$curHeart = $row['heart'];
		$nextLevel = $curLevel+1;
		$myExp = $row['exp'];

		$curMyExp = $myExp + $amount;

		$myKey = $row['userLevel'];
		$lv_apc_key = 'UserExp_MaxHeart_'.$myKey;
		$lv_data = apc_fetch($lv_apc_key);
		$curMaxExp = $lv_data[1];
		$curMaxHeart = $lv_data[2];

		$myMaxExp = $curMaxExp;

		$preLevel = $curLevel;

		if ( $curMyExp >= $curMaxExp ) {

			$next_apc_key = 'UserExp_MaxHeart_'.$nextLevel;
			$next_lv_data = apc_fetch($next_apc_key);
			$nextMaxExp = $next_lv_data[1];
			$nextMaxHeart = $next_lv_data[2];

			$myMaxExp = $nextMaxExp;
			
			$resultExp = $curMyExp - $curMaxExp;
			if ($resultExp > $nextMaxExp ) {
				for ($i = 0; $i < 10; $i++) {
					$nextLevel = $nextLevel+1;
					$resultExp = $resultExp - $nextMaxExp;
					$next_apc_key = 'UserExp_MaxHeart_'.$nextLevel;
					$next_lv_data = apc_fetch($next_apc_key);
					$nextMaxExp = $next_lv_data[1];
					$nextMaxHeart = $next_lv_data[2];

					$myMaxExp = $nextMaxExp;

					if ($resultExp < $nextMaxExp ) {
						break;
					}
				}
			}

			$sql = "select val from frdEffectForEtc where userId = :userId and type=1032";
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$db -> execute();
			$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
			if (!$row) {
				$val = 0;
			} else {
				$val = $row['val'];
			}

			$bonusMaxHeart = round($curMaxHeart * $val*0.00001);
			$curMaxHeart += $bonusMaxHeart;

			$bonusMaxHeart = round($nextMaxHeart * $val*0.00001);
			$nextMaxHeart += $bonusMaxHeart;


			if ( $curMaxHeart < $nextMaxHeart ) {
				$sql = "UPDATE frdUserData SET heart=:heart WHERE userId=:userId";
				$db -> prepare($sql);
				$db -> bindValue(':heart', $nextMaxHeart, PDO::PARAM_INT);
				$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
				$row = $db -> execute();
				if (!isset($row) || is_null($row) || $row == 0) {
					$this->logger->logError('addUserExp : FAIL userId: ' . $userId . ', sql : ' . $sql);
					$result['ResultCode'] = 300;
					return $result;
				}
			}
			else {
				$data["curHeart"] = $curMaxHeart;
			}

			$curLevel = $nextLevel;
			$myMaxExp = $nextMaxExp;
		}

		if ( $curLevel != $preLevel) {
			if ($curHeart < $nextMaxHeart) {
				$curHeart = $nextMaxHeart;
			}

			if ($curLevel >= 65) { 
				$resultExp = 0;
				$curLevel = 65;
				$myMaxExp = 1000000;
				$amount = 0;
			}

			$sql = "UPDATE frdUserData 
			SET userLevel = :level, exp = :resultExp, maxHeart = :nextMaxHeart, heart = :curHeart
			WHERE userId=:userId";
			$db -> prepare($sql);
			$db -> bindValue(':level', $curLevel, PDO::PARAM_INT);
			$db -> bindValue(':resultExp', $resultExp, PDO::PARAM_INT);
			$db -> bindValue(':nextMaxHeart', $nextMaxHeart, PDO::PARAM_INT);
			$db -> bindValue(':curHeart', $curHeart, PDO::PARAM_INT);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError('addUserExp : FAIL userId: ' . $userId . ', sql : ' . $sql);
				$result['ResultCode'] = 300;
				return $result;
			}
			$exp['curLevel'] = $curLevel;
			$exp['preLevel'] = $preLevel;
			$exp['maxHeart'] = $nextMaxHeart;
			$exp['curHeart'] = $curHeart;

			$sql = "select achieveTableId, achieveId, cnt FROM frdAchieveInfo
				where userId=:userId and achieveId > 1100 and achieveId < 1200";
			$db->prepare($sql);
			$db->bindValue(':userId', $userId, PDO::PARAM_INT);
			$db -> execute();
			$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
			if ($row) {
				$achieveId = $row['achieveId'];
				if ($row['cnt'] != -1 ) {
					$AchieveManager = new AchieveManager();
					$AchResult = $AchieveManager->updateAchieveData($db, $userId, $achieveId, $curLevel);
				}
			}

			/*
			$achieveTemp = $preLevel + (5 - $preLevel%5);
			if ($preLevel < 65) {
				if ($curLevel >=$achieveTemp) {
					require_once './classes/AchieveManager.php';
					$AchieveManager  = new AchieveManager();

					$achieveId = (int)$achieveTemp/5;
					if ($achieveId > 10) {
						$achieveId = '11'.$achieveId;
					} else {
						$achieveId = '110'.$achieveId;
					}
						
					$sendAmount = $curLevel - $preLevel;
					$AchResult = $AchieveManager->updateAchieveData($db, $userId, $achieveId, $curLevel);
				}
			}
			*/
		} else {
			$resultExp = $curMyExp;
			if ($curLevel >= 65) { 
				$amount = 0;
				$curLevel = 65;
				$resultExp = 0;
				$myMaxExp = 1000000;
			}

			$sql = "UPDATE frdUserData SET exp=exp+:amount WHERE userId=:userId";
			$db -> prepare($sql);
			$db -> bindValue(':amount', $amount, PDO::PARAM_INT);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError('addUserExp : FAIL userId: ' . $userId . ', sql : ' . $sql);
				$result['ResultCode'] = 300;
				return $result;
			}
		}
		
		$exp['addExp'] = $amount;
		$exp['curExp'] = $resultExp;
		$exp['maxExp'] = $myMaxExp;


		$logDBSendManager = new LogDBSendManager();
		$logDBSendManager->sendAsset($userId, 5001, $myMaxExp, $resultExp, $amount);

		$result["exp"] = $exp;
		$result['ResultCode'] = 100;

		return $result;
	}

	function useSkillPoint ($db, $userId, $amount) {
        $resultFail['ResultCode'] = 300;

		$sql = <<<SQL
        SELECT skillPoint
        FROM   frdUserData
        WHERE  userId = :userId
SQL;
        $db -> prepare($sql);
        $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
        $db -> execute();
        $row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
        if (!$row) {
            $this -> logger -> logError('usp:: SELECT FAIL userId :' . $userId);
            $result['ResultCode'] = 300;
            return $result;
        }

		$gold = $row['skillPoint'];

		$myGold = $gold - $amount;

		if ( $myGold < 0 ) {
			$this->logger->logError('usp : over  userId :' . $userId . ' amount : ' .$amount);
            $result['ResultCode'] = 300;
            return $result;
		}


		$sql = <<<SQL
			UPDATE frdUserData 
			SET    skillPoint = :skillPoint, update_date = now()  
			WHERE  userId = :userId 
SQL;

		$db -> prepare($sql);
		$db -> bindValue(':skillPoint', $myGold, PDO::PARAM_INT);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this -> logger -> logError('usp: updateFail userId :' . $userId . ' sql : ' .$sql);
			return $resultFail;
		}

		$result['ResultCode'] = 100;
		$result['skillPoint'] = $myGold;


		$logDBSendManager = new LogDBSendManager();
		$logDBSendManager->sendAsset($userId, Type_Medal, $myGold-$amount, $myGold, $amount);
	
		return $result;

	}	
	function useMedalForAbil ($db, $userId, $amount) {
        $resultFail['ResultCode'] = 300;

		$sql = <<<SQL
        SELECT skillPoint, accuSkillPoint
        FROM   frdUserData
        WHERE  userId = :userId
SQL;
        $db -> prepare($sql);
        $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
        $db -> execute();
        $row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
        if (!$row) {
            $this -> logger -> logError('usp:: SELECT FAIL userId :' . $userId);
            $result['ResultCode'] = 300;
            return $result;
        }

		$gold = $row['skillPoint'];
		$accuSkillPoint = $row['accuSkillPoint'];

		$myAccu = $accuSkillPoint - $amount;
		$myGold = $gold - $amount;

		if ( $myGold < 0 || $myAccu < 0 ) {
			$this->logger->logError('usp : over  userId :' . $userId . ' amount : ' .$amount);
            $result['ResultCode'] = 300;
            return $result;
		}


		$sql = <<<SQL
			UPDATE frdUserData 
			SET    skillPoint = :skillPoint, 
			accuSkillPoint = :myAccu, update_date = now()  
			WHERE  userId = :userId 
SQL;

		$db -> prepare($sql);
		$db -> bindValue(':skillPoint', $myGold, PDO::PARAM_INT);
		$db -> bindValue(':myAccu', $myAccu, PDO::PARAM_INT);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this -> logger -> logError('usp: updateFail userId :' . $userId . ' sql : ' .$sql);
			return $resultFail;
		}

		$result['ResultCode'] = 100;
		$result['skillPoint'] = $myGold;

		$logDBSendManager = new LogDBSendManager();
		$logDBSendManager->sendAsset($userId, Type_Medal, $myGold-$amount, $myGold, $amount);
	
		return $result;
	}	



	function useTicket ($db, $userId, $amount) {
        $resultFail['ResultCode'] = 300;

		$sql = <<<SQL
        SELECT ticket
        FROM   frdUserData
        WHERE  userId = :userId
SQL;
        $db -> prepare($sql);
        $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
        $db -> execute();
        $row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
        if (!$row) {
            $this -> logger -> logError(__FUNCTION__.':: SELECT FAIL userId :' . $userId);
            $result['ResultCode'] = 300;
            return $result;
        }

		$ticket = $row['ticket'];

		$myTicket = $ticket - $amount;

		if ( $myTicket < 0 ) {
			$this->logger->logError(__FUNCTION__.' : over  userId :' . $userId . ' amount : ' .$amount);
            $result['ResultCode'] = 300;
            return $result;
		}


		$sql = <<<SQL
			UPDATE frdUserData 
			SET    ticket = :ticket, update_date = now()  
			WHERE  userId = :userId 
SQL;

		$db -> prepare($sql);
		$db -> bindValue(':ticket', $myTicket, PDO::PARAM_INT);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this -> logger -> logError(__FUNCTION__.':  userId :' . $userId . ' sql : ' .$sql);
			return $resultFail;
		}

		$logDBSendManager = new LogDBSendManager();
		$logDBSendManager->sendAsset($userId, Type_Ticket, $myTicket+$amount, $myTicket, 0-$amount);
	
		$result['ResultCode'] = 100;
		$result['ticket'] = $myTicket;

		return $result;

	}	


}
?>
