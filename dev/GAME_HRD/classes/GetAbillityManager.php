<?php
require_once './lib/Logger.php';

class GetAbillityManager {
	public function __construct() {
		$this -> logger = Logger::get();
	}

	function GetCantUnlockSlotValue() {
		$val= 9223372036854775807;  // 2^63 -1
		return $val;
	}

	function getAbillity($db, $userId, $questIdx, $goddessId, $data) {
		$resultFail['ResultCode'] = 300;

		$privateIdx = ($goddessId-1)*5+$questIdx;

		if ( $privateIdx < 63 ) {
			$bundleIdx = 0;
			$mask = 1 << $privateIdx;
			$bundleName = "bundle0";
		}
		else {
			$bundleIdx = 1;
			$mask = 1 << ($privateIdx-64);
			$bundleName = "bundle1";
		}

		$defultValue = $this->GetCantUnlockSlotValue();

		$sql = "SELECT bundle0, bundle1 FROM frdAbillity WHERE userId=:userId";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db->execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			//			$this -> logger -> logError('getAbiliity : fail SELECT userId :' . $userId );
			if ( $bundleIdx == 0 ) {
				$bundle0 = $mask;
				$bundle1 = 0;
				$sql = "INSERT INTO frdAbillity
					(userId, openedSlot, canUnlockSlot, bundle0, bundle1, usingBundle0,usingBundle1, reg_date )
					values (:userId, 0, :defaultValue, :mask, 0, 0, 0,now())";
				$db -> prepare($sql);
				$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
				$db -> bindValue(':defaultValue', $defaultValue, PDO::PARAM_INT);
				$db -> bindValue(':mask', $mask, PDO::PARAM_STRING);
				$row = $db -> execute();
				if (!isset($row) || is_null($row) || $row == 0) {
					$this -> logger -> logError('getAbillity : fail UPDATE userId :' . $userId );
					$this -> logger -> logError('sql : ' . $sql );
					return $resultFail;
				}
			}
			else {
				$bundle0 = 0;
				$bundle1 = $mask;
				$sql = "INSERT INTO frdAbillity
					(userId, openedSlot, canUnlockSlot, bundle0, bundle1, usingBundle0,usingBundle1, reg_date )
					values (:userId, 0, :defaultValue, 0, :mask, 0, 0,now())";
				$db -> prepare($sql);
				$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
				$db -> bindValue(':defaultValue', $defaultValue, PDO::PARAM_INT);
				$db -> bindValue(':mask', $mask, PDO::PARAM_STRING);
				$row = $db -> execute();
				if (!isset($row) || is_null($row) || $row == 0) {
					$this -> logger -> logError('getAbillity : fail  1 UPDATE userId :' . $userId );
					$this -> logger -> logError('sql : ' . $sql );
					return $resultFail;
				}

			}
		} else {
			$resultMask = $row[$bundleName] | $mask;

			$sql = "UPDATE frdAbillity SET ".$bundleName."=:resultMask where userId=:userId";
			$db -> prepare($sql);
			$db -> bindValue(':resultMask', $resultMask, PDO::PARAM_STR);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this -> logger -> logError('getAbillity : fail UPDATE userId :' . $userId );
				$this -> logger -> logError('sql : ' . $sql );
				return $resultFail;
			}

			$sql = "SELECT bundle0, bundle1 FROM frdAbillity WHERE userId=:userId";
			$db->prepare($sql);
			$db->bindValue(':userId', $userId, PDO::PARAM_INT);
			$db->execute();
			$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
			if (!$row) {
				$this -> logger -> logError('getAbillity : fail UPDATE userId :' . $userId );
				$this -> logger -> logError('sql : ' . $sql );
				return $resultFail;
			}
			$bundle0 = $row["bundle0"];
			$bundle1 = $row["bundle1"];

		}
		$result['ResultCode'] = 100;
		$result['bundle0'] = $bundle0;
		$result['bundle1'] = $bundle1;

		return $result;
	}
}
?>
