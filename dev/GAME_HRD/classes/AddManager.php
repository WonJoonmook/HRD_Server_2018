<?php
include_once './common/define.php';
require_once './lib/Logger.php';
require_once './classes/AchieveManager.php';
require_once './classes/DailyQuestManager.php';
require_once './classes/LogDBSendManager.php';
require_once './classes/EventManager.php';

class AddManager {

	public function __construct() {
		$this -> logger = Logger::get();
	}

	function addItem($db, $userId, $itemId, $cnt) {
		$sql = "SELECT itemTableId, itemCount 
				FROM frdHavingItems WHERE userId = :userId and itemId=:itemId";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db->bindValue(':itemId', $itemId, PDO::PARAM_INT);
		$db->execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {

			$sql = "  INSERT INTO frdHavingItems (
				`userId`,
				`itemId`,
				itemCount,
				reg_date
					) VALUES (
						:userId, :itemId, :cnt, now()
						)";
			$db -> prepare($sql);
			$db -> bindValue(':userId',$userId, PDO::PARAM_INT);
			$db -> bindValue(':itemId',$itemId, PDO::PARAM_INT);
			$db -> bindValue(':cnt',$cnt, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError('sql : ' . $sql. ' userId: ' .$userId );
				$this->logger->logError('sql : itemId: ' .$itemId );
				return $resultFail;
			}
			$itemTableId = $db->lastinsertID();
			$curCnt = 0;
		} else {
			$itemTableId = $row['itemTableId'];
			$curCnt = $row['itemCount'];
			$sql = "UPDATE frdHavingItems SET itemCount= itemCount + :cnt 
				WHERE itemTableId = :itemTableId AND userId = :userId";
			$db -> prepare($sql);
			$db -> bindValue(':cnt', $cnt, PDO::PARAM_INT);
			$db -> bindValue(':itemTableId', $itemTableId, PDO::PARAM_INT);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this -> logger -> logError('selItem : fail UPDATE userId :' . $userId );
				$this -> logger -> logError('sql : ' . $sql );
				return $resultFail;
			}
		}

		if ($itemId >= 101000 && $itemId <= 106009 ) {
			$DailyQuestManager = new DailyQuestManager();
			$DailyQuestManager->updateDayMission($db, $userId, 10, 1);
		} 

		$logDBSendManager = new LogDBSendManager();
		$logDBSendManager->sendItem($userId, $itemId, $curCnt, $curCnt+$cnt, $cnt);


		$result['ResultCode'] = 100;
		$result['itemId'] = $itemId;
		$result['itemTableId'] = $itemTableId;
		$result['cnt'] = $curCnt + $cnt;
		return $result;
	}

	function addWeapon($db, $userId, $weaponId, $exp) {
		$resultFail['ResultCode'] = 300;
		if ($exp == null) {
			$exp = 0;
		}

		$apc_key = 'WeaponOption_'.$weaponId;
		$apc_data = apc_fetch($apc_key);
		if (!$apc_data) {
			$this->logger->logError(__FUNCTION__.' : APC Wrong userId : '.$userId.", weaponId : ".$weaponId);
			return $resultFail;
		}

		$statId0 = 0;
		$statVal0 = 0;
		$statId1 = 0;
		$statVal1 = 0;
		$statId2 = 0;
		$statVal2 = 0;
		$statId3 = 0;
		$statVal3 = 0;
		$statId4 = 0;
		$statVal4 = 0;

		$idNo0 = (int)$apc_data[1];
		$idNo1 = (int)$apc_data[4];
		$idNo2 = (int)$apc_data[7];
		$idNo3 = (int)$apc_data[10];

		if (isset($apc_data[13])) {
			$idNo4 = (int)$apc_data[13];
		} else {
			$idNo4 = 0;
		}


		$RanMaxArr = array ( 
			0, 					
			14, 				//101
			14, 				//102
			14, 				//103
			6, 					//104
			6, 					//105
			19 					//106
		);

		if ($idNo0) {
			if ( $idNo0 > 100 )
				$statId0 = rand( 0, $RanMaxArr[$idNo0-100] );
			else
				$statId0 = $idNo0;
		}
		$statVal0 = rand(0, 250);

		if ($idNo1 ) {
			if ( $idNo1 > 100 )
				$statId1 = rand( 0, $RanMaxArr[$idNo1-100] );
			else
				$statId1 = $idNo1;
		}
		$statVal1 = rand(0, 250);

		if ($idNo2 ) {
			if ( $idNo2 > 100 )
				$statId2 = rand( 0, $RanMaxArr[$idNo2-100] );
			else
				$statId2 = $idNo2;
		}
		$statVal2 = rand(0, 250);

		if ($idNo3 ) {
			if ( $idNo3 > 100 )
				$statId3 = rand( 0, $RanMaxArr[$idNo3-100] );
			else
				$statId3 = $idNo3;
		}
		$statVal3 = rand(0, 250);

		if ($idNo4) {
			if ( $idNo4 > 100 )
				$statId4 = rand( 0, $RanMaxArr[$idNo4-100] );
			else
				$statId4 = $idNo4;
		}
		$statVal4 = rand(0, 250);

		$level = 1;
		if ($weaponId > 389 && $weaponId < 399 ) {
			$statVal0 = 250;
			$statVal1 = 250;
			$statVal2 = 250;
			$statVal3 = 250;
			$statVal4 = 250;
			$level = 50;
		}
		$sql = "
			INSERT  INTO frdHavingWeapons
			( userId, weaponId, exp, level,
			  statId0, statId1, statId2, statId3,  statId4,
			  statVal0, statVal1, statVal2,statVal3,  statVal4,
				transcend, transcendExp,
				reg_date )
			VALUES (:userId, :weaponId, :exp, :level,
					:statId0,:statId1,:statId2,:statId3,:statId4,
					:statVal0,:statVal1,:statVal2,:statVal3,:statVal4,0,0,now()
				   )
			";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> bindValue(':weaponId', $weaponId, PDO::PARAM_INT);
		$db -> bindValue(':exp', $exp, PDO::PARAM_INT);
		$db -> bindValue(':level', $level, PDO::PARAM_INT);
		$db -> bindValue(':statId0', $statId0, PDO::PARAM_INT);
		$db -> bindValue(':statId1', $statId1, PDO::PARAM_INT);
		$db -> bindValue(':statId2', $statId2, PDO::PARAM_INT);
		$db -> bindValue(':statId3', $statId3, PDO::PARAM_INT);
		$db -> bindValue(':statId4', $statId4, PDO::PARAM_INT);
		$db -> bindValue(':statVal0', $statVal0, PDO::PARAM_INT);
		$db -> bindValue(':statVal1', $statVal1, PDO::PARAM_INT);
		$db -> bindValue(':statVal2', $statVal2, PDO::PARAM_INT);
		$db -> bindValue(':statVal3', $statVal3, PDO::PARAM_INT);
		$db -> bindValue(':statVal4', $statVal4, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this -> logger -> logError(__FUNCTION__.' : FAIL userId : '.$userId.", sql : ".$sql);
			$this -> logger -> logError(__FUNCTION__.' :  weaponId : '.$weaponId);
			return $resultFail;
		}

		$weaponTableId = $db->lastInsertId();	

		$result['ResultCode'] = 100;
		$result['weaponId'] = $weaponId;
		$result['weaponTableId'] = $weaponTableId;
		$statIds = null;
		$statVals = null;
		$statIds[]  = $statId0;
		$statVals[] = $statVal0;
		$statIds[]  = $statId1;
		$statVals[] = $statVal1;
		$statIds[]  = $statId2;
		$statVals[] = $statVal2;
		$statIds[]  = $statId3;
		$statVals[] = $statVal3;
		$statIds[]  = $statId4;
		$statVals[] = $statVal4;

		$result['level']    = $level;
		$result['statIds']    = $statIds;
		$result['statVals']   = $statVals;

		$result['exp'] = 0;

		$sql = "select achieveTableId, achieveId, cnt FROM frdAchieveInfo
			where userId=:userId and achieveId > 3100 and achieveId < 3200";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if ($row) {
			$achieveId = $row['achieveId'];
			if ($row['cnt'] != -1 ) {
				$AchieveManager = new AchieveManager();
				$AchieveManager->addAchieveData($db, $userId, $achieveId, 1);
			}
		}

		$logDBSendManager = new LogDBSendManager();
		$logDBSendManager->sendWeapon($userId, $weaponId, 1, 1, 0, $statId0, $statId1, $statId2, $statId3, $statVal0, $statVal1, $statVal2, $statVal3);


		return $result;
	}

	function addMagic($db, $userId, $magicId) {
		$resultFail['ResultCode'] = 300;
		
		if ($magicId == 690 ) {
			$level = 5;
		} else {
			$level = 0;
		}
		
		$this -> logger -> logError(__FUNCTION__.' :  userId : '.$userId.", magicId : ".$magicId);
		$sql =" 
			INSERT  INTO frdHavingMagics
			( userId, magicId, level,exp, reg_date )
			VALUES (:userId, :magicId, :level, 0, now()) ";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> bindValue(':magicId', $magicId, PDO::PARAM_INT);
		$db -> bindValue(':level', $level, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this -> logger -> logError(__FUNCTION__.' : FAIL userId : '.$userId.", sql : ".$sql);
			$this -> logger -> logError(__FUNCTION__.' : FAIL userId : '.$userId.", magicId : ".$magicId);
			return $resultFail;
		}

		$magicTableId = $db->lastinsertID();


		$sql = "select achieveTableId, achieveId, cnt FROM frdAchieveInfo
			where userId=:userId and achieveId > 3200 and achieveId < 3300";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if ($row) {
			$achieveId = $row['achieveId'];
			if ($row['cnt'] != -1 ) {
				$AchieveManager = new AchieveManager();
				$AchieveManager->addAchieveData($db, $userId, $achieveId, 1);
			}
		}

		$logDBSendManager = new LogDBSendManager();
		$logDBSendManager->sendMagic($userId, $magicId, 1, $level);
		

		$result['ResultCode'] = 100;
		$result['magicId'] = $magicId;
		$result['magicTableId'] = $magicTableId;

		return $result;
	}

	function addWeaponExp($db, $userId, $weaponIdxs, $resultExp, $type) {
		$resultFail['ResultCode'] = 300;

		$EventManager = new EventManager();
		if ( $EventManager->IsActiveEvent(EVENT_DOUBLE_WEAPONEXP) ) {
			$resultExp = $resultExp*2;
		}
		// 기획의도 10% 고정으로 적용
		$weaponExp = (int)($resultExp/10);
	
		$wLen = count($weaponIdxs);
		$sql_str = null;

		$temp = null;
		if (@$isSub) {
			$weaponExp = round($weaponExp*0.3);
		}

		for($i =0; $i < $wLen; $i++) {
			if($sql_str == null) {
				$sql_str = $weaponIdxs[$i];
			} else {
				$sql_str = $sql_str.','.$weaponIdxs[$i];
			}				
		}
		$sql = "SELECT weaponTableId, weaponId, transcend, level, exp
			FROM frdHavingWeapons WHERE weaponTableId IN ($sql_str) AND userId = :userId ";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$resultList = null;
		while ($row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
			$resultList[] = $row;
		}

		if ($wLen != count($resultList)) {
			$this->logger->logError(__FUNCTION__.': userId :'.$userId." not Match ");
			$resultFail['ResultCode'] = 300;
		}

		if ($type) {
			$sql = "select val from frdEffectForEtc where userId = :userId and type=1032";
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$db -> execute();
			$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
			if (!$row) {
				$val = 1;
			} else {
				$val = $row['val'];
			}
		} else {
			$val = 1;
		}

		for($i =0; $i < count($resultList); $i++) {

			$exp =  null;
			//Level,WeaponPrice,WeaponNeedExp

			$wi = $resultList[$i];
			$weaponId = (int)$wi['weaponId'];
			$apc_key = "WeaponPrice_Exp_".$wi['level'];
			$apc_data = apc_fetch($apc_key);
			$curLevel = $wi['level'];
			$curMyExp = $wi['exp']+ $weaponExp;

			$curMaxExp = $apc_data[3];
			$myMaxExp = $curMaxExp;
			$preLevel = $curLevel;

			if ($weaponId >= 390 ) {			//grade 7 legend weapon
				$exp[] = $wi['weaponTableId'];
				$exp[] = $weaponExp;
				$exp[] = $myMaxExp;
				$exp[] = $curLevel;
				$exp[] = $preLevel;
				$temp[] = $exp;
				continue;
			}

			$resultExp = $curMyExp;

			if ( $curMyExp >= $curMaxExp ) {
				$next_key = $wi['level'] + 1;
				$apc_key = "WeaponPrice_Exp_".$next_key;
				$apc_data = apc_fetch($apc_key);
				$nextMaxExp = $apc_data[3];

				$myMaxExp = $nextMaxExp;

				$resultExp = $curMyExp - $curMaxExp;
//				$this->logger->logError('addWeaponExp :  next : ' . $nextMaxExp . ", my : " . $resultExp);
				if ($resultExp >= $nextMaxExp ) {
					for ($j = 0; $j < 40; $j++) {
						$next_key = $next_key+1;
						$resultExp = $resultExp - $nextMaxExp;
						$next_apc_key = "WeaponPrice_Exp_".$next_key;
						$next_lv_data = apc_fetch($next_apc_key);
						$nextMaxExp = $next_lv_data[3];

						$myMaxExp = $nextMaxExp;
						if ($resultExp < $nextMaxExp ) {
							break;
						}
					}
				}

				$curLevel = $next_key;
				$myMaxExp = $nextMaxExp;
			}

			if ( $curLevel != $preLevel) {
				$grade = $this->GetWeaponGrade($weaponId);
				$apc_key = "WeaponInform_WeaponMexLevel";
				$max_lv_temp = apc_fetch($apc_key);
				$max_lv_data = explode("/",$max_lv_temp[1]);	
				if (count($max_lv_data)) {
					if ( $grade == 6 ) {
						$limitLV = min(40 + 2 * (int)$wi['transcend'], 50);// transcend5 = transcend6 = 50
					}
					else {
						$limitLV = $max_lv_data[$grade];
					}

					if ($curLevel >= $limitLV) {
						$curLevel = $limitLV;
						$resultExp = 0;
					}
				}

				$sql = "UPDATE frdHavingWeapons SET 
					exp = :weaponExp , level = :level, update_date = now()
					WHERE weaponTableId = :pId and userId=:userId";
				$db -> prepare($sql);
				$db -> bindValue(':weaponExp', $resultExp, PDO::PARAM_INT);
				$db -> bindValue(':level', $curLevel, PDO::PARAM_INT);
				$db -> bindValue(':pId', $wi['weaponTableId'], PDO::PARAM_INT);
				$db -> bindValue(':userId', $userId, PDO::PARAM_INT);

			} else {
				$sql = "UPDATE frdHavingWeapons SET 
					exp = exp + :weaponExp, update_date = now()
					WHERE weaponTableId = :pId and userId=:userId";
				$db -> prepare($sql);
				$db -> bindValue(':weaponExp', $weaponExp, PDO::PARAM_INT);
				$db -> bindValue(':pId', $wi['weaponTableId'], PDO::PARAM_INT);
				$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			}
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError('addWeaponExp : FAIL userId : ' . $userId . ", sql : " . $sql);
				return $resultFail;
			}
			$exp = null;
			$exp[] = $wi['weaponTableId'];
			$exp[] = $weaponExp;
			$exp[] = $resultExp;
			$exp[] = $curLevel;
			$exp[] = $preLevel;
			$temp[] = $exp;
		}
		$result['exp'] = $temp;
		$result['ResultCode'] = 100;
		return $result;
	}

	function setAddHero($db, $userId, $charId, $amount){
		$resultFail['ResultCode'] = 300;
		$sql = "
			SELECT charTableId, exp FROM frdCharInfo 
			WHERE userId=:userId and charId=:charId";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> bindValue(':charId', $charId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			if ($amount < 1) {
				$amount = 1;
			}
			$sql = "
				INSERT INTO frdCharInfo 
				(userId, charId, exp, reg_date)
				VALUES( :userId, :charId, :amount, now()) ";
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$db -> bindValue(':charId', $charId, PDO::PARAM_INT);
			$db -> bindValue(':amount', $amount, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError(__FUNCTION__.': FAIL userId : '.$userId.", sql : ".$sql);
				$this->logger->logError(__FUNCTION__.': FAIL userId : '.$userId.", charId : ".$charId);
				$this->logger->logError(__FUNCTION__.': FAIL userId : '.$userId.", amount : ".$amount);
				return $resultFail;
			}
			$charTableId =  $db -> lastInsertId();
			$exp = 0;
			$myExp = $amount;

		} else {
			$charTableId = $row['charTableId'];
			$exp = $row['exp'];
			if ($amount == null ) {
				// shop chest only
				$amount = 5;
			}
			$sql = "
				UPDATE frdCharInfo SET exp = exp + :amount, update_date = now()
				WHERE charTableId = :charTableId and userId = :userId  ";
			$db -> prepare($sql);
			$db -> bindValue(':amount', $amount, PDO::PARAM_INT);
			$db -> bindValue(':charTableId', $charTableId, PDO::PARAM_INT);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError(__FUNCTION__.': FAIL userId : '.$userId.", sql : ".$sql);
				return $resultFail;
			}
			$myExp = $exp + $amount;
			$exp = $exp + $amount;
		}
		// 랭크 업이 되면 검사를 해야 한다.
		// C 랭크는 검사 하지 않는다.

		if ( $myExp >= 30) {
			//$this->achieveRank($db, $userId, $exp, $amount);
			$this->achieveRankState($db, $userId);
		}


		$logDBSendManager = new LogDBSendManager();
		$logDBSendManager->sendChar($userId, $charId, $myExp - $amount, $myExp, $amount);
		

		$data = null;
		$result['charTableId'] = $charTableId;
		$result['charId'] = $charId;
		$result['curExp'] = $myExp;
		$result['addExp'] = $amount;

		$result['ResultCode'] = 100;
		$result['Data'] = $data;

		return $result;
	}

	function GetWeaponGrade($privateId) {
		switch($privateId) {
			case 498:
				return 1;
			case 497:
				return 2;
			case 496:
				return 3;
			case 495:
				return 4;
			default:
				if ( $privateId < 10 )
					return 1;
				else if ( $privateId < 50 )
					return 2;
				else if ( $privateId < 100 )
					return 3;
				else if ( $privateId < 200 )
					return 4;
				else if ( $privateId < 300 )
					return 5;
				else if ( $privateId < 390 )
					return 6;
				else
					return 7;
		}
	}

	function achieveRank($db, $userId, $exp, $amount) {
		// 이전 랭크가 랭크 업 전이고
		// 현재 랭크가 랭크업 상태다 이때 업적 검사를 한다.
		// 랭크 업 단계는 
		// 10 : C
		// 30 : B
		// 100 : A
		// 254 : S
		// 해당 내용은 누적이다. 
		$achieveTemp  = array(0,10,30,100,254,999999);

		// 일단 b랭크 이상을 조건으로 시작 
		// 신규 케릭터는 타지 않게 된다. 
		// 업적 체크 시작 ---> 

		//이전 값을 재 계산. 
		$aVal = $exp - $amount;
		// exp 는 현재 값

		// 랭크 변화가 있는지 검사 한다.
		$preRank = null;
		$curRank = null;
		for($ai=0; $ai < count($achieveTemp)-1; $ai++) {
			if ($aVal >= $achieveTemp[$ai] && $achieveTemp[$ai+1] > $aVal) {
				$preRank = $ai;
			}
			if ($exp >= $achieveTemp[$ai] && $achieveTemp[$ai+1] > $exp) {
				$curRank = $ai;
			}
		}

		// 랭크 변화가 감지 되었다!
		if ($preRank == null || $curRank == null )  {
			return true;
		}

		/*
		if ($preRank != $curRank) {
		}
		*/
		// 0 : C
		// 1 : B
		// 2 : A
		// 3 : S
		switch($curRank) {
			// 신규 B 랭크 가 입장 했다.
			case 2:
				$this->achieveRankState($db, $userId, 1700,1800, $curRank);
				break;
				// 신규 A 랭크 가 입장 했다.
			case 3:
				$this->achieveRankState($db, $userId, 1800,1900, $curRank);
				break;
				// 신규 S 랭크 가 입장 했다.
			case 4:
				$this->achieveRankState($db, $userId, 1900,2000, $curRank);
				break;
			default:
				// 해당 없음 종료 
				return true;
		}

		return true;
	}

	function achieveRankState($db, $userId) {
		$resultFail['ResultCode'] = 300;

		$sql = "SELECT exp FROM frdCharInfo WHERE userId=:userId ";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db->execute();
		$aCharTemp   = null;
		while ($row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
			$aCharTemp[] = $row;
		}

		$brCnt = 0;
		$arCnt = 0;
		$srCnt = 0;

		foreach ($aCharTemp as $acr) {
			if ($acr['exp'] >= 30 ) {
				$brCnt++;
			}
			if ($acr['exp'] >= 100 ) {
				$arCnt++;
			}
			if ($acr['exp'] >= 254 ) {
				$srCnt++;
			}
		}
		$AchieveManager = new AchieveManager();

		$sql = "select achieveId, cnt from frdAchieveInfo where userId=:userId and achieveId > 1700 AND achieveId < 1800";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$this->logger->logError(__FUNCTION__.': FAIL userId : '.$userId.", sql : ".$sql);
			return $resultFail;
		}
		$achieveId = $row['achieveId'];
		$myCnt = $row['cnt'];

		if ($myCnt == -1 ) {
			$this->logger->logError(__FUNCTION__.': no process 1700 userId : '.$userId.", myCnt : ".$myCnt);
		} else {
			$AchieveManager->updateAchieveData($db, $userId, $achieveId, $brCnt);
		}

		$sql = "select achieveId, cnt from frdAchieveInfo where userId=:userId and achieveId > 1800 AND achieveId < 1900";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$this->logger->logError(__FUNCTION__.': FAIL userId : '.$userId.", sql : ".$sql);
			return $resultFail;
		}
		$achieveId = $row['achieveId'];
		$myCnt = $row['cnt'];

		if ($myCnt == -1 ) {
			$this->logger->logError(__FUNCTION__.': no process 1800 userId : '.$userId.", myCnt : ".$myCnt);
		} else {
			$AchieveManager->updateAchieveData($db, $userId, $achieveId, $arCnt);
		}

		$sql = "select achieveId, cnt from frdAchieveInfo where userId=:userId and achieveId > 1900 AND achieveId < 2000";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$this->logger->logError(__FUNCTION__.': FAIL userId : '.$userId.", sql : ".$sql);
			return $resultFail;
		}
		$achieveId = $row['achieveId'];
		$myCnt = $row['cnt'];
		if ($myCnt == -1 ) {
			$this->logger->logError(__FUNCTION__.': no process 1900 userId : '.$userId.", myCnt : ".$myCnt);
		} else {
			$AchieveManager->updateAchieveData($db, $userId, $achieveId, $srCnt);
		}

		return true;
	}

	function achieveRankState_back($db, $userId, $min, $max,$curRank) {
		// 내 업적 상태값이 현재 어떤지 가져 옴	
		$sql = "select achieveId, cnt from frdAchieveInfo where userId=:userId and achieveId > ".$min." and achieveId < ".$max;
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$this->logger->logError(__FUNCTION__.': FAIL userId : '.$userId.", sql : ".$sql);
			return $resultFail;
		}

		$achieveId = $row['achieveId'];
		$this->logger->logError(__FUNCTION__.':  userId : '.$userId.", achieveId : ".$achieveId);
		$achieveCnt = $row['cnt'];
		$myCnt = $row['cnt'];

		if ($myCnt == -1 ) {
			$this->logger->logError(__FUNCTION__.': no process userId : '.$userId.", myCnt : ".$myCnt);
			return $resultFail;
		}

		$sql = "SELECT exp FROM frdCharInfo WHERE userId=:userId ";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db->execute();
		$aCharTemp   = null;
		while ($row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
			$aCharTemp[] = $row;
		}

		$brCnt = 0;
		$arCnt = 0;
		$srCnt = 0;


		foreach ($aCharTemp as $acr) {

			$this->logger->logError(__FUNCTION__.':  userId : '.$userId.", acrExp : ".$acr['exp']);

			if ($acr['exp'] >= 30 ) {
				$brCnt++;
			}
			if ($acr['exp'] >= 100 ) {
				$arCnt++;
			}
			if ($acr['exp'] >= 254 ) {
				$srCnt++;
			}
		}

		$this->logger->logError(__FUNCTION__.':  userId : '.$userId.", brCnt : ".$brCnt);
		$this->logger->logError(__FUNCTION__.':  userId : '.$userId.", arCnt : ".$arCnt);
		$this->logger->logError(__FUNCTION__.':  userId : '.$userId.", srCnt : ".$srCnt);

		switch($curRank) {
			case 2:
				$cnt = $brCnt;
				break;
			case 3:
				$cnt = $arCnt;
				break;
			case 4:
				$cnt = $srCnt;
				break;
			default:
				return true;
		}

		$AchieveManager = new AchieveManager();
		$AchieveManager->updateAchieveData($db, $userId, $achieveId, $cnt);

		return true;
	}

}
?>
