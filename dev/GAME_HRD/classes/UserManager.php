<?php
include_once './common/DB.php';
include_once './common/RedisDB.php';
include_once './common/LOGIN_DB.php';
require_once './lib/Logger.php';
require_once './classes/LogDBSendManager.php';

class UserManager {

    public function __construct() {
        $this -> logger = Logger::get();
    }

	function getRedisName() {
		switch($GLOBALS['Type']) {
			case 'OS':
			case 'AS':
				$climbName = "climbRank_2";
				break;
			case 'PS':
				$climbName = "climbRank_New";
				break;
			default:
				$climbName = "climbRank_Error";
				break;
		}

		return $climbName;
	}	

	function SetName ($param) {

		$userId = $param['userId'];
		$name 	= $param['name'];	

		$db = new DB();
		// 튜토리얼 인지 체크 
		$sql = "select userId, tutoLevel,name from frdUserData where userId=:userId";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$this -> logger -> logError(__FUNCTION__.': EXISIT userId :'.$userId.' name :'.$name);
			$result['ResultCode'] = 300;
			return $result;
		}
		$tutoLevel = $row['tutoLevel'];	
		$preName = $row['name'];	
			
		$isDelete = false;
		$isTuto = false;
		$useItem = false;
		if ($tutoLevel < 2) {
			$tutoLevel = 2;	
			$isTuto = true;
			$data['tutoLevel'] = $tutoLevel;
		} else { 
			// 튜토리얼이 아니기 때문에 수량을 확인 한다.
			$sql = "select itemId, itemCount from frdHavingItems where userId=:userId AND itemId = 100005";
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$db -> execute();
			$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
			if (!$row) {
				$this->logger->logError(__FUNCTION__.': Need  100005 userId :'.$userId.' name :'.$name);
				$result['ResultCode'] = 309;
				return $result;
			}
			$itemId = $row['itemId'];
			$itemCount = $row['itemCount'];
			$data['itemId'] = $itemId;


			if ($row['itemCount'] == 1) {
				//delete 예약 
				$isDelete = true;
				$itemCount = 0;
			} else {
				$itemCount--;
			}
			$data['itemCount'] = $itemCount;

			$logDBSendManager = new LogDBSendManager();
			$logDBSendManager->sendItem($userId, 100005, $itemCount+1, $itemCount, -1);
		}

		// 내디비에서 체크 
		$sql = "select userId, tutoLevel from frdUserData where name=:name";
		$db -> prepare($sql);
		$db -> bindValue(':name', $name, PDO::PARAM_STR);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if ($row) {
			$this -> logger -> logError(__FUNCTION__.': EXISIT 1 userId :'.$userId.' name :'.$name);
			$result['ResultCode'] = 100;
			$data['isExist'] = true;
			$result['Data'] = $data;
			return $result;
		}

		// 로그인 디비에서 체크
		$login_db = new LOGIN_DB();
		$sql = "select userId from t_Account_User where name=:name";
		$login_db -> prepare($sql);
		$login_db -> bindValue(':name', $name, PDO::PARAM_STR);
		$login_db -> execute();
		$row = $login_db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if ($row) {
			$this -> logger -> logError(__FUNCTION__.': EXISIT 2 userId :'.$userId.' name :'.$name);
			$result['ResultCode'] = 100;
			$data['isExist'] = true;
			$result['Data'] = $data;
			return $result;
		}

		$sql = "UPDATE t_Account_User SET name = :name WHERE userId=:userId";
		$login_db -> prepare($sql);
		$login_db -> bindValue(':name', $name, PDO::PARAM_STR);
		$login_db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$row = $login_db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this->logger->logError(__FUNCTION__.': FAIL userId :'.$userId.' sql :'.$sql.':name'.$name);
			$result['ResultCode'] = 300;
			return $result;
		}

		if ($isTuto) {
			$sql = "UPDATE frdUserData SET name=:name, tutoLevel = :tutoLevel WHERE userId=:userId";
			$db -> prepare($sql);
			$db -> bindValue(':name', $name, PDO::PARAM_STR);
			$db -> bindValue(':tutoLevel', $tutoLevel, PDO::PARAM_STR);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		} else {
			$sql = "UPDATE frdUserData SET name=:name WHERE userId=:userId";
			$db -> prepare($sql);
			$db -> bindValue(':name', $name, PDO::PARAM_STR);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		}
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this->logger->logError(__FUNCTION__.': FAIL userId :'.$userId.' sql :'.$sql.':name'.$name);
			$result['ResultCode'] = 300;
			return $result;
		}

		// 닉변권 수 반환
		if ($isDelete) {
			$sql = "DELETE FROM frdHavingItems 
				WHERE userId=:userId AND itemId = 100005";
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError(__FUNCTION__.': FAIL userId :'.$userId.' sql :'.$sql);
				$result['ResultCode'] = 300;
				return $result;
			}
			// 삭제 백업 
		} else {
			$sql = "UPDATE frdHavingItems 
				SET itemCount= itemCount - 1, update_date = now() 
				WHERE userId=:userId AND itemId = 100005";
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError(__FUNCTION__.': FAIL userId :'.$userId.' sql :'.$sql);
				$result['ResultCode'] = 300;
				return $result;
			}
		}

		$climbName = $this->getRedisName();

		if (!$isTuto) {
			$rankRedis = new RedisServer();
			$climbTopScore = $rankRedis->zScore($climbName, $preName);
			$climbTopInform = $rankRedis->hGet('climbInform', $preName);

			$pp = $rankRedis->zAdd($climbName, $climbTopScore, $name);
			$aa = $rankRedis->hSet('climbInform', $name, $climbTopInform);
			$rr = $rankRedis->zRem($climbName, $preName);
			$ss = $rankRedis->hDel('climbInform', $preName);

			$climbSpecDatas = $rankRedis->hGet('climbSpecDatas', $preName);
			$climbScoreDatas = $rankRedis->hGet('climbScoreDatas', $preName);
			$rankRedis->hSet('climbSpecDatas', $name, $climbSpecDatas);
			$rankRedis->hSet('climbScoreDatas', $name, $climbScoreDatas);
			$rankRedis->hSet('climbUserId', $name, $userId);

			$rankRedis->hDel('climbScoreDatas', $preName);
			$rankRedis->hDel('climbSpecDatas', $preName);
		}

		$result['ResultCode']  = 100;
		$result['Protocol']  = 'ResSetNick';
		
		$data['isExist'] = false;
		$result['Data'] = $data;
		return $result;
	}

	 function TutoLevel ($param) {

		$userId = $param['userId'];
		$tutoLevel 	= $param['tutoLevel'];	

		$db = new DB();
		// 튜토리얼 인지 체크 
		$sql = "select userId, tutoLevel from frdUserData where userId=:userId";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$this -> logger -> logError(__FUNCTION__.': EXISIT userId :'.$userId.' sql :'.$sql);
			$result['ResultCode'] = 300;
			return $result;
		}
		
		if ($tutoLevel > $row['tutoLevel'])	 {
			$sql = "UPDATE frdUserData SET tutoLevel = :tutoLevel WHERE userId=:userId";
			$db -> prepare($sql);
			$db -> bindValue(':tutoLevel', $tutoLevel, PDO::PARAM_STR);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError(__FUNCTION__.': FAIL userId :'.$userId.' sql :'.$sql);
				$result['ResultCode'] = 300;
				return $result;
			}
		} else {
			$this->logger->logError(__FUNCTION__.': FAIL TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT userId :'.$userId.' tutoLevel :'.$tutoLevel);
		}

		$result['ResultCode']  = 100;
		$result['Protocol']  = 'ResTutoLevel';
		
		$data['tutoLevel'] = $tutoLevel;
		$result['Data'] = $data;
		return $result;
	}
}
?>
