<?php
include_once './common/DB.php';
require_once './lib/Logger.php';
require_once './classes/AssetManager.php';

class SkillManager {
	public function __construct() {
		$this -> logger = Logger::get();
	}

	public function ResetSkillPoint($param) {
		$resultFail['ResultCode'] = 300;
		$userId = $param["userId"];

		$price 	= 50; // HARD
		$itemId = 100001; // HARD

		$db = new DB();
		// 내가 가진. 스킬 포인트와 보석 수량 확인.
		$sql = "SELECT skillPoint, accuSkillPoint, jewel  FROM frdUserData WHERE userId = :userId";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);

		if (!isset($row) || is_null($row) || $row == 0) {
			$this->logger->logError('resetSkillPoint : FAIL userId : ' . $userId . ", sql : " . $sql);
			return $resultFail;
		}

		$myJewel = $row['jewel'];
		// 없으면 보석으로 하기 위해 보석에서 차감 준비 
		$accuSkillPoint = $row['accuSkillPoint'];

		// 초기화 권이 있는지 확인. 
		$sql = "SELECT itemCount FROM frdHavingItems WHERE userId = :userId and itemId=:itemId";
		$db -> prepare($sql);
		$db -> bindValue(':itemId', $itemId, PDO::PARAM_INT);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			if ($myJewel < $price) {
				$this->logger->logError(__FUNCTION__.':not enough jewel : '.$userId.' my:'.$myJewel.'price :'.$price );
				return $resultFail;
			}
			$AssetManager = new AssetManager();
			$AssetResult = $AssetManager->useJewel($db,$userId, $price);
			if($AssetResult['ResultCode'] != 100) {
				return $resultFail;
			}
			$myJewel = $AssetResult['jewel'];
			$jewel = null;
			$jewel['curJewel'] = $myJewel; // 보석 수량 
			$jewel['addJewel'] = 0 - $price; // 보석 수량 
		
			$data['use']['jewel'] = $jewel;
			// frdUserData 에서 가져온 accuSkillPoint 로 업데이트 
			$sql = "UPDATE frdUserData SET skillPoint=:mySP where userId=:userId";
			$db -> prepare($sql);
			$db -> bindValue(':mySP', $accuSkillPoint, PDO::PARAM_INT);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError('resetSkillPoint : FAIL userId : '.$userId.", sql : ".$sql);
				return $resultFail;
			}
			// 스킬 레벨을 가진 테이블을 전부 삭제. 
			$sql = "DELETE FROM frdSkillLevels where userId=:userId";
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError('resetSkillPoint : FAIL userId : '.$userId.", sql : ".$sql);
				return $resultFail;
			}

			// 증가 효과 관련 기록 테이블도 같이 삭제 .
			$sql = "delete from frdEffectForPreCheckRewards where userId=:userId";
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError('resetSkillPoint : FAIL userId : '.$userId.", sql : ".$sql);
				return $resultFail;
			}
			$sql = "delete from frdEffectForRewardsSP where userId=:userId"; 
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError('resetSkillPoint : FAIL userId : '.$userId.", sql : ".$sql);
				return $resultFail;
			}


		} else {
			// 스킬 초기화 권이 있으면 사용량을 감소 
			$myItemCount = (int)$row["itemCount"] -1;
			// 보유량이 0 보다 작으면 아이템을 삭제 
			if ( $myItemCount <= 0 )  {
				$sql = "DELETE FROM frdHavingItems WHERE userId=:userId AND itemId=:itemId";
				$db -> prepare($sql);
				$db -> bindValue(':itemId', $itemId, PDO::PARAM_INT);
				$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
				$row = $db -> execute();
				if (!isset($row) || is_null($row) || $row == 0) {
					$this->logger->logError('resetSkillPoint : FAIL userId : '.$userId.", sql : ".$sql);
					return $resultFail;

				} 
			} else {
				// 보유량이 0보다 크면 변경 수량으로 업데이트 
				$sql = "UPDATE frdHavingItems SET itemCount=:myItemCount 
					WHERE userId=:userId AND itemId=:itemId";
				$db -> prepare($sql);
				$db -> bindValue(':myItemCount', $myItemCount, PDO::PARAM_INT);
				$db -> bindValue(':itemId', $itemId, PDO::PARAM_INT);
				$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
				$row = $db -> execute();
				if (!isset($row) || is_null($row) || $row == 0) {
					$this->logger->logError('resetSkillPoint : FAIL userId : '.$userId.", sql : ".$sql);
					return $resultFail;
				}
			}
			$items = null;
			$items['itemId'] = $itemId;
			$items['cur'] = $myItemCount; // 아이템 개수
			$items['add'] = -1; // 보석 수량 
			
			$data['use']['items'][] = $items;

			// frdUserData 에서 가져온 accuSkillPoint 로 업데이트 
			$sql = "UPDATE frdUserData SET skillPoint=:mySP where userId=:userId";
			$db -> prepare($sql);
			$db -> bindValue(':mySP', $accuSkillPoint, PDO::PARAM_INT);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError('resetSkillPoint : FAIL userId : '.$userId.", sql : ".$sql);
				return $resultFail;
			}

			// 스킬 레벨을 가진 테이블을 전부 삭제. 
			$sql = "DELETE FROM frdSkillLevels where userId=:userId";
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError('resetSkillPoint : FAIL userId : '.$userId.", sql : ".$sql);
				return $resultFail;
			}

			// 증가 효과 관련 기록 테이블도 같이 삭제 .
			$sql = "delete from frdEffectForPreCheckRewards where userId=:userId";
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError('resetSkillPoint : FAIL userId : '.$userId.", sql : ".$sql);
				return $resultFail;
			}
			$sql = "delete from frdEffectForRewardsSP where userId=:userId"; 
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError('resetSkillPoint : FAIL userId : '.$userId.", sql : ".$sql);
				return $resultFail;
			}

		}
		$medal['curMedal'] = $accuSkillPoint;
		$data['rewards']['medal'] = $medal; // 나의 누적 스킬 포인트

		$result['Protocol'] = 'ResResetSkillPoint';
		$result['ResultCode'] = 100;
		$result['Data'] = $data;

		return $result;
	}

	function remEffectSP($db, $userId, $spId, $curLevel){
		$resultFail['ResultCode'] = 300;
		$eff_data = null;
		$effectId = "EffectRewardCheckSP_".$spId;
		$eff_data = apc_fetch($effectId);

		// Array Info 
		//  userSP = Key
		// 0 reward_type
		// 1 percent
		if ($eff_data) {
			$type = (int)$eff_data[1];
			$val = (int)$eff_data[2];
			$subVal = $val * $curLevel;

			$sql = "SELECT val FROM frdEffectForRewardsSP WHERE userId=:userId AND type=:type";
			$db -> prepare($sql);
			$db -> bindValue(':type', $type, PDO::PARAM_INT);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError(__FUNCTION__.': FAIL userId : ' . $userId . ", sql : " . $sql);
				//				return $resultFail;
				$result['ResultCode'] = 100;
				return $result;
			}

			$myVal = $row["val"];
			if ( $subVal > $myVal ) {
				$this->logger->logError(__FUNCTION__.': FAIL userId : ' . $userId);
				return $resultFail;
			}

			if ( ($myVal - $subVal) == 0 ) {
				$sql = "delete from frdEffectForRewardsSP where userId=:userId and type=:type";
				$db -> prepare($sql);
				$db -> bindValue(':type', $type, PDO::PARAM_INT);
				$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			} else {
				$sql = "update frdEffectForRewardsSP set val=val-:subVal where userId=:userId and type=:type";
				$db -> prepare($sql);
				$db -> bindValue(':subVal', $subVal, PDO::PARAM_INT);
				$db -> bindValue(':type', $type, PDO::PARAM_INT);
				$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			}
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError(__FUNCTION__.': FAIL userId : ' . $userId . ", sql : " . $sql);
				return $resultFail;
			}
		}

		$eff_data = null;
		$effectId = trim("EffectRewardPreCheckSP_").$spId;
		$eff_data = apc_fetch($effectId);

		// Array Info 
		//  userSP = Key
		// 0 reward_type
		// 1 percent

		if ($eff_data) {
			$type = (int)$eff_data[0];
			$val = (int)$eff_data[1];
			$subVal = $val * $curLevel;

			$sql = "SELECT type FROM frdEffectForRewardsSP WHERE userId=:userId AND type=:type";
			$db -> prepare($sql);
			$db -> bindValue(':type', $type, PDO::PARAM_INT);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError(__FUNCTION__.': FAIL userId : ' . $userId . ", sql : " . $sql);
				//				return $resultFail;
				$result['ResultCode'] = 100;
				return $result;
			}

			$sql = "delete from frdEffectForRewardsSP where userId=:userId and type=:type";
			$db -> prepare($sql);
			$db -> bindValue(':type', $type, PDO::PARAM_INT);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError(__FUNCTION__.': FAIL userId : ' . $userId . ", sql : " . $sql);
				return $resultFail;
			}
		}

		$result['ResultCode'] = 100;
		return $result;
	}

	public function UpSkillLV ($param) {
		$resultFail['ResultCode'] = 300;

		$userId 		= $param["userId"];
		$skillId 		= (int)$param["skillId"];
		$needPoint 		= $param["needPoint"];
		$count 			= $param["count"];
		
		$db = new DB();

		if ($needPoint < 0 ) {
			$sql = "UPDATE frdUserData SET blockState = 2 where userId =:userId";
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError(__FUNCTION__.': FAIL userId : ' . $userId . ", sql : " . $sql);
				return $resultFail;
			}
		}

		$needPoint = abs($needPoint);


		$sql = "SELECT skillTableId, userId, skillId, exp FROM frdSkillLevels WHERE userId = :userId AND skillId=:skillId";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> bindValue(':skillId', $skillId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if ($skillId == 1101) {
			$needPoint = 1500;
			if ($row) {
				$tempExp  = $row['exp'];		
			} else {
				$tempExp = 0;
			}

			if ( ($tempExp + $count) > 6) {
				$count = 6 - $tempExp;
			}
		}
		
		$AssetManager = new AssetManager();
		$spResult = $AssetManager->useSkillPoint($db, $userId, $needPoint*$count);
		if ($spResult['ResultCode'] != 100 ) {
			$resultFail['ResultCode'] = $spResult['ResultCode'];
			return $resultFail;
		}
		$medal = null;
		$medal['curMedal'] = $spResult['skillPoint'];
		$medal['addMedal'] = 0 - $needPoint*$count;

		if (!$row) {
			$sql = "INSERT INTO frdSkillLevels (userId, skillId, exp, update_date) values 
					 ( :userId, :skillId, :count, now())";
			$db->prepare($sql);
			$db->bindValue(':userId', $userId, PDO::PARAM_INT);
			$db->bindValue(':skillId', $skillId, PDO::PARAM_INT);
			$db->bindValue(':count', $count, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError(__FUNCTION__.': FAIL userId : ' . $userId . ", sql : " . $sql);
				return $resultFail;
			}
			$data["exp"] = $count;
			$skillTableId = $db -> lastInsertId();

			$data["skillTableId"] = $skillTableId;
			$data["skillId"] = $skillId;
			$data["exp"] = $count;

			// 추가 효과와 곲셈 계산을 위해 지정 
			$myLV = 0;
		} else {
			$rowUserId = $row['userId'];
			$rowSkillIdx = $row['skillId'];
			// 추가 효과와 곲셈 계산을 위해 지정 
			$myLV = $row['exp'];
			if ( ($rowUserId != $userId) or ( $skillId != $rowSkillIdx) ) {
				$this->logger->logError(__FUNCTION__.': NOT MATCH  userId : ' . $userId . ", rowuseri " . $rowUserId);
				$this->logger->logError(__FUNCTION__.': NOT MATCH  skillId : ' . $skillId . ", skillId " . $rowSkillIdx);
				return $resultFail;
			}				

			$skillTableId 	= $row["skillTableId"];

			$sql = "update frdSkillLevels set exp=exp + :count where skillTableId =:skillTableId";
			$db -> prepare($sql);
			$db -> bindValue(':count', $count, PDO::PARAM_INT);
			$db -> bindValue(':skillTableId', $skillTableId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError(__FUNCTION__.': FAIL userId : ' . $userId . ", sql : " . $sql);
				return $resultFail;
			}

			$data["skillTableId"] = $skillTableId;
			$data["skillId"] = $skillId;
			$data["exp"] = $myLV + $count;
		}

		$apc_data = null;
		$apc_key = 'EffectRewardPreCheckSP_'.$skillId;
		$apc_data = apc_fetch($apc_key);
		if ($apc_data) {
			$type 		= $apc_data[0];			
			$subType 	= $apc_data[1];			
			$val 		= $apc_data[2];			

			$sql = "SELECT * FROM frdEffectForPreCheckRewards WHERE userId=:userId AND type=:type";
			$db -> prepare($sql);
			$db -> bindValue(':type', $type, PDO::PARAM_INT);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$db -> execute();
			$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
			if (!$row) {
				$sql = "INSERT INTO frdEffectForPreCheckRewards 
						(userId, type, subType, val, isSuccess, update_date, reg_date)
						VALUES	
						(:userId, :type, :subType, 0, 0, now(), now())";
				$db -> prepare($sql);
				$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
				$db -> bindValue(':type', $type, PDO::PARAM_INT);
				$db -> bindValue(':subType', $subType, PDO::PARAM_INT);
				$row = $db -> execute();
				if (!isset($row) || is_null($row) || $row == 0) {
					$this->logger->logError(__FUNCTION__.': FAIL userId : ' . $userId . ", sql : " . $sql);
					return $resultFail;
				}

				$EF_Check_Reward_TB_ID = $db -> lastInsertId();
			} 
		}

		$apc_data = null;
		$apc_key = 'EffectRewardCheckSP_'.$skillId;
		$apc_data = apc_fetch($apc_key);
		if ($apc_data) {
			$type 		= $apc_data[1];			
			$val 		= $apc_data[2];			

			$sql = "SELECT * FROM frdEffectForRewardsSP WHERE userId=:userId AND type=:type";
			$db->prepare($sql);
			$db->bindValue(':type', $type, PDO::PARAM_INT);
			$db->bindValue(':userId', $userId, PDO::PARAM_INT);
			$db -> execute();
			$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
			if (!$row) {
				$temp = $val*$count;
				$sql = "INSERT INTO frdEffectForRewardsSP 
						(userId, type, val, isSuccess, update_date, reg_date)
						VALUES	
						(:userId, :type, :temp, 0, now(), now())";
				$db -> prepare($sql);
				$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
				$db -> bindValue(':type', $type, PDO::PARAM_INT);
				$db -> bindValue(':temp', $temp, PDO::PARAM_INT);
				$row = $db -> execute();
				if (!isset($row) || is_null($row) || $row == 0) {
					$this->logger->logError(__FUNCTION__.': FAIL userId : ' . $userId . ", sql : " . $sql);
					return $resultFail;
				}
				$EF_Reward_TB_ID = $db -> lastInsertId();
			} else {

				$temp = $val*$count;
				
				$sql = "UPDATE frdEffectForRewardsSP SET val= val + :temp where userId=:userId AND type=:type";
				$db -> prepare($sql);
				$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
				$db -> bindValue(':type', $type, PDO::PARAM_INT);
				$db -> bindValue(':temp', $temp, PDO::PARAM_INT);
				$row = $db -> execute();
				if (!isset($row) || is_null($row) || $row == 0) {
					$this->logger->logError(__FUNCTION__.': FAIL userId : ' . $userId . ", sql : " . $sql);
					return $resultFail;
				}
			}
		}
	
		$data['use']['medal'] = $medal;

		$result['Data'] = $data;
		$result['ResultCode'] = 100;

		return $result;
	}



}
?>
