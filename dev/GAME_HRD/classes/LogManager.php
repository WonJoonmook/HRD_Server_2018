<?php
include_once './common/DB.php';
include_once './common/LOGIN_DB.php';
require_once './lib/Logger.php';

class LogManager {
    public function __construct() {
        $this -> logger = Logger::get();
    }

	// 로그인 디비에 적재 한다. 
    function SendPurchaseLog($param) {
        $resultFail['Protocol'] = 'ResSendPurchaseLog';
        $resultFail['ResultCode'] = 300;

		$userId = $param["userId"];
		$log = $param["log"];

        $db = new LOGIN_DB();
        $sql = <<<SQL
		SELECT userId
		FROM frdLocalLog 
		WHERE userId = :userId
SQL;
        $db -> prepare($sql);
        $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
        $db -> execute();
        $row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if ($row) {
			$sql = <<<SQL
            UPDATE frdLocalLog
            SET log = :log, update_date = now()
            WHERE userId = :userId 
SQL;

            $db -> prepare($sql);
            $db -> bindValue(':log', $log, PDO::PARAM_STR);
            $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
            $row = $db -> execute();
            if (!isset($row) || is_null($row) || $row == 0) {
                $this -> logger -> logError(__FUNCTION__.' :  FAIL userId : '.$userId . ", sql : ".$sql);
                return $resultFail;
            }
		} else {
			$sql = <<<SQL
           INSERT INTO frdLocalLog (userId, log,update_date) VALUES
			(:userId, :log, now())
SQL;
            $db -> prepare($sql);
            $db -> bindValue(':log', $log, PDO::PARAM_STR);
            $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
            $row = $db -> execute();
            if (!isset($row) || is_null($row) || $row == 0) {
                $this -> logger -> logError(__FUNCTION__.' :  FAIL userId : '.$userId . ", sql : ".$sql);
                return $resultFail;
            }
 	
		}
        $result['Protocol'] = "ResSendPurchaseLog";
        $result['ResultCode'] = 100;
        $result['Data'] = "일찍가고싶다";

        return $result;
    }
    function GPULog($param) {
        $resultFail['Protocol'] = 'ResGPULog';
        $resultFail['ResultCode'] = 100;

		$str = $param["str"];

        $db = new LOGIN_DB();
        $sql = <<<SQL
		SELECT * FROM frdGPULog WHERE strGPU = :strGPU
SQL;
        $db -> prepare($sql);
        $db -> bindValue(':strGPU', $strGPU, PDO::PARAM_INT);
        $db -> execute();
        $row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if ($row) {
			$sql = <<<SQL
            UPDATE frdGPULog
            SET cnt = cnt+1  WHERE strGPU = :strGPU 
SQL;

            $db -> prepare($sql);
			$db -> bindValue(':strGPU', $strGPU, PDO::PARAM_INT);
            $row = $db -> execute();
            if (!isset($row) || is_null($row) || $row == 0) {
                $this -> logger -> logError(__FUNCTION__.' :  FAIL userId : '.$userId . ", sql : ".$sql);
                return $resultFail;
            }
		} else {
			$sql = <<<SQL
           INSERT INTO frdGPULog (strGPU, cnt) VALUES
			(:strGPU, 1)
SQL;
            $db -> prepare($sql);
			$db -> bindValue(':strGPU', $strGPU, PDO::PARAM_INT);
            $row = $db -> execute();
            if (!isset($row) || is_null($row) || $row == 0) {
                $this -> logger -> logError(__FUNCTION__.' :  FAIL userId : '.$userId . ", sql : ".$sql);
                return $resultFail;
            }
 	
		}
        $result['Protocol'] = "ResGPULog";
        $result['ResultCode'] = 100;
        $result['Data'] = null;

        return $result;
    }

}
?>
