<?php

/**
 *
 * Enter description here ...
 * @author Administrator
 *
 */
class JsonParser {
	public function __construct() {
	}

	public function getallheader() {
		$headers = '';
		foreach ($_SERVER as $name => $value) {
			if (substr($name, 0, 5) == 'HTTP_') {
				$headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
			}
		}
		return $headers;
	}

	public function isJson($data) {
		return true;
	}

	public function encode($data) {
		$resultData = json_encode($data);
//		$resultData = $this->json_encode2($data);
		$resultError = json_last_error();
		if ($resultError != JSON_ERROR_NONE) {
			return false;
		} else {
			return $resultData;
		}
	}

	public function parser($data) {
		$resultData = json_decode($data);
		$resultError = json_last_error();
		if ($resultError != JSON_ERROR_NONE) {
			return false;
		} else {
			$returnArray = $this -> convertionArray($resultData);
			return $returnArray;
		}
	}

	public function convertionArray($obj) {
		$resultArray = array();
		if (!is_object($obj) && !is_array($obj)) {
			return $obj;
		}

		if (is_object($obj)) {
			foreach ($obj as $key => $value) {
				if (count($value) > 0) {
					$value = $this -> convertionArray($value);
				} else {
					if (isset($value))
						settype($value, 'array');
				}

				if (isset($value))
					$resultArray[$key] = $value;
			}

			return $resultArray;
		} else {
			foreach ($obj as $key => $value) {
				if (count($value) > 0) {
					$value = $this -> convertionArray($value);
				} else {
					if (isset($value))
						settype($value, 'array');
				}
				if (isset($value))
					$resultArray[$key] = $value;
			}
		}
		return $resultArray;
	}
	public function json_encode2($data) {

		if (gettype($data) == 'integer' or gettype($data) == 'double') {
			$data = (string)$data;
		}

		switch (gettype($data)) {
			case 'boolean' :
				return $data ? 'true' : 'false';
			case 'integer' :
			case 'double' :
				return $data;
			case 'string' :
				return '"' . strtr($data, array('\\' => '\\\\', '"' => '\\"')) . '"';
			case 'array' :
				$rel = false;
				// relative array?
				$key = array_keys($data);
				foreach ($key as $v) {
					if (!is_int($v)) {
						$rel = true;
						break;
					}
				}

				$arr = array();
				foreach ($data as $k => $v) {
					$arr[] = ($rel ? '"' . strtr($k, array('\\' => '\\\\', '"' => '\\"')) . '":' : '') . $this -> json_encode2($v);

				}
				return $rel ? '{' . join(',', $arr) . '}' : '[' . join(',', $arr) . ']';
			default :
				return 'null';
		}
	}
}	
