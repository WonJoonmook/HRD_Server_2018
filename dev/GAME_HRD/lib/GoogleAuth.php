<?php
include_once './common/DB.php';
require_once './lib/Logger.php';

class GoogleAuth {
    public function __construct() {
        $this -> logger = Logger::get();
    }
		
	public function googleAuth($idToken, $testId){

		if ( !is_null($_REQUEST["testId"]) && strlen ( $_REQUEST["testId"] ) > 5 )
			$id = $_REQUEST["testId"];
		else {
			$sParam = "access_token=".$idToken;
			$rResult = $this->getCurl("https://www.googleapis.com/oauth2/v3/tokeninfo","get","$sParam");

			$sResult = json_decode($rResult,true);

			if ( $sResult == false || $sResult["sub"] == 0 ) {
				$this -> logger -> logError('googleAuth : cant receive token! idToken : ' . $idToken);
				echo "cant receive token!" ;
				return null;
			}

			$id = (string)$sResult["sub"];
		}

		return $id;
	}	

	function getCurl($fUrl,$fMethod,$fParam) {
		$resultFail['ResultCode'] = 300;

		$sUrl = $fUrl.(($fParam && strtolower($fMethod)=="get") ? "?$fParam": "");
		$sMethod = (strtolower($fMethod)=="get") ? "0" : "1" ;
		$sParam = (strtolower($fMethod)=="get") ? "" : $fParam ;

		$ch = curl_init();
		curl_setopt ($ch, CURLOPT_URL,"$sUrl"); //접속할 URL 주소
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, FALSE); // 인증서 체크같은데 true 시 안되는경우가많다
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		// default 값이 true 이기때문에 이부분을 조심 (https 접속시에 필요)
		curl_setopt ($ch, CURLOPT_SSLVERSION,4); // SSL 버젼 (https 접속시에 필요)
		curl_setopt ($ch, CURLOPT_HEADER, 0); // 헤더 출력 여부
		curl_setopt ($ch, CURLOPT_POST, $sMethod); // Post Get 접속 여부
		curl_setopt ($ch, CURLOPT_POSTFIELDS, "$fParam"); // Post 값  Get 방식처럼적는다.
		curl_setopt ($ch, CURLOPT_TIMEOUT, 5); // 30->5 줄였습니다. 문제시에 다시 확장
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1); // 결과값을 받을것인지
		$result = curl_exec ($ch);

		$http_result_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);

		// CURL SUCEESS == 200
		// 100 CONTINUE
		if ($http_result_code != 200) {
			$this->logger->logError(__FUNCTION__.' SERVER CURL CONNECTION ERROR param : ' . $fParam);
			return $resultFail;
		}


		curl_close ($ch);
		return $result;
	}


}

?>
