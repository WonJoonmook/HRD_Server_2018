<?php

class BackUpManager {

    public function __construct() {
        $this -> logger = Logger::get();

        // $sell_route == 1 : 판매, 2 : 합성, 3 : 환생, 4 : 진화재료, 5 : 성장소재료, 6 : 스톤장착
        switch ($GLOBALS['requestData']['Protocol']) {

            case 'ReqMailList' :
                $GLOBALS['sell_route'] = 1;
                break;


            default :
                // 프로토콜 확인 필요
                $GLOBALS['sell_route'] = 0;
                break;
        }

    }

    function mail_del($db, $UID, $mail_info_array, $del_mail_id, $server_no = null) {
        $resultFail['ResultCode'] = 300;

        // delete mail
        $sql = <<<SQL
            DELETE FROM frdUserPost
            WHERE pId IN ({$del_mail_id}) AND recvUserId = '{$UID}'
SQL;
        $db -> prepare($sql);
        $row = $db -> execute();
        if (!isset($row) || is_null($row) || $row == 0) {
            $this -> logger -> logError($GLOBALS['AccessNo'] . ', BackUpManager::mail_del : Qeury DELETE  FAIL UID : ' . $UID . ", sql : " . $sql);
            $this -> logger -> logError($GLOBALS['AccessNo'] . ', BackUpManager::mail_del : Qeury DELETE  FAIL UID : ' . $UID . ", del_mail_id : " . $del_mail_id);
            return $resultFail;
        }

        $result['ResultCode'] = 100;

        return $result;
    }

    function item_del($db, $UID, $id_array) {
        $resultFail['ResultCode'] = 300;

        $id_str = null;

        for ($i = 0; $i < count($id_array); $i++) {

            if ($id_str == null) {

                $id_str = $id_array[$i];

            } else {

                $id_str = $id_str . ',' . $id_array[$i];

            }

        }// for end

        $sql = <<<SQL
         SELECT ItemID, Item_Idx, enchant_level, enchant_exp, item_option_values, protocol, reg_date
         FROM Inven_Item_All
         WHERE ItemID IN ($id_str) and UID = :UID
SQL;

        $db -> prepare($sql);
        $db -> bindValue(':UID', $UID, PDO::PARAM_INT);
        $db -> execute();

        $backup_info_array = array();

        $check_stone_id = array();

        while ($row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {

            array_push($backup_info_array, $row);
        }## while end


        // del item
        $sql = <<<SQL
         DELETE FROM Inven_Item_All
         WHERE ItemID IN ($id_str) and UID = :UID
SQL;

        $db -> prepare($sql);
        $db -> bindValue(':UID', $UID, PDO::PARAM_INT);
        $row = $db -> execute();
        if (!$row) {
            $this -> logger -> logError('DB : ' . $GLOBALS['AccessNo'] . ', BackUpManager::item_del : Qeury delete item FAIL UID : ' . $UID . ", sql : " . $sql);
            return $resultFail;
        }

        $result['ResultCode'] = 100;

        return $result;
    }

    function summon_del($db, $UID, $id_array) {
        $resultFail['ResultCode'] = 300;

        $id_str = null;

        for ($i = 0; $i < count($id_array); $i++) {

            if ($id_str == null) {

                $id_str = $id_array[$i];

            } else {

                $id_str = $id_str . ',' . $id_array[$i];

            }

        }// for end

        $sql = <<<SQL
         SELECT SummonID, Summon_Idx, summon_level, summon_level_max, summon_exp, enchant_level, enchant_exp, summon_status, summon_equips_amulet, summon_equips_ring, summon_equips_belt, chowol_floor, protocol, reg_date
         FROM Inven_Summon
         WHERE SummonID IN ($id_str) and UID = :UID
SQL;

        $db -> prepare($sql);
        $db -> bindValue(':UID', $UID, PDO::PARAM_INT);
        $db -> execute();

        $backup_info_array = array();

        $check_item_id = array();

        $check_stone_id = array();

        while ($row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {

            if ($row['summon_status'] > 0) {
                $this -> logger -> logError($GLOBALS['AccessNo'] . ', BackUpManager::summon_del summon_status > 0 :  UID : ' . $UID . 'SummonID : ' . $row['SummonID']);
                return $resultFail;
            }

            if ($row['summon_equips_amulet'] > 0) {
                array_push($check_item_id, $row['summon_equips_amulet']);
            }

            if ($row['summon_equips_ring'] > 0) {
                array_push($check_item_id, $row['summon_equips_ring']);
            }

            if ($row['summon_equips_belt'] > 0) {
                array_push($check_item_id, $row['summon_equips_belt']);
            }

            array_push($backup_info_array, $row);
        }## while end

        // del item
        $sql = <<<SQL
         DELETE FROM Inven_Summon
         WHERE SummonID IN ($id_str) and UID = :UID
SQL;

        $db -> prepare($sql);
        $db -> bindValue(':UID', $UID, PDO::PARAM_INT);
        $row = $db -> execute();
        if (!$row) {
            $this -> logger -> logError('DB : ' . $GLOBALS['AccessNo'] . ', BackUpManager::summon_del : Qeury delete summon FAIL UID : ' . $UID . ", sql : " . $sql);
            return $resultFail;
        }

        if (count($check_item_id) > 0) {

            $result_item_del = $this -> item_del($db, $UID, $check_item_id);
            if ($result_item_del['ResultCode'] != 100) {
                $this -> logger -> logError($GLOBALS['AccessNo'] . ', BackUpManager::summon_del item_del fail :  UID : ' . $UID);
                return $resultFail;
            }

        }

        $result['ResultCode'] = 100;

        return $result;
    }

}
?>
