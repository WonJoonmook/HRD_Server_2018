<?php
include_once './common/Config.php';
include_once './common/DB_Info.php';
include_once './common/DB.php';
include_once './common/LOGIN_DB.php';
require_once "./common/define.php";
require_once './classes/AddManager.php';
require_once './classes/InAppManager_YK.php';

$date = date('Y-m-d H:i:s');

$request = file_get_contents("php://input");

/*
■	amount ：amount는 이번 총 소비가격, 실제로 RMB(할인후) 받은 후 전까지 정확하게 계산
■	cpinfo ：개발사가 RoRoSDK에 기입한, RoRoServer OSPE // --> productId 로 변경 
■	pforderid ：모바일게임 마켓 서드파티 오픈 플랫폼 계정
■	pfuid ：모바일게임 마켓 서드파티 오픈 플랫폼 계정
■	plappid ：RoRo마켓에서 발행한 appid
■	plchid ：각 모바일게임 마켓 서드파티 오픈 플랫폼이 영킹게임 플랫폼에서 사용하는 ID; 동시에 RoRoSDK API에서 해당 파라미터를 앱 클라이언트로 전달
■	plorderid ：주문번호 (RoRo마켓 주문번호)
■	roleid ：게임의 roleid，게임 클라이언트 결제 시 RoRoSDK에 잔달, 서버에서 그대로 돌려보냄
■	sendtime ：시간, 포맷：yyyy-mm-dd HH24:mm:ss
■	sign ：RoRo 마켓 서명 MD5암호화 적용
■	zoneid ：게임 서버지역 ID. 게임 클라이언트 결제시 RoRoSDK에 전달, 서버에서 그대로 돌려보냄
*/
	
$param = json_decode($request);
$amount 	= $param->amount;
$cpinfo 	= $param->cpinfo;
$pforderid 	= $param->pforderid;
$pfuid 		= $param->pfuid;
$plappid 	= $param->plappid;
$plchid 	= $param->plchid;
$plorderid 	= $param->plorderid;
$roleid 	= $param->roleid;
$sendtime 	= $param->sendtime;
$sign 		= $param->sign;
$zoneid 	= $param->zoneid;

$ResultFail = null;
$ResultFail['code'] = 101;
$ResultFail['msg'] = 'Notify Success';
$mes =$amount.','.$cpinfo.','.$pforderid.','.$pfuid.','.$plappid.','.$plchid.','.$plorderid.','.$roleid.','.$sendtime.','.$sign.','.$zoneid; 
error_log("{$date} | {$mes}" . "\n", 3, "/home/log/YK_Req" . date('Ymd') . '.log');

$mes = null;

if ($amount < 1 ) {
	$result['code'] = 101;
	$result['msg'] = 'Invalid Amount';

	$result = json_encode($result);
	echo $result;
	return;
}

// RORO SDK OSPE CHECK 
if ( 
	( $pforderid == null || $pforderid == "") ||
	( $pfuid == null || $pfuid == "") || 
	( $cpinfo == null || $cpinfo == "") || 
	( $plappid != 10010) ||
	( $plchid == null || $plchid == "") ||
	( $plorderid == null || $plorderid == "") ||
	( $roleid == null || $roleid == "") ||
	( $sendtime == null || $sendtime == "") || 
	( $sign == null || $sign == "") ||
	( $zoneid == null || $zoneid == "") 
	)
	{

	$result['code'] = 102;
	$result['msg'] = 'Invalid Argument';

	$result = json_encode($result);
	echo $result;
	return;

}

$str = "amount=".$amount."&cpinfo=".$cpinfo."&pforderid=".$pforderid."&pfuid=".$pfuid."&plappid=".$plappid."&plchid=".$plchid;
$str .= "&plorderid=".$plorderid."&roleid=".$roleid."&sendtime=".$sendtime."&zoneid=".$zoneid."&plappsecret=nKcitRHim6ttGRAV";

if ( md5($str)!= $sign ) {
	$result['code'] = 103;
	$result['msg'] = 'Invalid Signature';

	$mes = md5($str);
	error_log("{$date} | {$mes}" . "\n", 3, "/home/log/YK_" . date('Ymd') . '.log');
	$result = json_encode($result);
	echo $result;
	return;

}
// 상품 지급  ---> 
// 계정 체크 
$login_db = new LOGIN_DB();
$sql = "select userId from t_Account_User where connectId=:connectId";
$login_db -> prepare($sql);
$login_db -> bindValue(':connectId', $pfuid, PDO::PARAM_STR);
$login_db -> execute();
$row = $login_db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
if (!$row) {
	$result['code'] = 104;
	$result['msg'] = 'Unknown Error';

	
	$mes = $sql.','.$connectId.','.$plchid;
	error_log("{$date} | {$mes}" . "\n", 3, "/home/log/YK_" . date('Ymd') . '.log');
	$result = json_encode($result);
	echo $result;
	return;
}

$userId = $row['userId'];

$db_no = substr($userId, 0, 3);
$GLOBALS['AccessNo'] = $db_no;

$db = new DB();
// 주문확인 
$sql = "SELECT * FROM frdLogBuy WHERE orderId = :orderId";
$db -> prepare($sql);
$db -> bindValue(':orderId', $plorderid, PDO::PARAM_STR);
$db -> execute();
$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
if (!$row) {
	$sql = "INSERT INTO frdLogBuy
		(userId, packageName, orderId, productId, purchaseTime,purchaseState, purchaseToken, signature, isComplete,reg_date )
		values
		(:userId, :packageName, :orderId, :productId, now(),:purchaseState, :purchaseToken, :signature, 0,now() )";
	$db -> prepare($sql);
	$db -> bindValue(':userId', $roleid, PDO::PARAM_INT);
	$db -> bindValue(':packageName', 'yk', PDO::PARAM_STR);
	$db -> bindValue(':orderId', $plorderid, PDO::PARAM_STR);
	$db -> bindValue(':productId', $cpinfo, PDO::PARAM_STR);
	$db -> bindValue(':purchaseState', 0, PDO::PARAM_INT);
	$db -> bindValue(':purchaseToken', $plchid, PDO::PARAM_STR); // plchid 가 들어가게 된다. 후에 이 값을 비교 한다.
	$db -> bindValue(':signature', '', PDO::PARAM_STR);
	$row = $db -> execute();
	if (!isset($row) || is_null($row) || $row == 0) {
		$result['code'] = 104;
		$result['msg'] = 'Unknown Error';

		$mes = $sql.','.$plorderid;
		error_log("{$date} | {$mes}" . "\n", 3, "/home/log/YK_" . date('Ymd') . '.log');
		$result = json_encode($result);
		echo $result;
		return;
	}
}

$sql = "SELECT shopItemId, itemId, itemCount,price FROM frdShopData 
WHERE productId = :productId";
$db -> prepare($sql);
$db -> bindValue(':productId', $cpinfo, PDO::PARAM_STR);
$db -> execute();
$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
if (!$row) {
	$result['code'] = 104;
	$result['msg'] = 'Unknown Error';

	$mes = $sql.','.$connectId.','.$plorderid;
	error_log("{$date} | {$mes}" . "\n", 3, "/home/log/YK_" . date('Ymd') . '.log');
	$result = json_encode($result);
	echo $result;
	return;
}

$shopItemId = $row['shopItemId'];
$itemId = $row['itemId'];
$itemCount = $row['itemCount'];
$price = $row['price'];

$sql = "UPDATE frdLogBuy SET purchaseState = 100 where userId = :userId and orderId = :orderId";
$db -> prepare($sql);
$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
$db -> bindValue(':orderId', $plorderid, PDO::PARAM_STR);
$row = $db -> execute();
if (!isset($row) || is_null($row) || $row == 0) {
	$result['code'] = 104;
	$result['msg'] = 'Unknown Error';
	error_log("{$date} | {$mes}" . "\n", 3, "/home/log/YK_" . date('Ymd') . '.log');
	$result = json_encode($result);
	echo $result;
	return;
}


$InAppManager_YK = new InAppManager_YK();

// 패키지 
if ( $shopItemId > 6000 && $shopItemId != 6005 ) {
	$tempTypes = explode('/',$itemId);	
	$tempCnts = explode('/',$itemCount);	

	$addResult = $this->addItemForPackage($db, $userId, $tempTypes, $tempCnts);
	$data['rewards'] = $addResult;
	// 보석 
} else if ( $shopItemId > 1000 && $shopItemId < 2000) {

	$AssetManager = new AssetManager();
	$AssetResult = $AssetManager->addJewel($db, $userId, $itemCount);
	$jewel['curJewel'] = $AssetResult['jewel'];
	$jewel['addJewel'] = $itemCount;

	$data['rewards']['jewel'] = $jewel;
} else if ( $shopItemId == 6005  ) {	

	$sql = "UPDATE frdMonthlyCard SET startTime = now(), duration = 30, recvCnt = 1
		WHERE UserId = :userId";
	$db -> prepare($sql);
	$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
	$row = $db -> execute();
	if (!isset($row) || is_null($row) || $row == 0) {
		$result['code'] = 104;
		$result['msg'] = 'Unknown Error';
		$result = json_encode($result);
		echo $result;
		return;
	}

	$sql = "INSERT INTO frdUserPost
		(recvUserId, sendUserId, type, count, expire_time, reg_date) VALUES ";
	$sql .= "($userId, 101, 5004, 20, DATE_ADD(now(), INTERVAL 30 DAY ), now()), ";  // jewel
	$sql .= "($userId, 101, 5003, 20, DATE_ADD(now(), INTERVAL 30 DAY ), now())  ";  // heart
	$db -> prepare($sql);
	$row = $db -> execute();
	if (!isset($row) || is_null($row) || $row == 0) {
		$result['code'] = 104;
		$result['msg'] = 'Unknown Error';
		$result = json_encode($result);
		echo $result;
		return;
	}
}
$data = json_encode($data);
$sql = "INSERT INTO frdYKBuyLog
(userId, state, orderId, log, update_date) VALUE 
(:userId, 100, :orderId, :log, now())  ";  
$db -> prepare($sql);
$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
$db -> bindValue(':orderId', $plorderid, PDO::PARAM_STR);
$db -> bindValue(':log', $data, PDO::PARAM_STR);
$row = $db -> execute();
if (!isset($row) || is_null($row) || $row == 0) {
	$result['code'] = 104;
	$result['msg'] = 'Unknown Error';
	$result = json_encode($result);
	echo $result;
	return;
}


$result['code'] = 100;
$result['msg'] = 'Notify Success';

$result = json_encode($result);
echo $result;
return;

?>
