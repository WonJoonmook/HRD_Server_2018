<?php
require_once "./common/Config.php";
require_once "./lib/Logger.php";
require_once "./classes/AbilityManager.php";
require_once './lib/ParamChecker.php';

class AbilityApi {
	public function __construct() {
		$this -> logger = Logger::get();
	}

	public function ReqGetCanUnlock($param) {
		$base_param = null;
		$ParamChecker = new ParamChecker();
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			$resultFail['Protocol'] = "ResGetCanUnlock";
			$resultFail['ResultCode'] = $check_result;
			return $resultFail;
		}
		$resultFail = array('Protocol' => 'ResGetCanUnlock', 'ResultCode' => 200);

		$Manager = new AbilityManager();
		$resultC = $Manager ->GetCanUnlock($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else//FAIL RETURN
		{
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}
	public function ReqEquipAbility($param) {
		$resultFail = array('Protocol' => 'ResEquipAillity', 'ResultCode' => 200);
		$base_param = null;
		$ParamChecker = new ParamChecker();
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			$resultFail['ResultCode'] = $check_result;
			return $resultFail;
		}

		$Manager = new AbilityManager();
		$resultC = $Manager ->EquipAbility($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else {
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}

	public function ReqApplyAbility ($param) {
		$ParamChecker = new ParamChecker();
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			$resultFail['Protocol'] = "ResApplyAbillilty";
			$resultFail['ResultCode'] = $check_result;
			return $resultFail;
		}
		$resultFail = array('Protocol' => 'ResApplyAbillilty', 'ResultCode' => 200);

		$Manager = new AbilityManager();
		$resultC = $Manager ->ApplyAbillilty($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else//FAIL RETURN
		{
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}

	public function ReqOpenSlot($param) {
		$resultFail = array('Protocol' => 'ResOpenSlot', 'ResultCode' => 200);
		$base_param = null;
		$ParamChecker = new ParamChecker();
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			$resultFail['ResultCode'] = $check_result;
			return $resultFail;
		}

		$Manager = new AbilityManager();
		$resultC = $Manager ->OpenSlot($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else {
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}

	public function ReqResetAbility($param) {
		$resultFail = array('Protocol' => 'ResResetAbility', 'ResultCode' => 200);
		$base_param = null;
		$ParamChecker = new ParamChecker();
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			$resultFail['ResultCode'] = $check_result;
			return $resultFail;
		}

		$Manager = new AbilityManager();
		$resultC = $Manager ->ResetAbility($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else {
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}


}
?>

