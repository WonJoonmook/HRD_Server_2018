<?php
require_once "./common/Config.php";
require_once "./lib/Logger.php";
require_once "./classes/MailManager.php";
require_once "./lib/ParamChecker.php";

class MailApi {
    public function __construct() {
        $this -> logger = Logger::get();
    }

    public function ReqMailList($param) {
        $resultFail = array('Protocol' => 'ResMailList', 'ResultCode' => 200);
		$base_param = null;
        $ParamChecker = new ParamChecker();
        $r = $ParamChecker -> param_check($param, $base_param);
        if ($r == 200) {
            $param_result['ResultCode'] = 200;
            return $param_result;
        }

        $MailManager = new MailManager();
        $resultInfo = $MailManager->MailList($param);
        if ($resultInfo['ResultCode'] == 100) {
            return $resultInfo;
        } else  {
            $resultFail['ResultCode'] = $resultInfo['ResultCode'];
            return $resultFail;
        }

        return $resultInfo;
    }

    public function ReqMailReceive($param) {
        $resultFail = array('Protocol' => 'ResMailReceive', 'ResultCode' => 200);

		$base_param = null;
        $ParamChecker = new ParamChecker();
        $r = $ParamChecker -> param_check($param, $base_param);
        if ($r == 200) {
            $param_result['ResultCode'] = 200;
            return $param_result;
        }

        $MailManager = new MailManager();
        $resultInfo = $MailManager->MailReceive($param);
        if ($resultInfo['ResultCode'] == 100) {
            return $resultInfo;
        } else  {
            $resultFail['ResultCode'] = $resultInfo['ResultCode'];
            return $resultFail;
        }

        return $resultInfo;
    }

}
?>
