<?php
require_once "./common/Config.php";
require_once "./lib/Logger.php";
require_once "./classes/ItemManager.php";
require_once './lib/ParamChecker.php';

class ItemApi {
	public function __construct() {
		$this -> logger = Logger::get();
	}

	public function ReqChangeWeaponStat($param) {
		$resultFail = array('Protocol' => 'ReqChangeWeaponStat', 'ResultCode' => 200);

		$base_param = null;
		$ParamChecker = new ParamChecker();
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			$resultFail['ResultCode'] = $check_result;
			return $resultFail;
		}

		$Manager = new ItemManager();
		$resultC = $Manager ->ChangeWeaponStat($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else { //FAIL RETURN 
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}

	public function ReqComposeRune($param) {
		$resultFail = array('Protocol' => 'ReqComposeRune', 'ResultCode' => 200);

		$base_param = null;
		$ParamChecker = new ParamChecker();
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			$resultFail['ResultCode'] = $check_result;
			return $resultFail;
		}

		$Manager = new ItemManager();
		$resultC = $Manager ->ComposeRune($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else { //FAIL RETURN 
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}

	public function ReqSellItem($param) {
		$resultFail = array('Protocol' => 'ReqSellItem', 'ResultCode' => 200);

		$base_param = null;
		$ParamChecker = new ParamChecker();
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			$resultFail['ResultCode'] = $check_result;
			return $resultFail;
		}

		$Manager = new ItemManager();
		$resultC = $Manager ->SellItem($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else { //FAIL RETURN 
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}

	public function ReqSpecialExpStone($param) {
		$resultFail = array('Protocol' => 'ResSpecialExpStone', 'ResultCode' => 200);

		$base_param = null;
		$ParamChecker = new ParamChecker();
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			$resultFail['ResultCode'] = $check_result;
			return $resultFail;
		}

		$Manager = new ItemManager();
		$resultC = $Manager ->SpecialExpStone($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else { //FAIL RETURN 
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}

	public function ReqUseItem($param) {
		$resultFail = array('Protocol' => 'ReqUseItem', 'ResultCode' => 200);

		$base_param = null;
		$ParamChecker = new ParamChecker();
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			$resultFail['ResultCode'] = $check_result;
			return $resultFail;
		}

		$Manager = new ItemManager();
		$resultC = $Manager ->UseItem($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else { //FAIL RETURN 
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}
}
?>

