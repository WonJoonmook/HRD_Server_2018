<?php
require_once "./common/Config.php";
require_once "./lib/Logger.php";
require_once "./classes/UserManager.php";
require_once './lib/ParamChecker.php';

class UserApi {

	public function __construct() {
		$this -> logger = Logger::get();

	}

	public function ReqSetName($param) {
		$ParamChecker = new ParamChecker();
		$base_param[] = 'userId';
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			$resultFail['Protocol'] = "ResUserInfo";
			$resultFail['ResultCode'] = $check_result;
			return $resultFail;
		}
		$resultFail = array('Protocol' => 'ResSetName', 'ResultCode' => 200);

		$UserManager = new UserManager();
		$result = $UserManager ->SetName($param);
		if ($result['ResultCode'] == 100) {
			return $result;
		} else//FAIL RETURN
		{
			$resultFail['ResultCode'] = $result['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}

	public function ReqTutoLevel($param) {
		$ParamChecker = new ParamChecker();
		$base_param[] = 'userId';
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			$resultFail['Protocol'] = "ResTutoLevel";
			$resultFail['ResultCode'] = $check_result;
			return $resultFail;
		}
		$resultFail = array('Protocol' => 'ResTutoLevel', 'ResultCode' => 200);

		$UserManager = new UserManager();
		$result = $UserManager ->TutoLevel($param);
		if ($result['ResultCode'] == 100) {
			return $result;
		} else//FAIL RETURN
		{
			$resultFail['ResultCode'] = $result['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}

}
?>

