<?php
require_once "./common/Config.php";
require_once "./lib/Logger.php";
require_once "./classes/TempleManager.php";
require_once './lib/ParamChecker.php';

class TempleApi {

	public function __construct() {
		$this -> logger = Logger::get();

	}

	public function ReqGetTempleInfo($param) {
		$resultFail = array('Protocol' => 'ResGetTempleInfo', 'ResultCode' => 200);
		$base_param = null;
        $ParamChecker = new ParamChecker();
        $check_result = $ParamChecker -> param_check($param, $base_param);
        if ($check_result == 200) {
            return $resultFail;
        }

		$Manager = new TempleManager();
		$resultC = $Manager ->GetTempleInfo($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else {
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}

    public function ReqTempleReward($param) {
        $resultFail = array('Protocol' => 'ResGetTempleReward', 'ResultCode' => 200);

		$base_param = null;
        $ParamChecker = new ParamChecker();
        $check_result = $ParamChecker -> param_check($param, $base_param);
        if ($check_result == 200) {
            return $resultFail;
        }

		$Manager = new TempleManager();
        $resultC = $Manager ->TempleReward($param);
        if ($resultC['ResultCode'] == 100) {
            return $resultC;
        } else {
            $resultFail['ResultCode'] = $resultC['ResultCode'];
            return $resultFail;
        }

        return $resultFail;
    }

    public function ReqLVUpTemple($param) {
        $resultFail = array('Protocol' => 'ResLVUpTemple', 'ResultCode' => 200);

		$base_param = null;
        $ParamChecker = new ParamChecker();
        $check_result = $ParamChecker -> param_check($param, $base_param);
        if ($check_result == 200) {
            return $resultFail;
        }

		$Manager = new TempleManager();
        $resultC = $Manager ->LVUpTemple($param);
        if ($resultC['ResultCode'] == 100) {
            return $resultC;
        } else {
            $resultFail['ResultCode'] = $resultC['ResultCode'];
            return $resultFail;
        }

        return $resultFail;
    }

    public function ReqEndTempleLVUP($param) {
        $resultFail = array('Protocol' => 'ResEndTempleLVUP', 'ResultCode' => 200);

		$base_param = null;
        $ParamChecker = new ParamChecker();
        $check_result = $ParamChecker -> param_check($param, $base_param);
        if ($check_result == 200) {
            return $resultFail;
        }

		$Manager = new TempleManager();
        $resultC = $Manager ->EndTempleLVUP($param);
        if ($resultC['ResultCode'] == 100) {
            return $resultC;
        } else {
            $resultFail['ResultCode'] = $resultC['ResultCode'];
            return $resultFail;
        }

        return $resultFail;
    }



}
?>

