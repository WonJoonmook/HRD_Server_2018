<?php
require_once "./common/Config.php";
require_once "./lib/Logger.php";
require_once "./classes/UserInfoManager.php";
require_once "./classes/LobbyManager.php";
require_once './lib/ParamChecker.php';

class UserInfoApi {

	public function __construct() {
		$this -> logger = Logger::get();

	}

	public function ReqUserInfo($param) {
		$ParamChecker = new ParamChecker();
		$base_param[] = 'userId';
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			$resultFail['Protocol'] = "ResUserInfo";
			$resultFail['ResultCode'] = $check_result;
			return $resultFail;
		}
		$resultFail = array('Protocol' => 'ResUserInfo', 'ResultCode' => 200);

		$UserInfoManager = new UserInfoManager();
		$result = $UserInfoManager ->getUserInfo($param);
		if ($result['ResultCode'] == 100) {
			return $result;
		} else//FAIL RETURN
		{
			$resultFail['ResultCode'] = $result['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}

	public function ReqSuccessEffectPreCheck($param) {
		$resultFail = array('Protocol' => 'ResSuccessEffectPreCheck', 'ResultCode' => 200);
		$base_param = null;
		$ParamChecker = new ParamChecker();
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			$resultFail['ResultCode'] = $check_result;
			return $resultFail;
		}

		$UserInfoManager = new UserInfoManager();
		$result = $UserInfoManager ->SuccessEffectPreCheck($param);
		if ($result['ResultCode'] == 100) {
			return $result;
		} else {
			$resultFail['ResultCode'] = $result['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}

	public function ReqGetEnterLobbyCheck($param) {
		$resultFail = array('Protocol' => 'ResGetEnterLobbyCheck', 'ResultCode' => 200);
		$base_param = null;
		$ParamChecker = new ParamChecker();
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			$resultFail['ResultCode'] = $check_result;
			return $resultFail;
		}

		$Manager = new LobbyManager();
		$result = $Manager ->GetEnterLobbyCheck($param);
		if ($result['ResultCode'] == 100) {
			return $result;
		} else {
			$resultFail['ResultCode'] = $result['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}
}
?>

