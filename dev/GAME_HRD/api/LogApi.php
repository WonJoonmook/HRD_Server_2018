<?php
require_once "./common/Config.php";
require_once "./lib/Logger.php";
require_once "./classes/LogManager.php";

class LogApi {
	public function __construct() {
		$this -> logger = Logger::get();
	}

	function ReqSendPurchaseLog($param) {
		$base_param = null;
		$resultFail = array('Protocol' => 'ResSendPurchaseLog', 'ResultCode' => 200);

		$Manager = new LogManager();
		$resultC = $Manager ->SendPurchaseLog($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else//FAIL RETURN
		{
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}

	function ReqGPULog($param) {
		$base_param = null;
		$resultFail = array('Protocol' => 'ResGPULog', 'ResultCode' => 200);

		$Manager = new LogManager();
		$resultC = $Manager ->GPULog($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else//FAIL RETURN
		{
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}

}
?>

