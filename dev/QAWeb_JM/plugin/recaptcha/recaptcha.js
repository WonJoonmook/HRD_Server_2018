$(function(){

	$("#captcha_reload").live("click", function(){
		$.ajax({
			type: 'POST',
			url: g5_captcha_url+'/recaptcha_api.php',
			data: { gubun: "1" },
			cache: false,
			async: false,
			success: function(html) {
				$("fieldset#captcha").after(html);
			}
		});
	}).trigger("click");

});

function chk_captcha()
{
	if ($('#g-recaptcha-response').val() == "") {
		alert("자동등록방지를 확인해 주십시오.");
		return false;
	}

	return true;
}
