<div class="modal-dialog">
   <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="window.location.reload();">×</button>
        <span class="modal-title"><strong>SNS 댓글공유</strong></span>
      </div>
    <div class="modal-body">
    자신이 작성한 댓글을 자신의 SNS계정으로 <strong class="text-info">동시</strong>에 공유합니다.<br />
	현재 아이콘 색상이 흐릿하게 보이신다면 <strong class="text-danger">활성화가 되지 않은상태</strong>입니다.<br />
    </div>

	   <div class="modal-footer">
           <button type="button" class="btn btn-default" data-dismiss="modal" onclick="window.location.reload();"><i class="fa fa-times-circle"></i> Close</button>
      </div>
    </div>
</div>