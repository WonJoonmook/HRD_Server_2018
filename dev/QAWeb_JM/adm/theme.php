<?php
$sub_menu = "100280";
include_once('./_common.php');

if ($is_admin != 'super')
    alert('최고관리자만 접근 가능합니다.');

// 테마 필드 추가
if(!isset($config['cf_theme'])) {
    sql_query(" ALTER TABLE `{$g5['config_table']}`
                    ADD `cf_theme` varchar(255) NOT NULL DEFAULT '' AFTER `cf_title` ", true);
}

$theme = get_theme_dir();
if($config['cf_theme'] && in_array($config['cf_theme'], $theme))
    array_unshift($theme, $config['cf_theme']);
$theme = array_values(array_unique($theme));
$total_count = count($theme);

// 설정된 테마가 존재하지 않는다면 cf_theme 초기화
if($config['cf_theme'] && !in_array($config['cf_theme'], $theme))
    sql_query(" update {$g5['config_table']} set cf_theme = '' ");

$g5['title'] = "테마설정";
include_once('./admin.head.php');
?>

<script src="<?php echo G5_ADMIN_URL; ?>/theme.js"></script>

<div class="well">
        <strong>
		설치된 테마 : <?php echo number_format($total_count); ?></strong>
</div>

<?php if($total_count > 0) { ?>
<div class="row" id="theme_list">
<?php
    for($i=0; $i<$total_count; $i++) {
        $info = get_theme_info($theme[$i]);

        $name = get_text($info['theme_name']);
        if($info['screenshot'])
            $screenshot = '<img src="'.$info['screenshot'].'" alt="'.$name.'" style="width:100%;">';
        else
            $screenshot = '<img src="'.G5_ADMIN_URL.'/img/theme_img.jpg" alt="" style="width:100%;">';

        if($config['cf_theme'] == $theme[$i]) {
            $btn_active = '<div class="alert alert-warning alert-dismissible" role="alert">
			<button type="button" class="theme_deactive close tooltip-top" data-dismiss="alert" aria-label="Close" title="해제" data-theme="'.$theme[$i].'" '.'data-name="'.$name.'">
			<span aria-hidden="true">&times;</span></button>
			<strong>사용중</strong></div>';
        } else {
            $tconfig = get_theme_config_value($theme[$i], 'set_default_skin');
            if($tconfig['set_default_skin'])
                $set_default_skin = 'true';
            else
                $set_default_skin = 'false';

            $btn_active = '<button type="button" class="btn btn-success theme_active" data-theme="'.$theme[$i].'" '.'data-name="'.$name.'" data-set_default_skin="'.$set_default_skin.'" style="width:100%; margin: 6px 0 18px 0;">테마적용</button>';
        }
    ?>
  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="thumbnail tmli_if">
      <?php echo $screenshot; ?>
      <div class="caption">
        <h5><?php echo get_text($info['theme_name']); ?></h5>
        <p><?php echo $btn_active; ?></p>
        <p class="text-right">
		<a href="./theme_preview.php?theme=<?php echo $theme[$i]; ?>" class="btn btn-primary" role="button" target="theme_preview">미리보기</a>
        <button type="button" class="btn btn-default tmli_dt theme_preview" role="button" data-theme="<?php echo $theme[$i]; ?>" data-toggle="modal" data-target="#Modal" class="btn btn-default">상세보기</button>
		</p>
      </div>
    </div>
  </div>
    <?php
    }
    ?>
</div>
  
<?php } else { ?>
<p class="no_theme">설치된 테마가 없습니다.</p>
<?php } ?>

<?php
include_once ('./admin.tail.php');
?>