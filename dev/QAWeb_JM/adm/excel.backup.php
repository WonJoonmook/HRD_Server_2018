<?
$sub_menu = "999200";
include_once('./_common.php');

auth_check($auth[$sub_menu], "r");

$token = get_token();

if ($is_admin != "super")
    alert("최고관리자만 접근 가능합니다.");

$g5[title] = "엑셀 백업";
include_once("./admin.head.php");
?>


<script> 
function goto_xls() 
{ 
var addr_sel = $("#addrbook").val();
var addr_day = $("#extractday").val();


if(addr_sel == ""){
  alert("주소록을 선택하지 않았습니다.");
  $("#addrbook").focus();
  return false;
}
    
if(addr_day == ""){
  alert("날짜를 선택해 주십시오.");
  $("#extractday").focus();
  return false;
}    
    
document.location.href = './excel.print.php?addr_sel='+addr_sel+'&addr_day='+addr_day; 
} 
</script>

<div class="row">
<div class="col-md-6">
<div class=" input-group">
<span class="input-group-addon" id="basic-addon1">주소록 종류</span>
<select size="1" id="addrbook" name="addrbook" class="form-control">
	<option value="" selected="selected">주소록 선택</option>
	<option value="1">주소록백업</option>    
	<option value="2">아웃룩</option>
	<option value="3">네이버</option>    
	<option value="4">구글</option>
	<option value="5">카페24대량</option>                
</select>
</div>
</div>

<div class="col-md-6">
 <div class="input-group">
 <input type="text" id="extractday" name="extractday" class="form-control hasDatepicker" placeholder="가입일"/>
 <span class="input-group-addon" id="basic-addon2">예) 2016-11-11</span>
 </div>
</div>
</div>

<div class="clearfix"></div>
<hr />
<div class="text-center">
<div class="text-danger">* 중복 백업을 방지하기위해 가입일자 이후 회원들만 출력합니다.</div><br />
<div class="text-info well">
메일받기가 체크된 회원만 추출하려면 adm/excel.print.php 파일 8번째줄 빈란에 숫자 1 넣고 저장하세요<br />
<code>$sql = " SELECT * FROM $g[member_table] where mb_mailling =<kbd>'1'</kbd> and mb_datetime >= '$ext'";</code><br />
</div>
<br />
<input type="button" id="btn_xls" value=' 엑셀출력 ' onclick="goto_xls();" class="btn btn-primary "/>
</div>

