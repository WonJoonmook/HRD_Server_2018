<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

include_once('./head.sub.php');
add_stylesheet('<link rel="stylesheet" href="./css/style.css">', 0);

$adm_menu = array(
    array('url' => 'index.php',       'text' => '설정')
);
?>

<div id="wrapper">

<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="./index.php">테마관리</a></a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li>
                    <a href="<?php echo G5_URL.'/index.php' ?>"><i class="fa fa-home fa-fw fa-2x" aria-hidden="true"></i></a>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                            <?php
                            foreach($adm_menu as $menu) {
                                if(basename($_SERVER['SCRIPT_NAME']) == $menu['url'])
                                    $menu_on = ' class="active"';
                                else
                                    $menu_on = '';
                            ?>
                                <li<?php echo $menu_on; ?>><a href="./<?php echo $menu['url']; ?>"><?php echo $menu['text']; ?></a></li>
                            <?php
                            }
                            ?>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>
		
<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> <?php echo $g5['title'] ?>
                            </li>
                        </ol>
					</div>
				</div>