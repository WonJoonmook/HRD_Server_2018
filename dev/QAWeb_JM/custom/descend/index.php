
<?php

	include_once('../common.php');
	include_once(G5_PATH.'/head.php');
	include_once('../define.php');
	include_once('../define_idtable.php');

	$title_category = "강림 관리";
	include_once('../title_category.php');

	set_error_handler("customError");

	//echo "<form method=\"get\">";
	$filePath = PATH_FILE_DESCEND;
	$file = fopen($filePath, "r");

	$buffer = fgets($file);
	$str = str_replace('"','',$buffer);
	$arr_colName = explode(",", trim($str));

	echo "<div class=\"table-responsive\">
			<table id=descendTable class=\"table table-bordered table-striped\">
				<thead>
					<tr>";
					foreach ( $arr_colName as $colName )
        				echo "<th scope=\"col\">".$colName."</th>";
    echo "			</tr>
				</thead>
				<tbody>";

	$colCount = count($arr_colName);
	$resultData = array();
	$descendCount = 0;
	$todayDate = date('ymd');
	$todayIdx=9999;



	while (!feof($file)) {
		
		$buffer = fgets($file);
		$str = str_replace('"','',$buffer);
		$str = explode(",", trim($str));
		if ( count($str) !== $colCount )
			continue;
		
		for ( $i=0; $i<count($str); $i++ ) {
			$resultData[$descendCount][] = $str[$i];
		
		}
		if ( $str[0] == $todayDate )
			$todayIdx = $descendCount;

        $descendCount++;
	}

	$deleteMaxIdx = $todayIdx-14;
	if ( $deleteMaxIdx > 0 ) {
		$tempData = array();
		$len = $descendCount - $deleteMaxIdx;
		for ( $i=0; $i<$len; $i++ ) {
			$tempData[$i] = $resultData[$i+$deleteMaxIdx];
		}
		$descendCount -= $deleteMaxIdx;
		$resultData = $tempData;
	}

	for ( $j=0; $j<$descendCount; $j++ ) {
		echo "<tr>";

		for ( $i=0; $i<count($resultData[$j]); $i++ ) {
			if ( $i==2 ) {
				$characId = (int)substr($resultData[$j][$i], 1,3);
				echo "<td>".$arr_resourceName[$characId]."</td>";
			}
			else {
				echo "<td>".$resultData[$j][$i]."</td>";
			}
		}

		echo "<td>";
			echo "<button type=\"button\" class=\"btn btn-warning\" onclick=\"onModify($j)\">수정</button>";
		echo "</td>";
        echo "</tr>";
	}

	echo "		</tbody>
			</table>
		</div>";
	fclose($file);

?>

<script>
	var arr_resourceName = <?php echo json_encode($arr_resourceName)?>;
	var resultData = <?php echo json_encode($resultData)?>;
	var descendCount = <?php echo $descendCount?>;
</script>





<button class="btn-default" onclick="onAdd( )" >
	<img src="<?php echo G5_URL ?>/img/btn_plus.png" width="15" height="15" >
	일정 추가
</button>

<br><br><br>
<button type="button" class="btn btn-primary" onclick="onApply()">변경내용 저장</button>








<script>
	function GetMaxDays(month) {
		switch(month) {
			case 1:case 3:case 5:case 7:case 8:case 10:case 12:
				return 31;
			case 2:
				return 28;
			default:
				return 30;
		}
	}

	function addRow(idx) {
		
		
		var my_tbody = document.getElementById('descendTable');
	    var row = my_tbody.insertRow( idx+1 );		//	(idx+1 입력)
	    var cell1 = row.insertCell(0);
	    var cell2 = row.insertCell(1);
	    var cell3 = row.insertCell(2);
	    if ( idx == 0 ) {
	    	var yyMMdd = resultData[idx][0];
		    var yy = Number(yyMMdd.substring(0, 2));
		    var mm = Number(yyMMdd.substring(2, 4));
		    var dd = Number(yyMMdd.substring(4, 6));
			
			dd--;
			if ( dd <= 0 ) {
				mm--;
				if ( mm <= 0 ) {
					mm = 12;
					yy--;
				}
				dd = GetMaxDays(mm);
			}
			if ( dd < 10 )
				dd = "0"+dd;
			if ( mm < 10 )
				mm = "0"+mm;
			
	        cell1.innerText = yy+""+mm+""+dd;
	    }
	    else {
		    var yyMMdd = resultData[idx-1][0];
		    var yy = Number(yyMMdd.substring(0, 2));
		    var mm = Number(yyMMdd.substring(2, 4));
		    var dd = Number(yyMMdd.substring(4, 6));
			
			dd++;
			if ( dd > GetMaxDays(mm) ) {
				dd = 1;
				mm++;
			}
			if ( mm > 12 ) {
				mm = 1;
				yy++;
			}
			if ( dd < 10 )
				dd = "0"+dd;
			if ( mm < 10 )
				mm = "0"+mm;
			
	        cell1.innerText = yy+""+mm+""+dd;
	    }
	    cell2.innerText = "24";
	    cell3.innerHTML = getHTML_SelectDescend(idx);	//make list_charac+idx

        var newItem = [(yy+""+mm+""+dd), "24", "0"];
        resultData = addArr(resultData, idx, newItem);
        descendCount++;
	}
	function onAdd() {
		
	    addRow(descendCount);
	}

	function onModify(idx) {
		var my_tbody = document.getElementById('descendTable');
	    if (my_tbody.rows.length < 1) return;
	    my_tbody.deleteRow(idx+1);
	    
	    resultData = subArr(resultData, idx);
	    descendCount--;
		addRow(idx);
	}
</script>

<script>

	function onApply() {
		for ( var i=0; i<document.forms.length; i++ ) {
			var formId = document.forms[i].id;
			idx = formId.replace("addedForm", "");
			var characId = Number(document.getElementById('list_charac'+idx).value);

			resultData[idx][2] = (characId<100 ? '10' : '1')+characId+'0';
		}


		var result = "";
		var my_tbody = document.getElementById('descendTable');
		var headerNames = "";
		for ( var i=0; i<my_tbody.rows[0].cells.length; i++ )
			headerNames += my_tbody.rows[0].cells[i].innerText+",";

	    $.ajax({
	        type: "POST",
	        url: "./ajax.saveDescend.php",
	        data: {
	            "resultData": resultData,
	            "headerNames": headerNames
	        },
	        cache: false,
	        async: false,
	        success: function(data) {
	            result = data;
	        }
	    });
	    
	    result = result.substring(1,result.length);
	    if ( result != "Success") {
	    	alert("!!!변경사항 적용에 실패하였습니다!!!");	
	    }
	    else {
	    	alert("변경사항이 적용되었습니다.");
	    }
	}

</script>

<script>
	function subArr(arr, idx) {
		var newArr = new Array(arr.length-1);
		for ( var i = 0; i<idx; i++ )
			newArr[i] = arr[i];
		for ( var i = idx; i<newArr.length; i++ )
			newArr[i] = arr[i+1];
		return newArr;
	}
	function addArr(arr, idx, newValue) {
		var newArr = new Array(arr.length+1);
		for ( var i = 0; i<idx; i++ )
			newArr[i] = arr[i];
		newArr[idx] = newValue;
		for ( var i = idx+1; i<newArr.length; i++ )
			newArr[i] = arr[i-1];
		return newArr;
	}
	function showSub(grade, id) {
		// 영웅번호 범위 얻기
		<?php 
			$filePath = PATH_FILE_CHARACINDEX;
			$file = fopen($filePath, "r");
			if ( $file == false ) {
				exec ('sudo chmod 777 '.$filePath);
				$file = fopen( $filePath, "r");
				if ( false == $file ) {
					die("Can't open file = ".$filePath);
					return;
				}
			}

			$grade = 0;
			$buffer = fgets($file);
			$grade++;
			$buffer = fgets($file);

			$hiddenEndIdx = array();
			$hiddenStartIdx = array();
			while (!feof($file)) {
				
				$buffer = fgets($file);
				$str = str_replace('"','',$buffer);
				$str = explode(",", trim($str));

				$hiddenEndIdx[$grade-1] = $str[1];
				$hiddenStartIdx[$grade] = $str[3];	
		        $grade++;
		        if ( $grade > 7 )
		        	break;
			}
			fclose($file);
		?>
		var hiddenStartIdx = <?php echo json_encode($hiddenStartIdx)?>;
		var hiddenEndIdx = <?php echo json_encode($hiddenEndIdx)?>;
		var startIdx = parseInt(hiddenStartIdx[grade]);
		var endIdx = parseInt(hiddenEndIdx[grade]);
		// ~영웅번호 범위 얻기

	    var str = "";
	    for ( var i=startIdx; i<endIdx; i++ ) {
	    	str += "<option value="+i+">"+arr_resourceName[i]+"</option>";
	    }
	    var list_charac = document.getElementById("list_charac"+id);
	    list_charac.innerHTML = str;
	}

	function getHTML_SelectDescend(id) {
		var str = 
			"<form id=\"addedForm"+id+"\"> <select name=\" \" onChange=\"showSub(this.options[this.selectedIndex].value, "+id+");\"><option value=1>영웅 등급 선택</option> <option value=3>3성 영웅</option> <option value=4>4성 영웅</option> <option value=5>5성 영웅</option> <option value=6>6성 영웅</option> </select> <select name=\"list_charac"+id+"\" id=\"list_charac"+id+"\" style=\"display: ;\"> </select> </form>";
		return str;
	}
</script>






<?php
	include_once(G5_PATH.'/tail.php');
?>

