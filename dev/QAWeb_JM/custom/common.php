<?php
function customError($errno, $errstr) {
	echo "Err Number = ".$errno.", Err Str = ".$errstr;
}

include_once('../../common.php');

$preDir = getcwd();
chdir(PATH_GAMESERVER);

require_once 'lib/Logger.php';
require_once 'common/Config.php';
require_once 'common/DB_Info.php';
require_once 'common/DB.php';
require_once 'common/LOGIN_DB.php';
require_once 'common/log_DB.php';

chdir($preDir);
require_once '../lib/MultiDBConnect.php';

if(!$is_member){
    $preDir = getcwd();
    chdir('../bbs/');
    include_once('./member_confirm.php');
    chdir($preDir);
}

?>
