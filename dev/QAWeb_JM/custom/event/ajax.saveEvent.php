<?php
	include_once('../common.php');
	$bannerId   = (int)$_POST['bannerId'];
	$eventId   = (int)$_POST['eventId'];
	$filePath  = $_POST['filePath'];
	$fr_date  = $_POST['fr_date'];
	$to_date  = $_POST['to_date'];
	$Action= $_POST['Action'];	//Del, Add, Mod
	$eventBigId = floor($eventId/100)*100;

	$arr_str = explode("/", $filePath);
	$apcName = $arr_str[count($arr_str)-1];
	$apcName = trim($apcName, ".csv");

	function subArr(&$arr, $idx) {
		$newLen = count($arr)-1;
		$newArr = array();
		for ( $i = 0; $i<$idx; $i++ )
			$newArr[$i] = $arr[$i];
		for ( $i = $idx; $i<$newLen; $i++ )
			$newArr[$i] = $arr[$i+1];
		$arr = $newArr;
	}
	
	function FindBigEventIdAndPlusValue(&$eventFileContents, $eventBigId, $addVal) {
		for ( $i=count($eventFileContents)-1; $i>=0; $i-- ) {
			if ( ((int)$eventFileContents[$i][0]) == $eventBigId ) {
				$eventFileContents[$i][1] = (int)$eventFileContents[$i][1] + $addVal;
				if ( $eventFileContents[$i][1] == 0 )
					subArr($eventFileContents, $i);
				return true;
			}	
		}
		return false;
	}

	$file = fopen($filePath, "r");
	if ( $file == false ) {
		exec ('sudo chmod 777 '.$filePath);
		$file = fopen( $filePath, "r");
		if ( false == $file ) {
			die("Can't open file = ".$filePath);
			return;
		}
	}
	
	$eventFileTitles = array();
	$eventFileContents = array();

	$buffer = fgets($file);
	$buffer = str_replace('"','',$buffer);
	$buffer = explode(",", trim($buffer, ","));
	$eventFileTitles = $buffer;

	$colCount = count($buffer);
	$checked = false;
	$preEventId = 100;
	$startEventId = $eventBigId;

	$toDeleteApcList = array();
	while (!feof($file)) {
		
		$buffer = fgets($file);
		$buffer = str_replace('"','',$buffer);
		$buffer = explode(",", trim($buffer, ","));
		$toDeleteApcList[] = $buffer[0];
		if ( $Action == "Del" ) {
			if ( $eventId == (int)$buffer[0] ) {
				FindBigEventIdAndPlusValue($eventFileContents, $eventBigId, -1);
				$checked = true;
				continue;
			}
			else {
				if ( (floor($buffer[0]/100)*100) == $eventBigId )
					$buffer[0] = $startEventId++;
			}
		}
		else if ( $Action == "Mod" ) {
			if ( $eventId == (int)$buffer[0] ) {
					
				if ( !$checked ) {
					$buffer[1] = $fr_date;
					$buffer[2] = $bannerId;
					$time_todate = strtotime('20'.$to_date.'0000');
					$time_frdate = strtotime('20'.$fr_date.'0000');
					$buffer[3] = intval(($time_todate-$time_frdate)/3600); //계산
					$buffer[4] = $to_date."\n";
					$checked = true;
				}

			}
		}
		else {	//Add
			if ( !$checked && $eventId < (int)$buffer[0] ) {
				if ( floor($eventId/100) !== floor((int)$buffer[0]/100) ) {
					$eventId = $preEventId+1;
					$result = FindBigEventIdAndPlusValue($eventFileContents, $eventBigId, 1);
					if ( false == $result ) {
						$eventFileContents[] = array($eventBigId, 1, "", "", "\n");
						$eventId = $eventBigId+1;
					}
					$time_todate = strtotime('20'.$to_date.'0000');
					$time_frdate = strtotime('20'.$fr_date.'0000');
					$continueTime = intval(($time_todate-$time_frdate)/3600);
					$eventFileContents[] = array($eventId, $fr_date, $bannerId, $continueTime, $to_date."\n");
					$checked = true;
				}
			}
		}
		if ( count($buffer) > 2 ) {
			$eventFileContents[] = $buffer;
			$preEventId = (int)$buffer[0];
		}
	}
	if ( $Action == "Add" && !$checked ) {
		$eventId = $preEventId+1;
		$result = FindBigEventIdAndPlusValue($eventFileContents, $eventBigId, 1);
		if ( false == $result ) {
			$eventFileContents[] = array($eventBigId, 1, "", "", "\n");
			$eventId = $eventBigId+1;
		}
		$time_todate = strtotime('20'.$to_date.'0000');
		$time_frdate = strtotime('20'.$fr_date.'0000');
		$continueTime = intval(($time_todate-$time_frdate)/3600);
		$eventFileContents[] = array($eventId, $fr_date, $bannerId, $continueTime, $to_date."\n");
	}
	fclose($file);

	$file = fopen( $filePath, "w");
	if ( $file == false ) {
		exec ('sudo chmod 777 '.$filePath);
		$file = fopen( $filePath, "w");
		if ( $file == false )
			die("Fail Open File\n".$filePath);
	}
	
	$resultStr = "";
	foreach ( $eventFileTitles as $title )
		$resultStr .= $title.",";
	$resultStr = trim($resultStr, ",");
	fputs($file, $resultStr);

	$toDeleteAPC = new APCIterator('user', '/^'.$apcName.'/', APC_ITER_VALUE);
	if ( false == apc_delete($toDeleteAPC) ) {
		die("Fail Del APC");
		return;
	}
//	apc_delete($toDeleteApcList);

	$returnStr = "";
	$len = count($eventFileContents);
	for ( $i=0; $i<$len; $i++ ) {
		$str="";
		for ( $j=0; $j< count($eventFileContents[$i]); $j++ ) {
			$str .= $eventFileContents[$i][$j].",";
		}
		apc_store($apcName.'_'.$eventFileContents[$i][0], $eventFileContents[$i]);
		$returnStr .= $apcName.'_'.$eventFileContents[$i][0]."\n";
		$str = trim($str, ",");
		fputs($file, $str);
	}
	fclose($file);

	if ( false == strpos($apcName, 'QA') ) {
		foreach ( $productions as $production ) {
			$ssh = 'sudo /usr/bin/rsync -avz --delete '.$filePath." root@".$production.":".PATH_LIVE_DATA;
			$result = exec($ssh);
			if ( empty($result) )
				die("Fail rsync");
		}

		foreach ( $URLs_LIVE_ROOT_QAWEB as $url_live_root_qaWeb ) {
			$post_data = array();
			$post_data['apcName'] = $apcName;
			$post_data['eventFileContentsJson'] = json_encode($eventFileContents);
			$URL = $url_live_root_qaWeb."custom/event/gameserver_updateEvent.php";

			$CH = curl_init();
			curl_setopt($CH, CURLOPT_URL, $URL);
			curl_setopt($CH, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($CH, CURLOPT_POST, 1);
			curl_setopt($CH, CURLOPT_POSTFIELDS, $post_data);
			$result = curl_exec($CH);

			if ( $result != "Success" )
				die ( "gameserver_updateEvent Fail - ".$result );
		}
	}
	die($returnStr." Success".$eventId);
?>
