<?php
	include_once('../../../config.php');
	include_once('../../lib/MultiDBConnect.php');
	function customError($errno, $errstr) {
		echo "Err Number = ".$errno.", Err Str = ".$errstr;
		die ( "Err Number = ".$errno.", Err Str = ".$errstr );
	}
	set_error_handler("customError");

	$country   = $_POST['country'];
	$refresh_hour_600 = $_POST['refresh_hour_600'];
	$fr_date = $_POST['fr_date'];
	$to_date = $_POST['to_date'];

	$db_fr_date = '20'.substr($fr_date, 0, 2).'-'.substr($fr_date, 2, 2).'-'.substr($fr_date, 4, 2).' '.substr($fr_date, 6, 2).':00:00';
	$db_to_date = '20'.substr($to_date, 0, 2).'-'.substr($to_date, 2, 2).'-'.substr($to_date, 4, 2).' '.substr($to_date, 6, 2).':00:00';
	
	
	switch ($country) {
	case 'Korea_QA':
		$dbIdxs = array(0);
		break;
	
	default:
		$dbIdxs = array();
		$storeIds = array(0,1,2);
		foreach ( $storeIds as $storeId ) {
    		$dbIdxs[] = 200 + (int)$storeId;
    		$dbIdxs[] = 500 + (int)$storeId;
    		$dbIdxs[] = 700 + (int)$storeId;
    		$dbIdxs[] = 800 + (int)$storeId;
    	}
		break;
	}
    $multidb = new MultiDB();
    $dbs = array();
    foreach ( $dbIdxs as $dbIdx )
    	$dbs[] = $multidb->get_game_db($country, $dbIdx);




    $sqls[] = "DELETE FROM frdGuerrillaShop WHERE DATEDIFF(startTime, '$db_fr_date') >= 0 AND DATEDIFF(endTime, '$db_to_date') <= 0";
//    $sqls[] = "SELECT shopId FROM frdGuerrillaShop order by shopId desc limit 1";
    $sqls[] = "SELECT MAX(shopId) as shopId FROM frdGuerrillaShop";

    $startDT = new DateTime( '20'.$fr_date.'0000' );
    $endDT	 = new DateTime( '20'.$to_date.'0000' );
    $isNotEnd = true;
    while($isNotEnd) {
    	$str_startDT = $startDT->format('Y-m-d H:i:s');
    	$startDT->add(new DateInterval('PT'.$refresh_hour_600.'H'));
    	$str_endDT = $startDT->format('Y-m-d H:i:s');
    	if ( $startDT >= $endDT ) {
    		$startDT = $endDT;
    		$str_endDT = $startDT->format('Y-m-d H:i:s');
    		$isNotEnd = false;
    	}

		$sqls[] = "INSERT INTO frdGuerrillaShop (shopId, startTime, endTime, tierWeight, reg_date) VALUES ( :shopId, '$str_startDT', '$str_endDT', '1000/1500/2000/2000/300/30', now() )";

    }


	foreach ( $dbs as $db ) {
		$db -> prepare($sqls[0]);
		$row = $db -> execute();
        if ( $row == false ) {
        	die("DB Query Fail - sql:".$sqls[0]);
		    return;
        }

        $db -> prepare($sqls[1]);
       	$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if ($row) {
			$shopId = (int)$row['shopId']+1;
		}
		else {
			$shopId = 10000;
		}

		for ( $i=2; $i<count($sqls); $i++ ) {
			$db -> prepare($sqls[$i]);
			$db -> bindValue(':shopId', $shopId, PDO::PARAM_INT);
			$shopId = $shopId + 1;
			$row = $db -> execute();
	        if ( $row == false ) {
	        	die("DB Query Fail - sql:".$sqls[$i]);
			    return;
	        }
		}
	}


	$apcContents = array();
	$apcContents['refresh_hour_600'] = $refresh_hour_600;

	$apcName = 'EventData'.$country.'600';
	apc_delete($apcName);
	$result = apc_store($apcName, $apcContents);
	if ( $result < 1) {
		die("Fail apc_store EventData200");
		return;
	}

	if ( false == strpos($country, 'QA') ) {
		foreach ( $URLs_LIVE_ROOT_QAWEB as $url_live_root_qaWeb ) {
			$post_data = array();
			$post_data['apcName'] = json_encode(array($apcName));
			$post_data['apcContents'] = json_encode(array($apcContents));
			$URL = $url_live_root_qaWeb."custom/lib/apc_store.php";

			$CH = curl_init();
			curl_setopt($CH, CURLOPT_URL, $URL);
			curl_setopt($CH, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($CH, CURLOPT_POST, 1);
			curl_setopt($CH, CURLOPT_POSTFIELDS, $post_data);
			$result = curl_exec($CH);
			if ( $result != "Success" )
				die ( $result );
		}
	}
	die("Success");
?>

