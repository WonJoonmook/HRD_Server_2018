<?php
	include_once('../../../config.php');
	include_once('../../lib/MultiDBConnect.php');
	function customError($errno, $errstr) {
		echo "Err Number = ".$errno.", Err Str = ".$errstr;
		die ( "Err Number = ".$errno.", Err Str = ".$errstr );
	}
	set_error_handler("customError");
	$rewardArrJson   = $_POST['rewardArr'];
	$country   = $_POST['country'];
	$returnDate = $_POST['returnDate'];

	$apcContents = array();
	$apcContents['rewardArrJson'] = $rewardArrJson;
	$apcContents['returnDate'] = $returnDate;

	$apcName = 'EventData'.$country.'800';
	apc_delete($apcName);
	$result = apc_store($apcName, $apcContents);
	if ( $result < 1) {
		die("Fail apc_store EventData200");
		return;
	}

	if ( false == strpos($country, 'QA') ) {
		foreach ( $URLs_LIVE_ROOT_QAWEB as $url_live_root_qaWeb ) {
			$post_data = array();
			$post_data['apcName'] = json_encode(array($apcName));
			$post_data['apcContents'] = json_encode(array($apcContents));
			$URL = $url_live_root_qaWeb."custom/lib/apc_store.php";

			$CH = curl_init();
			curl_setopt($CH, CURLOPT_URL, $URL);
			curl_setopt($CH, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($CH, CURLOPT_POST, 1);
			curl_setopt($CH, CURLOPT_POSTFIELDS, $post_data);
			$result = curl_exec($CH);
			if ( $result != "Success" )
				die ( $result );
		}
	}
	die("Success");
?>
