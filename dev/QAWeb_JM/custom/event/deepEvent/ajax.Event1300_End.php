<?php
	include_once('../../../config.php');
	include_once('../../lib/MultiDBConnect.php');
	function customError($errno, $errstr) {
		echo "Err Number = ".$errno.", Err Str = ".$errstr;
		die ( "Err Number = ".$errno.", Err Str = ".$errstr );
	}
	set_error_handler("customError");
	$resetDB			= (int)$_POST['resetDB'];
	$country   			= $_POST['country'];

	
	switch ($country) {
	case 'Korea_QA':
		$dbIdxs = array(0);
		break;
	
	default:
		$dbIdxs = array();
		$storeIds = array(0,1,2);
		foreach ( $storeIds as $storeId ) {
    		$dbIdxs[] = 200 + (int)$storeId;
    		$dbIdxs[] = 500 + (int)$storeId;
    		$dbIdxs[] = 700 + (int)$storeId;
    		$dbIdxs[] = 800 + (int)$storeId;
    	}
		break;
	}
    $multidb = new MultiDB();
    $dbs = array();
    foreach ( $dbIdxs as $dbIdx )
    	$dbs[] = $multidb->get_game_db($country, $dbIdx);

    if ( $resetDB > 0 ) {
	    $sqls[] = "DROP TABLE IF EXISTS `Event_ClimbEnterReward`";
	    $sqls[] = "CREATE TABLE `Event_ClimbEnterReward` (
					 `userId` int(11) NOT NULL,
					 `startYMD` int(11) NOT NULL,
					 `lastYMD` int(11) NOT NULL,
					 `totalChk` smallint(6) NOT NULL,
					 PRIMARY KEY (`userId`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8";

		
		foreach ( $dbs as $db ) {
			foreach ( $sqls as $sql ) {
				$db -> prepare($sql);
				$row = $db -> execute();
	            if ( $row == false ) {
	            	die("DB Query Fail - sql:".$sql);
				    return;
	            }
			}
		}
		die("Success and Reset db");
	}
	else
		die("Success and No Reset db");
?>
