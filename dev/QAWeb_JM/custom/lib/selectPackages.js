
    // $(document).ready(function() {
    //     alert("isLoad = "+isLoad);
    //     if ( false == isLoad )
    //         return;
    // });

    var temp_selected_rewardType;
    var temp_selected_nextId=1;
    var arr_packages;

    function LoadSelectPackages() {
        var apcNames = [
            ('EventSelectPackages'+GetCountry()+loadedBannerId)
        ];

        $.ajax({
            type: "POST",
            url: "../lib/apc_fetch.php",
            data: {
                "apcName": JSON.stringify(apcNames)
            },
            cache: false,
            async: false,
            success: function(data) {
                result = data;
            }
        });

        var apcContents = JSON.parse(result);
        var arr_packageNames = apcContents[0];
        for ( var k=0; k<arr_packageNames.length; k++ ) {

            var packageName = arr_packageNames[k];
            for ( var i=0; i<arr_packages.length; i++ ) {
                if ( packageName == arr_packages[i]['name'] ) {
                    $("#sel_packages"+k+" option:eq("+(i+1)+")").attr("selected", "selected");
                    break;
                }
            }
            onAppend_SP(this);

        }
    }

    function GetSelectPackagesHTML(callback) {

        $.ajax({
            type: "POST",
            url: "../lib/mysql_query.php",
            data: {
                "host"     : "172.27.100.10",
                "user"     : "root",
                "password" : "eoqkrskwk12",
                "port"     : "3306",
                "database" : "hrd_db_test",
                "query"    : "SELECT name FROM frdShopData where shopItemId >= 6000"
            },
            cache: false,
            async: false,
            success: function(data) {
                result = data;
            }
        });
        arr_packages = JSON.parse(result);

        var startIdx = 0;

        var returnHTML = "";
        returnHTML += "<div><button type=\"button\" class=\"btn-primary\" onclick=\"onAppend_SP(this)\" >패키지 추가</button></div>";
       
        returnHTML += "<div>";
        returnHTML += "<select id=\"sel_packages"+startIdx+"\">";
        returnHTML += "<option value=-1>종류 선택</option>";
        for ( var i=0; i<arr_packages.length; i++ ) {
            var name = arr_packages[i]['name'];
            returnHTML += "<option value="+i+">"+name+"</option>";
        }

        returnHTML +="</select>";
        returnHTML += "</div>";
        callback( returnHTML );
    }


    function GetSelectHTML_SP(idx) {

        var returnHTML = "<div>";
        returnHTML += "<select id=\"sel_packages"+idx+"\">";
        returnHTML += "<option value=-1>종류 선택</option>";
        for ( var i=0; i< arr_packages.length; i++ ) {
            var name = arr_packages[i]['name'];
            returnHTML += "<option value="+i+">"+name+"</option>";
        }

        returnHTML +="</select>";
        returnHTML +="<button style=\"margin-left:10px;\" class=\"btn-danger\" onclick=\"onDelete_SP(this)\">x</button><br>";
        returnHTML +="</div>";
        return returnHTML;
    }

    function onDelete_SP(obj) {
        document.getElementById('deepEventInform').removeChild(obj.parentNode);
    }

    function onAppend_SP(obj) {
        document.getElementById('deepEventInform').insertAdjacentHTML( 'beforeend', GetSelectHTML_SP(temp_selected_nextId++) );
    }

    function PoppackagesAndApcstore(bannerId, path_custom_lib) {
        var resultArr = [];

        var pointer =0;
        for ( var i=0; i<temp_selected_nextId; i++ ) {
            var selName = 'sel_packages'+i;
            var sel = document.getElementById(selName);
            if ( sel && sel.value && sel.value != -1 ) {
                resultArr[pointer++] = arr_packages[sel.value]['name'];
            }
        }

        var apcNames = [
            ('EventSelectPackages'+GetCountry()+bannerId)
        ];
        var apcContents = [
            resultArr
        ];

        $.ajax({
            type: "POST",
            url: path_custom_lib+"apc_store.php",
            data: {
                "apcName": JSON.stringify(apcNames),
                "apcContents": JSON.stringify(apcContents)
            },
            cache: false,
            async: false,
            success: function(data) {
                result = data;
            }
        });
        if ( result != "Success" ) {
            alert("!!!이벤트 정보 APC STORE에 실패하였습니다!!!<br>"+result);    
        }

        return resultArr;
    }
