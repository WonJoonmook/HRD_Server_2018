<?php

    $host   = $_POST['host'];
    $user   = $_POST['user'];
    $password   = $_POST['password'];
    $port   = $_POST['port'];
    $database   = $_POST['database'];
    $query   = $_POST['query'];

    $dsn= "mysql:dbname=" . $database . ";host=" . $host . ";port=" . $port;
    $db = new PDO($dsn, $user, $password, array(PDO::ATTR_PERSISTENT => true));

    $res = $db -> query($query);
    if ( false == $res ) {
        echo "Fail - ".$query."\n";
        mysqli_error($db);
    }
    else {
        $arr_return = array();
        while( $row = $res -> fetch(PDO::FETCH_ASSOC) )
            $arr_return[] = $row;
              
        echo json_encode($arr_return);
    }
?>

