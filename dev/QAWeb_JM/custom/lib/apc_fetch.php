<?php
    $apcNameJson          = $_REQUEST['apcName'];
    if ( empty($apcNameJson) ) {
        echo "Fail Get Contents";
        return;
    }

    $arr_apcName = json_decode($apcNameJson);
    $arr_apcContents = array();
    for ( $i=0; $i<count($arr_apcName); $i++ ) {
        $apcName = $arr_apcName[$i];
        $arr_apcContents[] = apc_fetch($apcName);
    }

    echo json_encode($arr_apcContents);
?>