<?php
$arr_country = array();
$arr_country[] = array('name'=>'한국', 'id'=>'Korea');
$arr_country[] = array('name'=>'한국 QA', 'id'=>'Korea_QA');
$arr_country[] = array('name'=>'중국 QA', 'id'=>'China_QA');

$arr_platform = array();
$arr_platform[] = "플레이스토어";
$arr_platform[] = "원스토어";
$arr_platform[] = "앱스토어";
$arr_platform[] = "모든 스토어";

$arr_searchRange = array();
$arr_searchRange[] = "소규모 검색";
$arr_searchRange[] = "중규모 검색";
$arr_searchRange[] = "대규모 검색";

$arr_userRankName = array();
$arr_userRankName[] = "";
$arr_userRankName[] = "브론즈";
$arr_userRankName[] = "실버";
$arr_userRankName[] = "골드";
$arr_userRankName[] = "플레티넘";
$arr_userRankName[] = "다이아";
$arr_userRankName[] = "마스터";
$arr_userRankName[] = "첼린저";
$arr_userRankName[] = "마스터첼린저";
$arr_userRankName[] = "히어로";
$arr_userRankName[] = "마스터히어로";
$arr_userRankName[] = "레전더리";
$arr_userRankName[] = "페르소나";
$arr_userRankName[] = "이터널";

$arr_userTierName = array();
$arr_userTierName[] = "";
$arr_userTierName[] = "1";
$arr_userTierName[] = "2";
$arr_userTierName[] = "3";
$arr_userTierName[] = "4";
$arr_userTierName[] = "5";

$arr_resource = array();
$arr_resource[] = array('name'=>"골드", 'itemIdx'=>5000, 'table'=>'frdUserData', 'colName'=>'gold');
$arr_resource[] = array('name'=>"티켓", 'itemIdx'=>5002, 'table'=>'frdUserData', 'colName'=>'ticket');
$arr_resource[] = array('name'=>"행동력", 'itemIdx'=>5003, 'table'=>'frdUserData', 'colName'=>'heart');
$arr_resource[] = array('name'=>"보석", 'itemIdx'=>5004, 'table'=>'frdUserData', 'colName'=>'jewel');
$arr_resource[] = array('name'=>"누적메달", 'itemIdx'=>5005, 'table'=>'frdUserData', 'colName'=>'accuSkillPoint');
$arr_resource[] = array('name'=>"마법가루", 'itemIdx'=>5006, 'table'=>'frdUserData', 'colName'=>'powder');

$arr_resource[] = array('name'=>"무지개룬", 'itemIdx'=>110000, 'table'=>'frdHavingItems', 'colName'=>'itemCount', 'condColName'=>'itemId');

?>

<script>
	var arr_platform = <?php echo json_encode($arr_platform)?>;
	var arr_userRankName = <?php echo json_encode($arr_userRankName)?>;
	var str_itemTable = "골드    : 5000\n경험치 : 5001\n티겟   : 5002\n번개   : 5003\n메달   : 5005\n가루   : 5006\n영웅ID : 0~500, 501~506 (1~6성 랜덤)\n유물ID : 1000~1500 (0~500), 1501~1506 (1~6성 랜덤)\n마법ID : 10000~10500 (0~500), 10501~10506 (1~6성 랜덤)";
</script>
