<?php
include_once('../common.php');
?>

<div class="modal-dialog">
  <div class="modal-content">
    
    <div class="modal-body">
      <div class="table-responsive">
        <form action="./UserManager.php?country=<?php echo $country?>&CMD=user_kick&userId=<?php echo $userId?>" name="form_option" method="post">

          <strong>접속 종료를 진행 하시겠습니까 ?</strong>
          <a id="form_option_submit" onclick="return form_option_submit();" class="btn btn-default"><i class="fa fa-rotate-left"></i>
              확인
            </a>
            <button type="button" class="btn btn-default" data-dismiss="modal" onclick="window.location.reload();"><i class="fa fa-times-circle"></i> Close</button>
          
        </form>

        <script>
          function form_option_submit(){
            document.form_option.submit();
          }
        </script>
      </div>
    </div>
  </div>
</div>
