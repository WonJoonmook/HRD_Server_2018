
<?php
	include_once('../common.php');
	include_once(G5_PATH.'/head.php');
	include_once('../define.php');

	$title_category = "일괄 우편 전송";
	include_once('../title_category.php');

	if ( empty($country) )
		$country = "Korea";
	if ( empty($platform) )
		$platform = 0;
?>

<script>

	function set_reward_select_box_value_add() {
		var reward_type = document.getElementById('item_group').value;
		var get_reward_item_value = document.getElementById('reward_item');
		var get_reward_value = document.getElementById('item_count');
		var get_reward_summon_value = document.getElementById('reward_summon');
		var get_reward_select_box_value = document.getElementById('reward_select_box_value');
		var get_reward_select_box_target = document.getElementById('reward_select_box_target');
		var get_reward_select_box_any = document.getElementById('reward_select_box_any');

		if (reward_type == 'select_box_summon' || reward_type == 'select_box_item' || reward_type == 'select_box_any') {
			if (get_reward_value.value.length <= 1) {
				if (reward_type == 'select_box_item') {
					get_reward_value.value = get_reward_item_value.value;
					get_reward_select_box_target.value = '1';
				} else if (reward_type == 'select_box_summon') {
					get_reward_value.value = get_reward_summon_value.value;
					get_reward_select_box_target.value = '2';
				} else if (reward_type == 'select_box_any') {
					get_reward_value.value = get_reward_select_box_value.value;
					get_reward_select_box_target.value = get_reward_select_box_any.value;
				}
			} else {

				var reward_value_arr = get_reward_value.value.split('/');
				if (reward_value_arr.length > 4) {
					alert('5개 이상 입력불가.');
					return;
				}

				if (reward_type == 'select_box_item') {
					get_reward_value.value = get_reward_value.value + '/' + get_reward_item_value.value;
					get_reward_select_box_target.value = get_reward_select_box_target.value + '/1';
				} else if (reward_type == 'select_box_summon') {
					get_reward_value.value = get_reward_value.value + '/' + get_reward_summon_value.value;
					get_reward_select_box_target.value = get_reward_select_box_target.value + '/2';
				} else if (reward_type == 'select_box_any') {
					get_reward_value.value = get_reward_value.value + '/' + get_reward_select_box_value.value;
					get_reward_select_box_target.value = get_reward_select_box_target.value + '/' + get_reward_select_box_any.value;
				}
			}
		}
	}
</script>


    <!-- <form id="fsearch" name="fsearch" class="form-inline" method="post">
    	
	</form> -->


	
<div class="well well-sm">

	<form id="sumbitToSend" class="form-inline" action="./UserManager.php?country=<?php echo $country?>&CMD=<?php echo "all_send"?>&userId=<?php echo $userId?>&memo=<?php echo $memo?>" name="form_option" method="post">
	
		<div class="col-md-11.5 col-md-offset-0.5">

            <select name="country" id="country" class="form-control">
            	<?php 
        			for ( $i=0; $i<count($arr_country); $i++ ) {
        				$id = $arr_country[$i]['id'];
        				echo "<option value=".$id.(($id==$country)?" selected>":">").$arr_country[$i]['name']."</option>";
        			}
        		?>
            </select>

            <select name="platform" id="platform" class="form-control">
            	<?php 
            		for ( $i=0; $i<count($arr_platform); $i++ )
        				echo "<option value=".$i.(($platform==$i)?" selected>":">").$arr_platform[$i]."</option>";
        		?>
            </select>

            <button type="nosubmit" class="btn btn-default" id="button1" onclick="onShowItemTable()">
				<strong>아이템 테이블</strong>
			</button>

	    </div>

	    <br>
	    <div class="col-md-11.5 col-md-offset-0.5">
			<input type='text' style="width:10%;" maxlength='6' name="item_idx" id="item_idx" onkeypress="if (event.keyCode<48|| event.keyCode>57) event.returnValue=false;" style='IME-MODE:disabled;' required="" class="required form-control" placeholder="아이템 ID"> 

			<input type='text' style="width:10%;" maxlength='6' name="item_count" id="item_count" onkeypress="if (event.keyCode<48|| event.keyCode>57) event.returnValue=false;" style='IME-MODE:disabled;' required="" class="required form-control" placeholder="아이템 갯수"> 

		
			<select style="width:10%;" name="mail_msg" class="form-control">
				<option value="11-1">운영자지급</option>
			</select>
		</div>

		<br>
		<div class="col-md-11.5 col-md-offset-0.5">
			<strong>메모</strong>
			<input type="text" name="memo" value="" id="memo"  class="required form-control" placeholder="<?php echo $user_cash?>" required>
		</div>

		<br>
		<hr>

	</form>

	<button class="btn btn-default" class="btn btn-primary" onclick="button_event()">
		<strong>전송</strong>
	</button>
</div>


<script>
	function onShowItemTable() {
		alert(str_itemTable);
	//	alert(str_itemTable);
	}

	function button_event(){
		var country = document.getElementById('country').value;
		var platform = document.getElementById('platform').value;
		var item_idx = document.getElementById('item_idx').value;
		var item_count = document.getElementById('item_count').value;

	//	var informStr = country+", "+arr_platform[platform]+", "+item_idx+", "+item_count;
		var informStr = country+"의 "+ arr_platform[platform]+" 모든 유저에게\n상품 ID="+item_idx+"\n상품 갯수="+item_count+"\n를 지급하는것이 정말 맞습니까?";

		if ( confirm(informStr) ) {
			document.getElementById('sumbitToSend').submit();
		}
	}

</script>



<br>
#. 일괄지급은 DB에 부하를 주는 작업 입니다.
<br>
#. 알림창이 확인 된후 페이지 이동 또는 다음 작업을 진행 하시기 바랍니다. 
<br>
#. 라이브 서버에서 실제 이용시 분단위로 시간이 소요 됩니다. 
<br>
<br>
<br><hr>



<?php
	include_once(G5_PATH.'/tail.php');
?>