
<?php
	$title_category = "슈퍼 계정 생성";
	include_once('./manage_users_head.php');
?>

<h4>
<strong><mark>
	닉네임 : <?php echo $loginRow['name']?><br><p></p>
	e-mail : <?php echo $loginRow['email']?><br><p></p>
	유저ID : <?php echo $loginRow['userId']?><br><p></p>
	플랫폼ID : <?php echo $loginRow['connectId']?><br>

</mark></strong>
</h4>
<br>

<button class="btn btn-primary" onclick="showConfirm()">슈퍼 계정으로 만들기</button>

<script>

	function showConfirm(){

		if ( !confirm("진행 하시겠습니까?") )
		 	return;

		<?php
			$GLOBALS['COUNTRY'] = $country;
			switch ($country) {
				case 'China_QA':
					$GLOBALS['REAL'] = 4;
					break;
				
				default:
					$GLOBALS['REAL'] = 5;
					break;
			}

			$sql = array();
			$sql[] = "DELETE FROM frdCharInfo where userId=$userId";

			$query = "INSERT INTO frdCharInfo (userId, charId, exp) VALUES ";
			for ( $i=0; $i<390; $i++ ) {
				if ( $i !== 0 )
					$query .= ", ";
				$query .= "($userId, $i, 10)";
			}
			$query .= "ON DUPLICATE KEY UPDATE exp = 10";

			$sql[] = $query;
			$sql[] = "UPDATE frdUserData SET tutoLevel=99, userLevel=55, manaLevel=80, gold=123456789, jewel=123456, powder=2345678, stageLevel=99, skillPoint=2200000, accuSkillPoint=2200000, ticket=18000 WHERE  userId =$userId";
			$sql[] = "delete from frdTemple where userId=$userId";
			$sql[] =  "insert into frdTemple values 
					(0, $userId, 1, 5, 5, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00')
					,(0, $userId, 2, 5, 5, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00')
					,(0, $userId, 3, 5, 5, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00')
					,(0, $userId, 4, 5, 5, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00')
					,(0, $userId, 5, 5, 5, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00')
					,(0, $userId, 6, 5, 5, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00')
					,(0, $userId, 7, 5, 5, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00')
					,(0, $userId, 8, 5, 5, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00')
					,(0, $userId, 9, 5, 5, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00')
					,(0, $userId, 10, 5, 5, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00')";

			// $sql[] = "delete from frdAbillity where userId=$userId";
		 //    $sql[] = "insert into frdAbillity values ($userId, 63, 377614, ((1048576<<25)+(1048576<<20)+(1048576<<15)+(1048576<<10)+(1048576<<5)+1048576+32768+1024+32+1), 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00')";
		 //    $sql[] = "delete from frdGoddess where userId=$userId";
		 //    $sql[] = "insert into frdGoddess (userId, goddessId, level, exp) values ($userId, 1, 1, 0),($userId, 2, 1, 0),($userId, 3, 1, 0),
		 //   			 ($userId, 4, 1, 0),($userId, 5, 1, 0),($userId, 6, 1, 0),($userId, 7, 1, 0),($userId, 8, 1, 0),
		 //    		 ($userId, 9, 1, 0),($userId, 10, 1, 0)";

			$sql[] = "delete from frdAbillity where userId=$userId";
		    $sql[] = "insert into frdAbillity values ($userId, 33554431, 9223372036821221376, 1125899906842623, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00')";
		    $sql[] = "delete from frdGoddess where userId=$userId";
		    $sql[] = "insert into frdGoddess (userId, goddessId, level, exp) values ($userId, 1, 5, 0),($userId, 2, 5, 0),($userId, 3, 5, 0),
		   			 ($userId, 4, 5, 0),($userId, 5, 5, 0),($userId, 6, 5, 0),($userId, 7, 5, 0),($userId, 8, 5, 0),
		    		 ($userId, 9, 5, 0),($userId, 10, 5, 0)";
		    
		    $sql[] = "delete from frdHavingItems where userId=$userId";
		    $query = "insert into frdHavingItems (userId, itemId, itemCount) values ";
		    for ( $i=100000; $i<=100006; $i++ )
		      $query .= "($userId, $i, 1000),";
		    for ( $i=101000; $i<=101009; $i++ )
		      $query .= "($userId, $i, 1000),";
		    for ( $i=102000; $i<=102009; $i++ )
		      $query .= "($userId, $i, 1000),";
		    for ( $i=103000; $i<=103009; $i++ )
		      $query .= "($userId, $i, 1000),";
		    for ( $i=104000; $i<=104009; $i++ )
		      $query .= "($userId, $i, 1000),";
		    for ( $i=105000; $i<=105009; $i++ )
		      $query .= "($userId, $i, 1000),";
		    for ( $i=106000; $i<=106009; $i++ )
		      $query .= "($userId, $i, 1000),";
		    for ( $i=111000; $i<=111004; $i++ )
		      $query .= "($userId, $i, 1000),";
		    for ( $i=112000; $i<=112004; $i++ )
		      $query .= "($userId, $i, 1000),";
		    for ( $i=113000; $i<=113004; $i++ )
		      $query .= "($userId, $i, 1000),";
		    for ( $i=114000; $i<=114004; $i++ )
		      $query .= "($userId, $i, 1000),";
		    for ( $i=115000; $i<=115004; $i++ )
		      $query .= "($userId, $i, 1000),";
		    $query .= "($userId, 110000, 1000)";
		    $sql[] = $query;

		    $sql[] = "delete from frdHavingWeapons where userId=$userId";
		    $query = "insert into frdHavingWeapons (userId, weaponId, level) values";
		    for ( $j=0; $j<10; $j++ ) {
			    for ( $i=200; $i<=226; $i++ )
			      $query .= "($userId, $i, 40),";
			}
		  	$query .= "($userId, 200, 40)";
		  	$sql[] = $query;

		  	$sql[] = "delete from frdHavingMagics where userId=$userId";
		  	$query = "insert into frdHavingMagics (userId, magicId, level) values";
		    for ( $i=400; $i<=407; $i++ )
		      $query .= "($userId, $i, 5),";
		  	for ( $i=400; $i<=407; $i++ )
		      $query .= "($userId, $i, 5),";
		  	for ( $i=0; $i<=1; $i++ ) {
			  	$query .= "($userId, 309, 5),";
				$query .= "($userId, 308, 5),";
				$query .= "($userId, 209, 40),";
				$query .= "($userId, 208, 40),";
				$query .= "($userId, 204, 40),";
				$query .= "($userId, 213, 40),";
			}
		  	$query .= "($userId, 400, 5)";
		  	$sql[] = $query;

		  	$queryResult = true;
		    for ( $i=0; $i<count($sql); $i++ ) {
		    	$db -> prepare($sql[$i]);
                $row = $db -> execute();
                if ( $row == false ) {
                	$queryResult = false;
                	error_log('Fail!!! Fail!!!!  userId : ' . $userId . ",\nsql : " . $sql[$i]);
                }
		    }
		   	
		   	if ( $queryResult ) {
                echo "alert(\" 슈퍼 계정으로 전환이 완료되었습니다. \");";
            }
            else {
                echo "alert(\" 슈퍼 계정으로 전환에 실패했습니다. \");";
            }
		?>

		
	}
</script>

<?php
include_once(G5_PATH.'/tail.php');
?>

