<?php
include_once ('../common.php');
?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 
                class="modal-title">패키지 지급
                <small>Send Pakcage</small>
            </h4>
        </div>

        <div class="container"></div>
        <div class="modal-body">
            <div class="table-responsive">
                <form action="./UserManager.php?country=<?php echo $country?>&CMD=package_send&userId=<?php echo $userId?>" name="form_option" method="post">
                    
                    <select name="item_group" id="item_group" class="form-control">
                        <?php 
                            $multidb = new MultiDB();
                            $db = $multidb -> get_game_db("Korea", 200);
                            $sql = "SELECT productId, name, itemId, itemCount FROM frdShopData where shopItemId >= 6000";
                            $db -> prepare($sql);
                            $db -> execute();
                            while ($row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
                                $value = $row['productId'].",".$row['itemId'].",".$row['itemCount'];
                                echo "<option value=".$value.">".$row['name']."</option>";
                            }
                        ?>
                    <!-- 
                        <option value="package_start_new">스타터 패키지</option>
                        <option value="package_honeyjam_new">허니잼 패키지</option>
                        <option value="package_hrd">히랜디 패키지</option>
                        <option value="package_abyss">어빌리티 패키지(어빌리티슬롯지급안됨)</option>
                        <option value="buy_monthly_card0">레아의 사랑열매 패키지(지급초기화금일부터)</option>
                        <option value="package_event_abillity">이벤트 어빌리티 패키지</option>
                        <option value="package_170901_1">상품권 패키지</option>
                        <option value="package_170901_2">룬 패키지</option>
                        <option value="package_170901_4">메달 패키지</option>
                        <option value="package_170901_6">영혼각인석 패키지</option>
                        <option value="package_170901_8">스페셜 패키지</option> -->
                    </select>
 
                    <h4><strong>MEMO</strong></h4>
                     <input type="text" name="memo" value="" id="memo" class="required form-control" placeholder="메모입력">
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-default"><i class="fa fa-rotate-left"></i>확인</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="window.location.reload();"><i class="fa fa-times-circle"></i>Close</button>                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
function form_option_submit(){
    document.form_option.submit();
}                
</script>
