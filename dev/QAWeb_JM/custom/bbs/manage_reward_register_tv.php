<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<?php
	$title_category = "방송 컨텐츠 가입 보상 지급";
	include_once('./manage_users_head.php');
?>

<h4>
<strong><mark>

	닉네임 : <?php echo $loginRow['name']?><br><p></p>
	e-mail : <?php echo $loginRow['email']?><br><p></p>
	유저ID : <?php echo $loginRow['userId']?><br><p></p>
	플랫폼ID : <?php echo $loginRow['connectId']?><br>

</mark></strong>
</h4>
<br>

<button class="btn btn-primary" onclick="showConfirm_RewardRegisterTV()">방송 컨텐츠 가입 보상 지급하기</button>

<script>

	function showConfirm_RewardRegisterTV(){

		if ( !confirm("진행 하시겠습니까?") ) {
		 	return;
		}

		<?php
			$GLOBALS['COUNTRY'] = $country;
			switch ($country) {
				case 'China_QA':
					$GLOBALS['REAL'] = 4;
					break;
				
				default:
					$GLOBALS['REAL'] = 5;
					break;
			}

			$sql = array();

		    $sql[] = "insert into frdUserPost (postTableId, recvUserId, sendUserId, type, count, expire_time) values (0, $userId, 0, 110000, 5, DATE_ADD(now(), INTERVAL 7 DAY )),
		    (0, $userId, 0, 1505, 3, DATE_ADD(now(), INTERVAL 7 DAY )),
		    (0, $userId, 0, 100000, 5, DATE_ADD(now(), INTERVAL 7 DAY )),
		    (0, $userId, 0, 5002, 5, DATE_ADD(now(), INTERVAL 7 DAY )),
		    (0, $userId, 0, 105000, 3, DATE_ADD(now(), INTERVAL 7 DAY )),     
		    (0, $userId, 0, 105001, 3, DATE_ADD(now(), INTERVAL 7 DAY )),   
		    (0, $userId, 0, 105002, 3, DATE_ADD(now(), INTERVAL 7 DAY )),
			(0, $userId, 0, 105003, 3, DATE_ADD(now(), INTERVAL 7 DAY )),
			(0, $userId, 0, 105004, 3, DATE_ADD(now(), INTERVAL 7 DAY )),     
			(0, $userId, 0, 105005, 3, DATE_ADD(now(), INTERVAL 7 DAY )),   
			(0, $userId, 0, 105006, 3, DATE_ADD(now(), INTERVAL 7 DAY )),
		    (0, $userId, 0, 105007, 3, DATE_ADD(now(), INTERVAL 7 DAY )),
		    (0, $userId, 0, 105008, 3, DATE_ADD(now(), INTERVAL 7 DAY )),
		    (0, $userId, 0, 105009, 3, DATE_ADD(now(), INTERVAL 7 DAY ))";
		    
		  	$queryResult = true;

		    for ( $i=0; $i<count($sql); $i++ ) {
		    	$row = $db -> query($sql[$i]);
                if ( $row == false ) {
                	$queryResult = false;
                	error_log('Fail!!! Fail!!!!  userId : ' . $userId . ",\nsql : " . $sql[$i]);
                }
                else {
                	$debug = var_export($row, true);
                	error_log($debug);
                }
		    }
		   	
		   	if ( $queryResult ) {
                echo "alert(\" 방송 컨텐츠 가입 보상이 지급되었습니다!! \");";
            }
            else {
                echo "alert(\" 보상 지급에 실패했습니다!!! \");";
            }
            $db = null;
		?>

	}
</script>

<?php
include_once(G5_PATH.'/tail.php');
?>

