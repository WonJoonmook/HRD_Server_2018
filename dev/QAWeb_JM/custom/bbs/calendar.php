<?php

class calendar
{
    
    /* $id , $name ,$placeholder*/
    function show($id, $name,$placeholder)
    {
        echo "
        <link href='//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/a549aa8780dbda16f6cff545aeabc3d71073911e/build/css/bootstrap-datetimepicker.css' rel='stylesheet'>
        <script src='//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js'></script>
        <script src='//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/a549aa8780dbda16f6cff545aeabc3d71073911e/src/js/bootstrap-datetimepicker.js'></script>
        
        
        <div class='container''>
            <div class='row'>
                <div class='col-sm-6'>
                    <div class='form-group'>
                        <div class='input-group date' id={$id} >
                            <input type='text' value= <?php echo ${$id}?> class='form-control' name={$name} placeholder={$placeholder}>
                            <span class='input-group-addon'>
                                <span class='glyphicon glyphicon-calendar'></span>
                            </span>
                        </div>
                    </div>
                </div>
                <script type='text/javascript'>
                    $(function () {
                        $('#{$id}').datetimepicker({
                            format:'YYYY-MM-DD 00:00:00'
                        });
                    });
                </script>
            </div>
        </div>
        ";
     }
     /* $start_date, $end_date, $callback*/
     function for_date_range_action($start_date,$end_date,$args, $callback)
     {
        $str_fr_date = strptime($start_date, "%Y-%m-%d");
        $str_to_date = strptime($end_date, "%Y-%m-%d");
        $datetime_to_date = date('Ymd', mktime(0, 0, 0, $str_to_date[tm_mon] + 1, $str_to_date[tm_mday] + 1, $str_to_date[tm_year] + 1900));
 
        $range_mon = (($str_to_date[tm_year] + 1900) - ($str_fr_date[tm_year] + 1900)) * 12 + ($str_to_date[tm_mon] + 1) - ($str_fr_date[tm_mon] + 1);

        $year_count = 0;
        $start_day = $str_fr_date[tm_mday];
        $diff_day = 0;
        for ($mon_i = 0; $mon_i <= $range_mon; ++$mon_i) {
            if (($str_fr_date[tm_mon] + 1 + $i % 12) == 0) {
                $year_count++;
                $str_fr_date[tm_mon] = 0;
            }
            $last_day = (int) date("t", mktime(0, 0, 0, $str_fr_date[tm_mon] + 1 + $mon_i, 1, $str_fr_date[tm_year] + 1900 + $year_count));
            $diff_day = $last_day - $start_day;

            for ($day_i = 0; $day_i <= $diff_day; ++$day_i) {
                // echo "<br>";
                $current_day = date('Ymd', mktime(0, 0, 0, $str_fr_date[tm_mon] + 1 + $mon_i, $start_day + $day_i, $str_fr_date[tm_year] + 1900 + $year_count));
                
        
                if ($current_day == $datetime_to_date) {
                    break;
                }
                
                
                /* Do Something...*/
                if(is_callable($callback))
                {
                    call_user_func_array($callback, array($current_day,$args));
                }
                
            }
            $start_day = 1;
        }
     }
}
?>