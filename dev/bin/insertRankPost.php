<?php

$GLOBALS['REAL'] = 5;

include_once("DB.php");
include_once("RedisDB.php");

$date = date('Y-m-d H:i:s');


$climbRankRewards = array(
		"10,5004,400,5000,30000,5005,100,5006,300",
        "100,5004,300,5000,20000,5005,80,5006,200",
        "500,5004,250,5000,10000,5005,60,5006,100",
        "1000,5004,200,5000,8000,5005,40,5006,50",
        "2000,5004,150,5000,6000,5005,20,5002,5",
        "3000,5004,100,5000,4000,5005,10,5002,5",
        "5000,5004,50,5000,2000,5002,3",
        "9999,5000,1000");

for ( $serverId = 0; $serverId < 1; $serverId++ ) {
	if ($serverId == 0) {
		$rankDbName = 'climbRank_New';
	} else { 
//		$rankDbName = 'climbRank_New_1';
	}
	$ymd = date("ymd");
	$fp = fopen("/home/log/climbTopLog/climb_log_".$ymd, 'w'); 
	
	$redis = new RedisDB();
	if ($redis == false) {
		fwrite($fp, "Error: Open RankDB\n");
		fclose($fp);
		return ;
	}

	$fileName = "/home/log/climbTopLog/climbRank_New170626_Rank";
	$file = fopen($fileName, "r");
	$jsonStr = fread($file, filesize($fileName));
	$rankData = json_decode($jsonStr);

	fclose($file);

	// 랭킹 가져오기
//	$rankData = $redis->zrevrange($rankDbName, 0, -1);

	// 랭킹시 유저 데이터 가져오기
	$resourceData = $redis->zrevrange($rankDbName.'_Resource', 0, -1);
	
	// 초기 0 위 입력 	
	$redis->zadd($rankDbName, 2099999999, 'god');

	$dbs = null;

	$GLOBALS['DB_USER'] = "root";
	$GLOBALS['DB_PASS'] = "eoqkrskwk12";
	$GLOBALS['DB_PORT'] = "3306";
	if ($dbs == null) {
		if ($GLOBALS['REAL'] == 5) {
			if ($serverId == 0) { 
				$GLOBALS['DB_HOST'] = "172.27.100.3";
				$GLOBALS['DB_NAME'] = "hrd_db_game_20";
				$dbs[] = new DB();
				$GLOBALS['DB_NAME'] = "hrd_db_game_70";
				$dbs[] = new DB();
				//-----------------------------------
				$GLOBALS['DB_HOST'] = "172.27.100.4";
				$GLOBALS['DB_NAME'] = "hrd_db_game_50";
				$dbs[] = new DB();
				$GLOBALS['DB_NAME'] = "hrd_db_game_80";
				$dbs[] = new DB();
			} else {
				$GLOBALS['DB_HOST'] = "172.27.100.3";
				$GLOBALS['DB_NAME'] = "hrd_db_game_21";
				$dbs[] = new DB();
				$GLOBALS['DB_NAME'] = "hrd_db_game_22";
				$dbs[] = new DB();
				//-----------------------------------
				$GLOBALS['DB_NAME'] = "hrd_db_game_71";
				$dbs[] = new DB();
				$GLOBALS['DB_NAME'] = "hrd_db_game_72";
				$dbs[] = new DB();
				//-----------------------------------
				$GLOBALS['DB_HOST'] = "172.27.100.4";
				$GLOBALS['DB_NAME'] = "hrd_db_game_51";
				$dbs[] = new DB();
				$GLOBALS['DB_NAME'] = "hrd_db_game_52";
				$dbs[] = new DB();
				//-----------------------------------
				$GLOBALS['DB_NAME'] = "hrd_db_game_81";
				$dbs[] = new DB();
				$GLOBALS['DB_NAME'] = "hrd_db_game_82";
				$dbs[] = new DB();
			} 
		} else {
			$GLOBALS['DB_HOST'] = "14.49.38.192";
			$GLOBALS['DB_NAME'] = "hrd_db_test";
			$GLOBALS['DB_PORT'] = "1306";
			$dbs[] = new DB();
		}
	}

	$yearVal = ((int)date('y'))*100;
	$time = time() + 9*3600 - 24*3600*4-5*3600-3600;
	$totalWeeks = (int)($time / 604800);
	$weekVal = (int)600 + ((int)($totalWeeks%5));

	$rewardLength = count($climbRankRewards);
	$pointer=0;

	$length = count($rankData);
	$pieces = explode (",", $climbRankRewards[0]);
	$lastRankk = (int) floor($pieces[0] * $length * 0.0001);

	for ( $i=1; $i < $length; $i++ ) {
		$name = $rankData[$i];

		$res;
		$findDB = null;
		$isOk = false;
		$userId = null;
		$userId = (int)$redis->hget('climbUserId', $name);

		$accDBNo = substr($userId, 0,3);

		if ($GLOBALS['REAL'] == 5) {
			if ($serverId == 0 ) {
				switch($accDBNo) {
					case 200:
						$db = $dbs[0];
						break;
					case 500:
					case 599:
					case 600:
						$db = $dbs[1];
						break;
					case 700:
						$db = $dbs[2];
						break;
					case 800:
						$db = $dbs[3];
						break;
					default :
						// error
						break;
				}	
			} else {
				switch($accDBNo) {
					case 201:
						$db = $dbs[0];
						break;
					case 202:
						$db = $dbs[1];
						break;
					case 501:
						$db = $dbs[2];
						break;
					case 502:
						$db = $dbs[3];
						break;
					case 701:
						$db = $dbs[4];
						break;
					case 702:
						$db = $dbs[5];
						break;
					case 801:
						$db = $dbs[6];
						break;
					case 802:
						$db = $dbs[7];
						break;
					default :
						// error
						break;
				}	
			} 
		} else {
			$db = $dbs[0];
		}

		$error = true;
		for($dbcnt = 0; $dbcnt < count($dbs); $dbcnt++) {		
			$db = $dbs[$dbcnt];
			$row = null;
			$sql = "select * from frdUserData where name = :name";
			$db->prepare($sql);
			$db->bindValue(':name', $name, PDO::PARAM_STR);
			$db->execute();
			$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
			if ($row) {
				$userId = $row['userId'];
				$error = false;
				break;
			}
		}
		if ($error) {
			continue;
		}

		$count = count($pieces);
		$query = "insert into frdUserPost (recvUserId, sendUserId, TYPE, COUNT, expire_time, reg_date ) values ";
		for ( $j=1; $j<$count; $j+=2 ) {
			$rewardType = $pieces[$j];
			$rewardAmount = $pieces[$j+1];

			$query.= sprintf("( '%s', 3, %d, %d,DATE_ADD(NOW(), INTERVAL 3 DAY), now() ), ", $userId, $rewardType, $rewardAmount);
		}
		$query.= sprintf("( '%s', 3, 999, 0,DATE_ADD(NOW(), INTERVAL 3 DAY), now() )", $userId);
		$db -> prepare($query);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			error_log("{$date}| {$sql}| {$userId}" . "\n", 3, "/home/log/climbTopLog_" . date('Ymd') . '.log');
			return false;
		}

		$sql = "select * from frdClimbTopData where userId = :userId";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db->execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if ($row) {
			$lastRank = $i;
			$playCount = $row["playCount"];
			$averageRank = ($row["averageRank"]*$playCount + $lastRank) / ($playCount+1);
			$avrgTotalUserCount = ($row["avrgTotalUserCount"]*$playCount + $length) / ($playCount+1);
			$playCount = (int)($playCount+1);
			if ( $lastRank < $row["bestRank"] ) {
				$bestRank = $lastRank;
				$bestTotalUserCount = $length;
				$sql = "UPDATE frdClimbTopData SET 

					bestRank=:bestRank, 
					bestTotalUserCount=:bestTotalUserCount, 
					playCount=:playCount, 
					averageRank=:averageRank,

					avrgTotalUserCount= :avrgTotalUserCount, 
					lastRank= :lastRank, 
					lastTotalUserCount=:length 

					WHERE userId = :recvUserId";
				$db -> prepare($sql);

				$db->bindValue(':bestRank', $bestRank, PDO::PARAM_INT);
				$db->bindValue(':bestTotalUserCount', $bestTotalUserCount, PDO::PARAM_INT);
				$db->bindValue(':playCount', $playCount, PDO::PARAM_INT);
				$db->bindValue(':averageRank', $averageRank, PDO::PARAM_INT);

				$db->bindValue(':avrgTotalUserCount', $avrgTotalUserCount, PDO::PARAM_INT);
				$db->bindValue(':lastRank', $lastRank, PDO::PARAM_INT);
				$db->bindValue(':length', $length, PDO::PARAM_INT);

				$db->bindValue(':recvUserId', $userId, PDO::PARAM_INT);
				$row = $db -> execute();
				if (!isset($row) || is_null($row) || $row == 0) {
					error_log("{$date}| {$sql}| {$userId}" . "\n", 3, "/home/log/climbTopLog_" . date('Ymd') . '.log');
					return false;
				}
			} else {
				$sql = "UPDATE frdClimbTopData SET 
					playCount=:playCount, 
					averageRank=:averageRank, 
					avrgTotalUserCount=:avrgTotalUserCount, 
					lastRank=:lastRank, 
					lastTotalUserCount=:length 
					WHERE userId = :userId";
				$db -> prepare($sql);
				$db->bindValue(':playCount', $playCount, PDO::PARAM_INT);
				$db->bindValue(':averageRank', $averageRank, PDO::PARAM_INT);
				$db->bindValue(':avrgTotalUserCount', $avrgTotalUserCount, PDO::PARAM_INT);
				$db->bindValue(':lastRank', $lastRank, PDO::PARAM_INT);
				$db->bindValue(':length', $length, PDO::PARAM_INT);
				$db->bindValue(':userId', $userId, PDO::PARAM_INT);
				$row = $db -> execute();
				if (!isset($row) || is_null($row) || $row == 0) {
					error_log("{$date}| {$sql}| {$userId}" . "\n", 3, "/home/log/climbTopLog_" . date('Ymd') . '.log');
					return false;
				}
			}
		} else {
			$playCount = 1;
			$sql = "INSERT INTO frdClimbTopData 
				( userId, bestRank, bestTotalUserCount, playCount, averageRank, avrgTotalUserCount, lastRank, lastTotalUserCount 	)
				VALUES (:userId, $i, $length, $playCount, $i, $length, $i, $length)";
			$db -> prepare($sql);
			$db->bindValue(':userId', $userId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				error_log("{$date}| {$sql}| {$userId}" . "\n", 3, "/home/log/climbTopLog_" . date('Ymd') . '.log');
				return false;
			}

		}
		$year_count = $yearVal + $playCount;
		$query ="insert into frdClimbTopWeeklyData 
				( userId, year_count, climbTopId, rank, totalCount, reg_date )
values ($userId, $year_count, $weekVal, $i, $length, now())";
		$db -> prepare($query);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			error_log("{$date}| {$query}| {$userId}" . "\n", 3, "/home/log/climbTopLog_" . date('Ymd') . '.log');
			return false;
		}

		if ( ($i+1) > $lastRankk ) {
			$pointer++;
			if ( $pointer >=  $rewardLength )
				break;

			$pieces = explode (",", $climbRankRewards[$pointer]);
			$lastRankk = (int) ($pieces[0] * $length * 0.0001);
			fwrite($fp, $climbRankRewards[$pointer]."\n");
		}

	}

	fwrite($fp, "Finish!!\n");
	fclose($fp);

	unset($db);
}

?>
