<?php
require_once "./common/Config.php";
require_once "./lib/Logger.php";
require_once "./classes/WebViewManager.php";
require_once './lib/ParamChecker.php';

class UserWebViewApi {

	public function __construct() {
		$this -> logger = Logger::get();

	}

	public function UserWebView($param) {
		/*
		$ParamChecker = new ParamChecker();
		$base_param[] = 'idToken';
		$base_param[] = 'email';
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			$resultFail['Protocol'] = "ResLogin";
			$resultFail['ResultCode'] = $check_result;
			return $resultFail;
		}
		$resultFail = array('Protocol' => 'ResLogin', 'ResultCode' => 200);
		*/

		$WebViewManager = new WebViewManager();
		$result = $WebViewManager ->getUserWebView($param);

		$result['Protocol'] = "ResUserWebView";
		return $result;
	}

	public function UserReceive($param) {
		/*
		$ParamChecker = new ParamChecker();
		$base_param[] = 'idToken';
		$base_param[] = 'email';
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			$resultFail['Protocol'] = "ResLogin";
			$resultFail['ResultCode'] = $check_result;
			return $resultFail;
		}
		$resultFail = array('Protocol' => 'ResLogin', 'ResultCode' => 200);
		*/

		$WebViewManager = new WebViewManager();
		$result = $WebViewManager ->setUserReceive($param);

		$result['Protocol'] = "ResUserReceive";
		return $result;
	}

}
?>

