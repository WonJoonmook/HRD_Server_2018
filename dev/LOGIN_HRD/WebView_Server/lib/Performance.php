<?php

class Performance
{
	public static $timers = array();
	
	public static $performances = array();
	
	public static function timer_start($key)
	{
		self::$timers[$key] = microtime(TRUE);
	}
	
	public static function timer_stop($key)
	{
		$end = microtime(TRUE);
		
		if (isset(self::$timers[$key]) === FALSE)
		{
			return FALSE;
		}
		
		$start = self::$timers[$key];
		
		if ($start === 0)
		{
			return FALSE;
		}
		
		if (isset(self::$performances[$key]) === FALSE)
		{
			self::$performances[$key] = 0;
		}
		
		self::$performances[$key] += (int)(($end - $start) * 100000) / 100;
		self::$timers[$key] = 0;
		
		return self::$performances;
	}
}


