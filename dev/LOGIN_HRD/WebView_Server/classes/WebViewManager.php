<?php
require_once './lib/Logger.php';
require_once './lib/RandManager.php';
include_once './common/DB_Info.php';
include_once './common/DB.php';

class WebViewManager {
    public function __construct() {
        $this -> logger = Logger::get();
    }

	public function getUserWebView($param) {
    	$resultFail['ResultCode'] = 300;
		$Data = $param['Data'];
		$userId = $Data['userId'];

		if ($userId == "" || $userId == null || $userId == 0) {
			return $resultFail;
		}

		$nowDay = date('ymdH', time());
		// GAME DB 에 접속 하여 현재 유저의 지급 상황을 확인 한다
		// 보유 아이템 개tn 와 현재 지급 완료된 내역	
        $db = new DB();
        $sql = <<<SQL
			SELECT cnt, today,
			DATEDIFF(now(), update_date) as diff
			FROM frdWebViewEvent 
			WHERE userId = :userId 
SQL;
        $db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
        $db -> execute();
		$result = null;
		$Data = null;
		$Data['cnt'] = 0;
		$Data['today'] = 0;
        $db -> execute();
        $row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if ($row) {
			$Data['cnt'] = $row['cnt'];
			$Data['today'] = $row['today'];
			/*
			if ($row['diff'] > 0 ) {
				if ( $nowDay >  17100300 &&  $nowDay < 17100600) {
					$cnt = $row['cnt']+3;	
				}  else {
					$cnt = $row['cnt']+1;	
				}
				$sql = "UPDATE frdWebViewEvent SET cnt = :cnt, today = 0, update_date = now()
					WHERE userId = :userId";
				$db -> prepare($sql);
				$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
				$db -> bindValue(':cnt', $cnt, PDO::PARAM_INT);
				$row = $db -> execute();
				if (!isset($row) || is_null($row) || $row == 0) {
					$result = null;
					$result['ResultCode'] = 101;
					$this -> logger -> logError(__FUNCTION__.' FAIL  userId.' . $userId. ' sql: '.$sql);
					return $result;
				}
	
				$Data['cnt'] = $cnt;
				$Data['today'] = 0;
			}
			*/
		} else {
			/*
			// 시간제 무조건 확률증가, 획득 증가 로직
			if ( $nowDay >  17100300 &&  $nowDay < 17100600) {
				$cnt = 3;	
			}  else {
				$cnt = 1;	
			}

			$sql = " INSERT INTO frdWebViewEvent (userId, cnt , today, update_date) VALUE
				(:userId, :cnt, 0, now()) ";
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$db -> bindValue(':cnt', $cnt, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$result = null;
				$result['ResultCode'] = 101;
				$this -> logger -> logError(__FUNCTION__.'   FAIL  userId.' . $userId. ' sql: '.$sql);
				return $result;
			}
			$Data['cnt'] = $cnt;
			$Data['today'] = $today;
			*/
			$Data['cnt'] = 0;
			$Data['today'] = 0;

		}

		$Data['userId'] = $userId;
		$result['Data'] = $Data;
		return $result;
	}

    public function setUserReceive($param) {
    	$resultFail['ResultCode'] = 300;
		$Data = $param['Data'];

		$userId = $Data['userId'];

		// 게임 디비 에서 유저의 보유 상황을 확인하여검증후 지급 절차를 진행 한다.
        $db = new DB();
        $sql = <<<SQL
			SELECT cnt FROM frdWebViewEvent Where userId = :userId
SQL;
        $db -> prepare($sql);
        $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
        $db -> execute();
        $row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if ($row) {
			$myCount = $row['cnt'];
		} else {
			$result = null;
			$result['ResultCode'] = 101;
			$this -> logger -> logError(__FUNCTION__.' :  FAIL  userId.' . $userId.' sql: '.$sql);
			return $result;
		}

		if ($myCount < 1) {
			$result['ResultCode'] = 303;
			return $result;			
		}
		
		$Ids = array(5003,5003,5003,100003,100003,100003,100000,5006,5006,5006,5000,5000,5000,98,283,282,237,236,235,100004,5004,5004,5004,5002,5002,5002,10504,10505,1505,115005,115005,115005,115005,110000,110000,1495,1495,1495,106010,106010,106010,105010,105010,105010);
		$Cnts = array(10,20,30,1,2,3,1,50,100,200,1000,3000,5000,10,10,10,10,10,10,1,5,10,15,1,2,3,1,1,1,1,1,3,5,1,3,1,2,3,1,2,3,1,3,5);
		$Rates = array(1100,400,100,310,100,15,10,802,300,100,1110,450,150,15,5,20,20,20,15,3,680,300,100,950,300,100,90,10,80,35,300,80,25,20,5,700,400,100,150,80,30,300,150,50);

		$RandManager = new RandManager();

		$randResult = $RandManager->Rate_N_Idx($Ids, $Rates);
		$itemId = $randResult['val'];
		$pos = $randResult['pos'];
		$itemCnt = $Cnts[$pos];

		if ($itemId == 115005 || $itemId == 106010 || $itemId == 105010) {
			if ($itemId == 115005) {
				$itemId = $itemId -5 + rand(0,4);
			} else {
				$itemId = $itemId-10 + rand(0,9);
			}
		}

		$sql = "INSERT INTO frdUserPost
			(recvUserId, sendUserId, type, count, expire_time, reg_date) VALUE ";
		$sql .= "(:userId, 0, :itemId, :itemCnt, DATE_ADD(now(), INTERVAL 3 DAY ), now()) ";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> bindValue(':itemId', $itemId, PDO::PARAM_INT);
		$db -> bindValue(':itemCnt', $itemCnt, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this -> logger -> logError(__FUNCTION__.' :  FAIL  userId.' . $userId.' sql: '.$sql);
			$result['ResultCode'] = 300;
			return $result;
		}

		$sql = <<<SQL
		UPDATE frdWebViewEvent SET cnt = cnt - 1 WHERE userId = :userId
SQL;
        $db -> prepare($sql);
        $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
        $row = $db -> execute();
        if (!isset($row) || is_null($row) || $row == 0) {
			$this -> logger -> logError(__FUNCTION__.' :  FAIL  userId.' . $userId.' sql: '.$sql);
            $result['ResultCode'] = 300;
            return $result;
        }

		$result = null;
    	$result['ResultCode'] = 100;

        return $result;
    }

}
?>

