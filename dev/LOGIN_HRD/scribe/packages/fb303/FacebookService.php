<?php
/**
 * Autogenerated by Thrift Compiler (0.8.0)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
include_once $GLOBALS['THRIFT_ROOT'].'/Thrift.php';

include_once $GLOBALS['THRIFT_ROOT'].'/packages/fb303/fb303_types.php';

interface FacebookServiceIf {
  public function getName();
  public function getVersion();
  public function getStatus();
  public function getStatusDetails();
  public function getCounters();
  public function getCounter($key);
  public function setOption($key, $value);
  public function getOption($key);
  public function getOptions();
  public function getCpuProfile($profileDurationInSec);
  public function aliveSince();
  public function reinitialize();
  public function shutdown();
}

class FacebookServiceClient implements FacebookServiceIf {
  protected $input_ = null;
  protected $output_ = null;

  protected $seqid_ = 0;

  public function __construct($input, $output=null) {
    $this->input_ = $input;
    $this->output_ = $output ? $output : $input;
  }

  public function getName()
  {
    $this->send_getName();
    return $this->recv_getName();
  }

  public function send_getName()
  {
    $args = new FacebookService_getName_args();
    $bin_accel = ($this->output_ instanceof TProtocol::$TBINARYPROTOCOLACCELERATED) && function_exists('thrift_protocol_write_binary');
    if ($bin_accel)
    {
      thrift_protocol_write_binary($this->output_, 'getName', TMessageType::CALL, $args, $this->seqid_, $this->output_->isStrictWrite());
    }
    else
    {
      $this->output_->writeMessageBegin('getName', TMessageType::CALL, $this->seqid_);
      $args->write($this->output_);
      $this->output_->writeMessageEnd();
      $this->output_->getTransport()->flush();
    }
  }

  public function recv_getName()
  {
    $bin_accel = ($this->input_ instanceof TProtocol::$TBINARYPROTOCOLACCELERATED) && function_exists('thrift_protocol_read_binary');
    if ($bin_accel) $result = thrift_protocol_read_binary($this->input_, 'FacebookService_getName_result', $this->input_->isStrictRead());
    else
    {
      $rseqid = 0;
      $fname = null;
      $mtype = 0;

      $this->input_->readMessageBegin($fname, $mtype, $rseqid);
      if ($mtype == TMessageType::EXCEPTION) {
        $x = new TApplicationException();
        $x->read($this->input_);
        $this->input_->readMessageEnd();
        throw $x;
      }
      $result = new FacebookService_getName_result();
      $result->read($this->input_);
      $this->input_->readMessageEnd();
    }
    if ($result->success !== null) {
      return $result->success;
    }
    throw new Exception("getName failed: unknown result");
  }

  public function getVersion()
  {
    $this->send_getVersion();
    return $this->recv_getVersion();
  }

  public function send_getVersion()
  {
    $args = new FacebookService_getVersion_args();
    $bin_accel = ($this->output_ instanceof TProtocol::$TBINARYPROTOCOLACCELERATED) && function_exists('thrift_protocol_write_binary');
    if ($bin_accel)
    {
      thrift_protocol_write_binary($this->output_, 'getVersion', TMessageType::CALL, $args, $this->seqid_, $this->output_->isStrictWrite());
    }
    else
    {
      $this->output_->writeMessageBegin('getVersion', TMessageType::CALL, $this->seqid_);
      $args->write($this->output_);
      $this->output_->writeMessageEnd();
      $this->output_->getTransport()->flush();
    }
  }

  public function recv_getVersion()
  {
    $bin_accel = ($this->input_ instanceof TProtocol::$TBINARYPROTOCOLACCELERATED) && function_exists('thrift_protocol_read_binary');
    if ($bin_accel) $result = thrift_protocol_read_binary($this->input_, 'FacebookService_getVersion_result', $this->input_->isStrictRead());
    else
    {
      $rseqid = 0;
      $fname = null;
      $mtype = 0;

      $this->input_->readMessageBegin($fname, $mtype, $rseqid);
      if ($mtype == TMessageType::EXCEPTION) {
        $x = new TApplicationException();
        $x->read($this->input_);
        $this->input_->readMessageEnd();
        throw $x;
      }
      $result = new FacebookService_getVersion_result();
      $result->read($this->input_);
      $this->input_->readMessageEnd();
    }
    if ($result->success !== null) {
      return $result->success;
    }
    throw new Exception("getVersion failed: unknown result");
  }

  public function getStatus()
  {
    $this->send_getStatus();
    return $this->recv_getStatus();
  }

  public function send_getStatus()
  {
    $args = new FacebookService_getStatus_args();
    $bin_accel = ($this->output_ instanceof TProtocol::$TBINARYPROTOCOLACCELERATED) && function_exists('thrift_protocol_write_binary');
    if ($bin_accel)
    {
      thrift_protocol_write_binary($this->output_, 'getStatus', TMessageType::CALL, $args, $this->seqid_, $this->output_->isStrictWrite());
    }
    else
    {
      $this->output_->writeMessageBegin('getStatus', TMessageType::CALL, $this->seqid_);
      $args->write($this->output_);
      $this->output_->writeMessageEnd();
      $this->output_->getTransport()->flush();
    }
  }

  public function recv_getStatus()
  {
    $bin_accel = ($this->input_ instanceof TProtocol::$TBINARYPROTOCOLACCELERATED) && function_exists('thrift_protocol_read_binary');
    if ($bin_accel) $result = thrift_protocol_read_binary($this->input_, 'FacebookService_getStatus_result', $this->input_->isStrictRead());
    else
    {
      $rseqid = 0;
      $fname = null;
      $mtype = 0;

      $this->input_->readMessageBegin($fname, $mtype, $rseqid);
      if ($mtype == TMessageType::EXCEPTION) {
        $x = new TApplicationException();
        $x->read($this->input_);
        $this->input_->readMessageEnd();
        throw $x;
      }
      $result = new FacebookService_getStatus_result();
      $result->read($this->input_);
      $this->input_->readMessageEnd();
    }
    if ($result->success !== null) {
      return $result->success;
    }
    throw new Exception("getStatus failed: unknown result");
  }

  public function getStatusDetails()
  {
    $this->send_getStatusDetails();
    return $this->recv_getStatusDetails();
  }

  public function send_getStatusDetails()
  {
    $args = new FacebookService_getStatusDetails_args();
    $bin_accel = ($this->output_ instanceof TProtocol::$TBINARYPROTOCOLACCELERATED) && function_exists('thrift_protocol_write_binary');
    if ($bin_accel)
    {
      thrift_protocol_write_binary($this->output_, 'getStatusDetails', TMessageType::CALL, $args, $this->seqid_, $this->output_->isStrictWrite());
    }
    else
    {
      $this->output_->writeMessageBegin('getStatusDetails', TMessageType::CALL, $this->seqid_);
      $args->write($this->output_);
      $this->output_->writeMessageEnd();
      $this->output_->getTransport()->flush();
    }
  }

  public function recv_getStatusDetails()
  {
    $bin_accel = ($this->input_ instanceof TProtocol::$TBINARYPROTOCOLACCELERATED) && function_exists('thrift_protocol_read_binary');
    if ($bin_accel) $result = thrift_protocol_read_binary($this->input_, 'FacebookService_getStatusDetails_result', $this->input_->isStrictRead());
    else
    {
      $rseqid = 0;
      $fname = null;
      $mtype = 0;

      $this->input_->readMessageBegin($fname, $mtype, $rseqid);
      if ($mtype == TMessageType::EXCEPTION) {
        $x = new TApplicationException();
        $x->read($this->input_);
        $this->input_->readMessageEnd();
        throw $x;
      }
      $result = new FacebookService_getStatusDetails_result();
      $result->read($this->input_);
      $this->input_->readMessageEnd();
    }
    if ($result->success !== null) {
      return $result->success;
    }
    throw new Exception("getStatusDetails failed: unknown result");
  }

  public function getCounters()
  {
    $this->send_getCounters();
    return $this->recv_getCounters();
  }

  public function send_getCounters()
  {
    $args = new FacebookService_getCounters_args();
    $bin_accel = ($this->output_ instanceof TProtocol::$TBINARYPROTOCOLACCELERATED) && function_exists('thrift_protocol_write_binary');
    if ($bin_accel)
    {
      thrift_protocol_write_binary($this->output_, 'getCounters', TMessageType::CALL, $args, $this->seqid_, $this->output_->isStrictWrite());
    }
    else
    {
      $this->output_->writeMessageBegin('getCounters', TMessageType::CALL, $this->seqid_);
      $args->write($this->output_);
      $this->output_->writeMessageEnd();
      $this->output_->getTransport()->flush();
    }
  }

  public function recv_getCounters()
  {
    $bin_accel = ($this->input_ instanceof TProtocol::$TBINARYPROTOCOLACCELERATED) && function_exists('thrift_protocol_read_binary');
    if ($bin_accel) $result = thrift_protocol_read_binary($this->input_, 'FacebookService_getCounters_result', $this->input_->isStrictRead());
    else
    {
      $rseqid = 0;
      $fname = null;
      $mtype = 0;

      $this->input_->readMessageBegin($fname, $mtype, $rseqid);
      if ($mtype == TMessageType::EXCEPTION) {
        $x = new TApplicationException();
        $x->read($this->input_);
        $this->input_->readMessageEnd();
        throw $x;
      }
      $result = new FacebookService_getCounters_result();
      $result->read($this->input_);
      $this->input_->readMessageEnd();
    }
    if ($result->success !== null) {
      return $result->success;
    }
    throw new Exception("getCounters failed: unknown result");
  }

  public function getCounter($key)
  {
    $this->send_getCounter($key);
    return $this->recv_getCounter();
  }

  public function send_getCounter($key)
  {
    $args = new FacebookService_getCounter_args();
    $args->key = $key;
    $bin_accel = ($this->output_ instanceof TProtocol::$TBINARYPROTOCOLACCELERATED) && function_exists('thrift_protocol_write_binary');
    if ($bin_accel)
    {
      thrift_protocol_write_binary($this->output_, 'getCounter', TMessageType::CALL, $args, $this->seqid_, $this->output_->isStrictWrite());
    }
    else
    {
      $this->output_->writeMessageBegin('getCounter', TMessageType::CALL, $this->seqid_);
      $args->write($this->output_);
      $this->output_->writeMessageEnd();
      $this->output_->getTransport()->flush();
    }
  }

  public function recv_getCounter()
  {
    $bin_accel = ($this->input_ instanceof TProtocol::$TBINARYPROTOCOLACCELERATED) && function_exists('thrift_protocol_read_binary');
    if ($bin_accel) $result = thrift_protocol_read_binary($this->input_, 'FacebookService_getCounter_result', $this->input_->isStrictRead());
    else
    {
      $rseqid = 0;
      $fname = null;
      $mtype = 0;

      $this->input_->readMessageBegin($fname, $mtype, $rseqid);
      if ($mtype == TMessageType::EXCEPTION) {
        $x = new TApplicationException();
        $x->read($this->input_);
        $this->input_->readMessageEnd();
        throw $x;
      }
      $result = new FacebookService_getCounter_result();
      $result->read($this->input_);
      $this->input_->readMessageEnd();
    }
    if ($result->success !== null) {
      return $result->success;
    }
    throw new Exception("getCounter failed: unknown result");
  }

  public function setOption($key, $value)
  {
    $this->send_setOption($key, $value);
    $this->recv_setOption();
  }

  public function send_setOption($key, $value)
  {
    $args = new FacebookService_setOption_args();
    $args->key = $key;
    $args->value = $value;
    $bin_accel = ($this->output_ instanceof TProtocol::$TBINARYPROTOCOLACCELERATED) && function_exists('thrift_protocol_write_binary');
    if ($bin_accel)
    {
      thrift_protocol_write_binary($this->output_, 'setOption', TMessageType::CALL, $args, $this->seqid_, $this->output_->isStrictWrite());
    }
    else
    {
      $this->output_->writeMessageBegin('setOption', TMessageType::CALL, $this->seqid_);
      $args->write($this->output_);
      $this->output_->writeMessageEnd();
      $this->output_->getTransport()->flush();
    }
  }

  public function recv_setOption()
  {
    $bin_accel = ($this->input_ instanceof TProtocol::$TBINARYPROTOCOLACCELERATED) && function_exists('thrift_protocol_read_binary');
    if ($bin_accel) $result = thrift_protocol_read_binary($this->input_, 'FacebookService_setOption_result', $this->input_->isStrictRead());
    else
    {
      $rseqid = 0;
      $fname = null;
      $mtype = 0;

      $this->input_->readMessageBegin($fname, $mtype, $rseqid);
      if ($mtype == TMessageType::EXCEPTION) {
        $x = new TApplicationException();
        $x->read($this->input_);
        $this->input_->readMessageEnd();
        throw $x;
      }
      $result = new FacebookService_setOption_result();
      $result->read($this->input_);
      $this->input_->readMessageEnd();
    }
    return;
  }

  public function getOption($key)
  {
    $this->send_getOption($key);
    return $this->recv_getOption();
  }

  public function send_getOption($key)
  {
    $args = new FacebookService_getOption_args();
    $args->key = $key;
    $bin_accel = ($this->output_ instanceof TProtocol::$TBINARYPROTOCOLACCELERATED) && function_exists('thrift_protocol_write_binary');
    if ($bin_accel)
    {
      thrift_protocol_write_binary($this->output_, 'getOption', TMessageType::CALL, $args, $this->seqid_, $this->output_->isStrictWrite());
    }
    else
    {
      $this->output_->writeMessageBegin('getOption', TMessageType::CALL, $this->seqid_);
      $args->write($this->output_);
      $this->output_->writeMessageEnd();
      $this->output_->getTransport()->flush();
    }
  }

  public function recv_getOption()
  {
    $bin_accel = ($this->input_ instanceof TProtocol::$TBINARYPROTOCOLACCELERATED) && function_exists('thrift_protocol_read_binary');
    if ($bin_accel) $result = thrift_protocol_read_binary($this->input_, 'FacebookService_getOption_result', $this->input_->isStrictRead());
    else
    {
      $rseqid = 0;
      $fname = null;
      $mtype = 0;

      $this->input_->readMessageBegin($fname, $mtype, $rseqid);
      if ($mtype == TMessageType::EXCEPTION) {
        $x = new TApplicationException();
        $x->read($this->input_);
        $this->input_->readMessageEnd();
        throw $x;
      }
      $result = new FacebookService_getOption_result();
      $result->read($this->input_);
      $this->input_->readMessageEnd();
    }
    if ($result->success !== null) {
      return $result->success;
    }
    throw new Exception("getOption failed: unknown result");
  }

  public function getOptions()
  {
    $this->send_getOptions();
    return $this->recv_getOptions();
  }

  public function send_getOptions()
  {
    $args = new FacebookService_getOptions_args();
    $bin_accel = ($this->output_ instanceof TProtocol::$TBINARYPROTOCOLACCELERATED) && function_exists('thrift_protocol_write_binary');
    if ($bin_accel)
    {
      thrift_protocol_write_binary($this->output_, 'getOptions', TMessageType::CALL, $args, $this->seqid_, $this->output_->isStrictWrite());
    }
    else
    {
      $this->output_->writeMessageBegin('getOptions', TMessageType::CALL, $this->seqid_);
      $args->write($this->output_);
      $this->output_->writeMessageEnd();
      $this->output_->getTransport()->flush();
    }
  }

  public function recv_getOptions()
  {
    $bin_accel = ($this->input_ instanceof TProtocol::$TBINARYPROTOCOLACCELERATED) && function_exists('thrift_protocol_read_binary');
    if ($bin_accel) $result = thrift_protocol_read_binary($this->input_, 'FacebookService_getOptions_result', $this->input_->isStrictRead());
    else
    {
      $rseqid = 0;
      $fname = null;
      $mtype = 0;

      $this->input_->readMessageBegin($fname, $mtype, $rseqid);
      if ($mtype == TMessageType::EXCEPTION) {
        $x = new TApplicationException();
        $x->read($this->input_);
        $this->input_->readMessageEnd();
        throw $x;
      }
      $result = new FacebookService_getOptions_result();
      $result->read($this->input_);
      $this->input_->readMessageEnd();
    }
    if ($result->success !== null) {
      return $result->success;
    }
    throw new Exception("getOptions failed: unknown result");
  }

  public function getCpuProfile($profileDurationInSec)
  {
    $this->send_getCpuProfile($profileDurationInSec);
    return $this->recv_getCpuProfile();
  }

  public function send_getCpuProfile($profileDurationInSec)
  {
    $args = new FacebookService_getCpuProfile_args();
    $args->profileDurationInSec = $profileDurationInSec;
    $bin_accel = ($this->output_ instanceof TProtocol::$TBINARYPROTOCOLACCELERATED) && function_exists('thrift_protocol_write_binary');
    if ($bin_accel)
    {
      thrift_protocol_write_binary($this->output_, 'getCpuProfile', TMessageType::CALL, $args, $this->seqid_, $this->output_->isStrictWrite());
    }
    else
    {
      $this->output_->writeMessageBegin('getCpuProfile', TMessageType::CALL, $this->seqid_);
      $args->write($this->output_);
      $this->output_->writeMessageEnd();
      $this->output_->getTransport()->flush();
    }
  }

  public function recv_getCpuProfile()
  {
    $bin_accel = ($this->input_ instanceof TProtocol::$TBINARYPROTOCOLACCELERATED) && function_exists('thrift_protocol_read_binary');
    if ($bin_accel) $result = thrift_protocol_read_binary($this->input_, 'FacebookService_getCpuProfile_result', $this->input_->isStrictRead());
    else
    {
      $rseqid = 0;
      $fname = null;
      $mtype = 0;

      $this->input_->readMessageBegin($fname, $mtype, $rseqid);
      if ($mtype == TMessageType::EXCEPTION) {
        $x = new TApplicationException();
        $x->read($this->input_);
        $this->input_->readMessageEnd();
        throw $x;
      }
      $result = new FacebookService_getCpuProfile_result();
      $result->read($this->input_);
      $this->input_->readMessageEnd();
    }
    if ($result->success !== null) {
      return $result->success;
    }
    throw new Exception("getCpuProfile failed: unknown result");
  }

  public function aliveSince()
  {
    $this->send_aliveSince();
    return $this->recv_aliveSince();
  }

  public function send_aliveSince()
  {
    $args = new FacebookService_aliveSince_args();
    $bin_accel = ($this->output_ instanceof TProtocol::$TBINARYPROTOCOLACCELERATED) && function_exists('thrift_protocol_write_binary');
    if ($bin_accel)
    {
      thrift_protocol_write_binary($this->output_, 'aliveSince', TMessageType::CALL, $args, $this->seqid_, $this->output_->isStrictWrite());
    }
    else
    {
      $this->output_->writeMessageBegin('aliveSince', TMessageType::CALL, $this->seqid_);
      $args->write($this->output_);
      $this->output_->writeMessageEnd();
      $this->output_->getTransport()->flush();
    }
  }

  public function recv_aliveSince()
  {
    $bin_accel = ($this->input_ instanceof TProtocol::$TBINARYPROTOCOLACCELERATED) && function_exists('thrift_protocol_read_binary');
    if ($bin_accel) $result = thrift_protocol_read_binary($this->input_, 'FacebookService_aliveSince_result', $this->input_->isStrictRead());
    else
    {
      $rseqid = 0;
      $fname = null;
      $mtype = 0;

      $this->input_->readMessageBegin($fname, $mtype, $rseqid);
      if ($mtype == TMessageType::EXCEPTION) {
        $x = new TApplicationException();
        $x->read($this->input_);
        $this->input_->readMessageEnd();
        throw $x;
      }
      $result = new FacebookService_aliveSince_result();
      $result->read($this->input_);
      $this->input_->readMessageEnd();
    }
    if ($result->success !== null) {
      return $result->success;
    }
    throw new Exception("aliveSince failed: unknown result");
  }

  public function reinitialize()
  {
    $this->send_reinitialize();
  }

  public function send_reinitialize()
  {
    $args = new FacebookService_reinitialize_args();
    $bin_accel = ($this->output_ instanceof TProtocol::$TBINARYPROTOCOLACCELERATED) && function_exists('thrift_protocol_write_binary');
    if ($bin_accel)
    {
      thrift_protocol_write_binary($this->output_, 'reinitialize', TMessageType::CALL, $args, $this->seqid_, $this->output_->isStrictWrite());
    }
    else
    {
      $this->output_->writeMessageBegin('reinitialize', TMessageType::CALL, $this->seqid_);
      $args->write($this->output_);
      $this->output_->writeMessageEnd();
      $this->output_->getTransport()->flush();
    }
  }
  public function shutdown()
  {
    $this->send_shutdown();
  }

  public function send_shutdown()
  {
    $args = new FacebookService_shutdown_args();
    $bin_accel = ($this->output_ instanceof TProtocol::$TBINARYPROTOCOLACCELERATED) && function_exists('thrift_protocol_write_binary');
    if ($bin_accel)
    {
      thrift_protocol_write_binary($this->output_, 'shutdown', TMessageType::CALL, $args, $this->seqid_, $this->output_->isStrictWrite());
    }
    else
    {
      $this->output_->writeMessageBegin('shutdown', TMessageType::CALL, $this->seqid_);
      $args->write($this->output_);
      $this->output_->writeMessageEnd();
      $this->output_->getTransport()->flush();
    }
  }
}

// HELPER FUNCTIONS AND STRUCTURES

class FacebookService_getName_args extends TBase {
  static $_TSPEC;


  public function __construct() {
    if (!isset(self::$_TSPEC)) {
      self::$_TSPEC = array(
        );
    }
  }

  public function getName() {
    return 'FacebookService_getName_args';
  }

  public function read($input)
  {
    return $this->_read('FacebookService_getName_args', self::$_TSPEC, $input);
  }
  public function write($output) {
    return $this->_write('FacebookService_getName_args', self::$_TSPEC, $output);
  }
}

class FacebookService_getName_result extends TBase {
  static $_TSPEC;

  public $success = null;

  public function __construct($vals=null) {
    if (!isset(self::$_TSPEC)) {
      self::$_TSPEC = array(
        0 => array(
          'var' => 'success',
          'type' => TType::STRING,
          ),
        );
    }
    if (is_array($vals)) {
      parent::__construct(self::$_TSPEC, $vals);
    }
  }

  public function getName() {
    return 'FacebookService_getName_result';
  }

  public function read($input)
  {
    return $this->_read('FacebookService_getName_result', self::$_TSPEC, $input);
  }
  public function write($output) {
    return $this->_write('FacebookService_getName_result', self::$_TSPEC, $output);
  }
}

class FacebookService_getVersion_args extends TBase {
  static $_TSPEC;


  public function __construct() {
    if (!isset(self::$_TSPEC)) {
      self::$_TSPEC = array(
        );
    }
  }

  public function getName() {
    return 'FacebookService_getVersion_args';
  }

  public function read($input)
  {
    return $this->_read('FacebookService_getVersion_args', self::$_TSPEC, $input);
  }
  public function write($output) {
    return $this->_write('FacebookService_getVersion_args', self::$_TSPEC, $output);
  }
}

class FacebookService_getVersion_result extends TBase {
  static $_TSPEC;

  public $success = null;

  public function __construct($vals=null) {
    if (!isset(self::$_TSPEC)) {
      self::$_TSPEC = array(
        0 => array(
          'var' => 'success',
          'type' => TType::STRING,
          ),
        );
    }
    if (is_array($vals)) {
      parent::__construct(self::$_TSPEC, $vals);
    }
  }

  public function getName() {
    return 'FacebookService_getVersion_result';
  }

  public function read($input)
  {
    return $this->_read('FacebookService_getVersion_result', self::$_TSPEC, $input);
  }
  public function write($output) {
    return $this->_write('FacebookService_getVersion_result', self::$_TSPEC, $output);
  }
}

class FacebookService_getStatus_args extends TBase {
  static $_TSPEC;


  public function __construct() {
    if (!isset(self::$_TSPEC)) {
      self::$_TSPEC = array(
        );
    }
  }

  public function getName() {
    return 'FacebookService_getStatus_args';
  }

  public function read($input)
  {
    return $this->_read('FacebookService_getStatus_args', self::$_TSPEC, $input);
  }
  public function write($output) {
    return $this->_write('FacebookService_getStatus_args', self::$_TSPEC, $output);
  }
}

class FacebookService_getStatus_result extends TBase {
  static $_TSPEC;

  public $success = null;

  public function __construct($vals=null) {
    if (!isset(self::$_TSPEC)) {
      self::$_TSPEC = array(
        0 => array(
          'var' => 'success',
          'type' => TType::I32,
          ),
        );
    }
    if (is_array($vals)) {
      parent::__construct(self::$_TSPEC, $vals);
    }
  }

  public function getName() {
    return 'FacebookService_getStatus_result';
  }

  public function read($input)
  {
    return $this->_read('FacebookService_getStatus_result', self::$_TSPEC, $input);
  }
  public function write($output) {
    return $this->_write('FacebookService_getStatus_result', self::$_TSPEC, $output);
  }
}

class FacebookService_getStatusDetails_args extends TBase {
  static $_TSPEC;


  public function __construct() {
    if (!isset(self::$_TSPEC)) {
      self::$_TSPEC = array(
        );
    }
  }

  public function getName() {
    return 'FacebookService_getStatusDetails_args';
  }

  public function read($input)
  {
    return $this->_read('FacebookService_getStatusDetails_args', self::$_TSPEC, $input);
  }
  public function write($output) {
    return $this->_write('FacebookService_getStatusDetails_args', self::$_TSPEC, $output);
  }
}

class FacebookService_getStatusDetails_result extends TBase {
  static $_TSPEC;

  public $success = null;

  public function __construct($vals=null) {
    if (!isset(self::$_TSPEC)) {
      self::$_TSPEC = array(
        0 => array(
          'var' => 'success',
          'type' => TType::STRING,
          ),
        );
    }
    if (is_array($vals)) {
      parent::__construct(self::$_TSPEC, $vals);
    }
  }

  public function getName() {
    return 'FacebookService_getStatusDetails_result';
  }

  public function read($input)
  {
    return $this->_read('FacebookService_getStatusDetails_result', self::$_TSPEC, $input);
  }
  public function write($output) {
    return $this->_write('FacebookService_getStatusDetails_result', self::$_TSPEC, $output);
  }
}

class FacebookService_getCounters_args extends TBase {
  static $_TSPEC;


  public function __construct() {
    if (!isset(self::$_TSPEC)) {
      self::$_TSPEC = array(
        );
    }
  }

  public function getName() {
    return 'FacebookService_getCounters_args';
  }

  public function read($input)
  {
    return $this->_read('FacebookService_getCounters_args', self::$_TSPEC, $input);
  }
  public function write($output) {
    return $this->_write('FacebookService_getCounters_args', self::$_TSPEC, $output);
  }
}

class FacebookService_getCounters_result extends TBase {
  static $_TSPEC;

  public $success = null;

  public function __construct($vals=null) {
    if (!isset(self::$_TSPEC)) {
      self::$_TSPEC = array(
        0 => array(
          'var' => 'success',
          'type' => TType::MAP,
          'ktype' => TType::STRING,
          'vtype' => TType::I64,
          'key' => array(
            'type' => TType::STRING,
          ),
          'val' => array(
            'type' => TType::I64,
            ),
          ),
        );
    }
    if (is_array($vals)) {
      parent::__construct(self::$_TSPEC, $vals);
    }
  }

  public function getName() {
    return 'FacebookService_getCounters_result';
  }

  public function read($input)
  {
    return $this->_read('FacebookService_getCounters_result', self::$_TSPEC, $input);
  }
  public function write($output) {
    return $this->_write('FacebookService_getCounters_result', self::$_TSPEC, $output);
  }
}

class FacebookService_getCounter_args extends TBase {
  static $_TSPEC;

  public $key = null;

  public function __construct($vals=null) {
    if (!isset(self::$_TSPEC)) {
      self::$_TSPEC = array(
        1 => array(
          'var' => 'key',
          'type' => TType::STRING,
          ),
        );
    }
    if (is_array($vals)) {
      parent::__construct(self::$_TSPEC, $vals);
    }
  }

  public function getName() {
    return 'FacebookService_getCounter_args';
  }

  public function read($input)
  {
    return $this->_read('FacebookService_getCounter_args', self::$_TSPEC, $input);
  }
  public function write($output) {
    return $this->_write('FacebookService_getCounter_args', self::$_TSPEC, $output);
  }
}

class FacebookService_getCounter_result extends TBase {
  static $_TSPEC;

  public $success = null;

  public function __construct($vals=null) {
    if (!isset(self::$_TSPEC)) {
      self::$_TSPEC = array(
        0 => array(
          'var' => 'success',
          'type' => TType::I64,
          ),
        );
    }
    if (is_array($vals)) {
      parent::__construct(self::$_TSPEC, $vals);
    }
  }

  public function getName() {
    return 'FacebookService_getCounter_result';
  }

  public function read($input)
  {
    return $this->_read('FacebookService_getCounter_result', self::$_TSPEC, $input);
  }
  public function write($output) {
    return $this->_write('FacebookService_getCounter_result', self::$_TSPEC, $output);
  }
}

class FacebookService_setOption_args extends TBase {
  static $_TSPEC;

  public $key = null;
  public $value = null;

  public function __construct($vals=null) {
    if (!isset(self::$_TSPEC)) {
      self::$_TSPEC = array(
        1 => array(
          'var' => 'key',
          'type' => TType::STRING,
          ),
        2 => array(
          'var' => 'value',
          'type' => TType::STRING,
          ),
        );
    }
    if (is_array($vals)) {
      parent::__construct(self::$_TSPEC, $vals);
    }
  }

  public function getName() {
    return 'FacebookService_setOption_args';
  }

  public function read($input)
  {
    return $this->_read('FacebookService_setOption_args', self::$_TSPEC, $input);
  }
  public function write($output) {
    return $this->_write('FacebookService_setOption_args', self::$_TSPEC, $output);
  }
}

class FacebookService_setOption_result extends TBase {
  static $_TSPEC;


  public function __construct() {
    if (!isset(self::$_TSPEC)) {
      self::$_TSPEC = array(
        );
    }
  }

  public function getName() {
    return 'FacebookService_setOption_result';
  }

  public function read($input)
  {
    return $this->_read('FacebookService_setOption_result', self::$_TSPEC, $input);
  }
  public function write($output) {
    return $this->_write('FacebookService_setOption_result', self::$_TSPEC, $output);
  }
}

class FacebookService_getOption_args extends TBase {
  static $_TSPEC;

  public $key = null;

  public function __construct($vals=null) {
    if (!isset(self::$_TSPEC)) {
      self::$_TSPEC = array(
        1 => array(
          'var' => 'key',
          'type' => TType::STRING,
          ),
        );
    }
    if (is_array($vals)) {
      parent::__construct(self::$_TSPEC, $vals);
    }
  }

  public function getName() {
    return 'FacebookService_getOption_args';
  }

  public function read($input)
  {
    return $this->_read('FacebookService_getOption_args', self::$_TSPEC, $input);
  }
  public function write($output) {
    return $this->_write('FacebookService_getOption_args', self::$_TSPEC, $output);
  }
}

class FacebookService_getOption_result extends TBase {
  static $_TSPEC;

  public $success = null;

  public function __construct($vals=null) {
    if (!isset(self::$_TSPEC)) {
      self::$_TSPEC = array(
        0 => array(
          'var' => 'success',
          'type' => TType::STRING,
          ),
        );
    }
    if (is_array($vals)) {
      parent::__construct(self::$_TSPEC, $vals);
    }
  }

  public function getName() {
    return 'FacebookService_getOption_result';
  }

  public function read($input)
  {
    return $this->_read('FacebookService_getOption_result', self::$_TSPEC, $input);
  }
  public function write($output) {
    return $this->_write('FacebookService_getOption_result', self::$_TSPEC, $output);
  }
}

class FacebookService_getOptions_args extends TBase {
  static $_TSPEC;


  public function __construct() {
    if (!isset(self::$_TSPEC)) {
      self::$_TSPEC = array(
        );
    }
  }

  public function getName() {
    return 'FacebookService_getOptions_args';
  }

  public function read($input)
  {
    return $this->_read('FacebookService_getOptions_args', self::$_TSPEC, $input);
  }
  public function write($output) {
    return $this->_write('FacebookService_getOptions_args', self::$_TSPEC, $output);
  }
}

class FacebookService_getOptions_result extends TBase {
  static $_TSPEC;

  public $success = null;

  public function __construct($vals=null) {
    if (!isset(self::$_TSPEC)) {
      self::$_TSPEC = array(
        0 => array(
          'var' => 'success',
          'type' => TType::MAP,
          'ktype' => TType::STRING,
          'vtype' => TType::STRING,
          'key' => array(
            'type' => TType::STRING,
          ),
          'val' => array(
            'type' => TType::STRING,
            ),
          ),
        );
    }
    if (is_array($vals)) {
      parent::__construct(self::$_TSPEC, $vals);
    }
  }

  public function getName() {
    return 'FacebookService_getOptions_result';
  }

  public function read($input)
  {
    return $this->_read('FacebookService_getOptions_result', self::$_TSPEC, $input);
  }
  public function write($output) {
    return $this->_write('FacebookService_getOptions_result', self::$_TSPEC, $output);
  }
}

class FacebookService_getCpuProfile_args extends TBase {
  static $_TSPEC;

  public $profileDurationInSec = null;

  public function __construct($vals=null) {
    if (!isset(self::$_TSPEC)) {
      self::$_TSPEC = array(
        1 => array(
          'var' => 'profileDurationInSec',
          'type' => TType::I32,
          ),
        );
    }
    if (is_array($vals)) {
      parent::__construct(self::$_TSPEC, $vals);
    }
  }

  public function getName() {
    return 'FacebookService_getCpuProfile_args';
  }

  public function read($input)
  {
    return $this->_read('FacebookService_getCpuProfile_args', self::$_TSPEC, $input);
  }
  public function write($output) {
    return $this->_write('FacebookService_getCpuProfile_args', self::$_TSPEC, $output);
  }
}

class FacebookService_getCpuProfile_result extends TBase {
  static $_TSPEC;

  public $success = null;

  public function __construct($vals=null) {
    if (!isset(self::$_TSPEC)) {
      self::$_TSPEC = array(
        0 => array(
          'var' => 'success',
          'type' => TType::STRING,
          ),
        );
    }
    if (is_array($vals)) {
      parent::__construct(self::$_TSPEC, $vals);
    }
  }

  public function getName() {
    return 'FacebookService_getCpuProfile_result';
  }

  public function read($input)
  {
    return $this->_read('FacebookService_getCpuProfile_result', self::$_TSPEC, $input);
  }
  public function write($output) {
    return $this->_write('FacebookService_getCpuProfile_result', self::$_TSPEC, $output);
  }
}

class FacebookService_aliveSince_args extends TBase {
  static $_TSPEC;


  public function __construct() {
    if (!isset(self::$_TSPEC)) {
      self::$_TSPEC = array(
        );
    }
  }

  public function getName() {
    return 'FacebookService_aliveSince_args';
  }

  public function read($input)
  {
    return $this->_read('FacebookService_aliveSince_args', self::$_TSPEC, $input);
  }
  public function write($output) {
    return $this->_write('FacebookService_aliveSince_args', self::$_TSPEC, $output);
  }
}

class FacebookService_aliveSince_result extends TBase {
  static $_TSPEC;

  public $success = null;

  public function __construct($vals=null) {
    if (!isset(self::$_TSPEC)) {
      self::$_TSPEC = array(
        0 => array(
          'var' => 'success',
          'type' => TType::I64,
          ),
        );
    }
    if (is_array($vals)) {
      parent::__construct(self::$_TSPEC, $vals);
    }
  }

  public function getName() {
    return 'FacebookService_aliveSince_result';
  }

  public function read($input)
  {
    return $this->_read('FacebookService_aliveSince_result', self::$_TSPEC, $input);
  }
  public function write($output) {
    return $this->_write('FacebookService_aliveSince_result', self::$_TSPEC, $output);
  }
}

class FacebookService_reinitialize_args extends TBase {
  static $_TSPEC;


  public function __construct() {
    if (!isset(self::$_TSPEC)) {
      self::$_TSPEC = array(
        );
    }
  }

  public function getName() {
    return 'FacebookService_reinitialize_args';
  }

  public function read($input)
  {
    return $this->_read('FacebookService_reinitialize_args', self::$_TSPEC, $input);
  }
  public function write($output) {
    return $this->_write('FacebookService_reinitialize_args', self::$_TSPEC, $output);
  }
}

class FacebookService_shutdown_args extends TBase {
  static $_TSPEC;


  public function __construct() {
    if (!isset(self::$_TSPEC)) {
      self::$_TSPEC = array(
        );
    }
  }

  public function getName() {
    return 'FacebookService_shutdown_args';
  }

  public function read($input)
  {
    return $this->_read('FacebookService_shutdown_args', self::$_TSPEC, $input);
  }
  public function write($output) {
    return $this->_write('FacebookService_shutdown_args', self::$_TSPEC, $output);
  }
}

?>
