<?php
require_once './lib/Logger.php';

class MultiDB {
    public function __construct() {
        $this -> logger = Logger::get();
    }

    public function set_multi_db() {
        /* INSERT T_Device_Info  TABLE INFO */
        $real_db_info = $GLOBALS['DB_ARRAY'];
        //var_dump($real_db_info);
        $temp_host = $GLOBALS['DB_HOST'];
        // SAVE BASIC HOST
        $temp_name = $GLOBALS['DB_NAME'];
        // SAVE BASIC HOST
        $temp_pass = $GLOBALS['DB_PASS'];
        // SAVE BASIC HOST
        $i = 0;

        foreach ($real_db_info as $re) {
            $i++;
            if ($i > 10) {
                break;
            }
            switch($i) {
                case 1 :
                    $GLOBALS['DB_HOST'] = $re['DB_HOST'];
                    $GLOBALS['DB_NAME'] = $re['DB_NAME'];
                    $GLOBALS['DB_PASS'] = $re['DB_PASS'];
                    $db1 = new DB();
                    $db_obj[11] = $db1;
                    break;
                case 2 :
                    $GLOBALS['DB_HOST'] = $re['DB_HOST'];
                    $GLOBALS['DB_NAME'] = $re['DB_NAME'];
                    $GLOBALS['DB_PASS'] = $re['DB_PASS'];
                    $db2 = new DB();
                    $db_obj[12] = $db2;
                    break;
                case 3 :
                    $GLOBALS['DB_HOST'] = $re['DB_HOST'];
                    $GLOBALS['DB_NAME'] = $re['DB_NAME'];
                    $GLOBALS['DB_PASS'] = $re['DB_PASS'];
                    $db3 = new DB();
                    $db_obj[13] = $db3;
                    break;
                case 4 :
                    $GLOBALS['DB_HOST'] = $re['DB_HOST'];
                    $GLOBALS['DB_NAME'] = $re['DB_NAME'];
                    $GLOBALS['DB_PASS'] = $re['DB_PASS'];
                    $db4 = new DB();
                    $db_obj[14] = $db4;
                    break;

                case 5 :
                    $GLOBALS['DB_HOST'] = $re['DB_HOST'];
                    $GLOBALS['DB_NAME'] = $re['DB_NAME'];
                    $GLOBALS['DB_PASS'] = $re['DB_PASS'];
                    $db5 = new DB();
                    $db_obj[15] = $db5;
                    break;
                /*
                 case 6 :
                 $GLOBALS['DB_HOST'] = $re['DB_HOST'];
                 $GLOBALS['DB_NAME'] = $re['DB_NAME'];
                 $GLOBALS['DB_PASS'] = $re['DB_PASS'];
                 $db6 = new DB();
                 $db_obj[16] = $db6;
                 break;

                 case 7 :
                 $GLOBALS['DB_HOST'] = $re['DB_HOST'];
                 $GLOBALS['DB_NAME'] = $re['DB_NAME'];
                 $GLOBALS['DB_PASS'] = $re['DB_PASS'];
                 $db7 = new DB();
                 $db_obj[17] = $db7;
                 break;

                 case 8 :
                 $GLOBALS['DB_HOST'] = $re['DB_HOST'];
                 $GLOBALS['DB_NAME'] = $re['DB_NAME'];
                 $GLOBALS['DB_PASS'] = $re['DB_PASS'];
                 $db8 = new DB();
                 $db_obj[18] = $db8;
                 break;

                 case 9 :
                 $GLOBALS['DB_HOST'] = $re['DB_HOST'];
                 $GLOBALS['DB_NAME'] = $re['DB_NAME'];
                 $GLOBALS['DB_PASS'] = $re['DB_PASS'];
                 $db9 = new DB();
                 $db_obj[19] = $db9;
                 break;

                 case 10 :
                 $GLOBALS['DB_HOST'] = $re['DB_HOST'];
                 $GLOBALS['DB_NAME'] = $re['DB_NAME'];
                 $GLOBALS['DB_PASS'] = $re['DB_PASS'];
                 $db10 = new DB();
                 $db_obj[20] = $db10;
                 break;
                 */

                default :
                    break;
            }
        }

        // roll back base DB Info
        $GLOBALS['DB_HOST'] = $temp_host;
        $GLOBALS['DB_NAME'] = $temp_name;
        $GLOBALS['DB_PASS'] = $temp_pass;

        return $db_obj;
    }

    public function set_single_db($AccessNo) {
        $real_db_info = $GLOBALS['DB_ARRAY'];
        $re = $real_db_info[$AccessNo - 11];

        $db_single = null;

        // SAVE MY DB INFO
        $temp_host = $GLOBALS['DB_HOST'];
        $temp_name = $GLOBALS['DB_NAME'];
        $temp_pass = $GLOBALS['DB_PASS'];

        $GLOBALS['DB_HOST'] = $re['DB_HOST'];
        $GLOBALS['DB_NAME'] = $re['DB_NAME'];
        $GLOBALS['DB_PASS'] = $re['DB_PASS'];
        $db_s = new DB();
        $db_single[$AccessNo] = $db_s;

        // ROLLBACK CONFIG DB INFO
        $GLOBALS['DB_HOST'] = $temp_host;
        $GLOBALS['DB_NAME'] = $temp_name;
        $GLOBALS['DB_PASS'] = $temp_pass;

        return $db_single;
    }

}
?>

