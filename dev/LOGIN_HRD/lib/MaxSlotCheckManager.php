<?php
require_once './common/DB.php';
require_once './lib/Logger.php';

class MaxSlotCheckManager {

    public function __construct() {
        $this -> logger = Logger::get();
    }

    function check_item_summon($db, $UID) {
        $resultFail['ResultCode'] = 300;

		//hansw costume slot add
        $sql = <<<SQL
        SELECT inven_item_slot_size, inven_summon_slot_size, inven_costume_slot_size
        FROM Inven_Info
        WHERE UID = :UID
SQL;

        $db -> prepare($sql);
        $db -> bindValue(':UID', $UID, PDO::PARAM_INT);
        $db -> execute();
        $row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
        if (!isset($row) || is_null($row) || $row == 0) {
            $this -> logger -> logError($GLOBALS['AccessNo'] . ', MaxSlotCheck::check_item_summon SELECT1 FAIL UID.' . $UID . ', sql : ' . $sql);
            return $resultFail;
        }

        $inven_item_slot_size = $row['inven_item_slot_size'];
        $inven_summon_slot_size = $row['inven_summon_slot_size'];
        $inven_costume_slot_size = $row['inven_costume_slot_size'];

        $sql = <<<SQL
        SELECT count(*) as count_item
        FROM Inven_Item_All
        WHERE UID = :UID and item_equip <= 0 and item_type != 303 and item_type != 304
SQL;

        $db -> prepare($sql);
        $db -> bindValue(':UID', $UID, PDO::PARAM_INT);
        $db -> execute();
        $row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
        if (!isset($row) || is_null($row) || $row == 0) {
            $this -> logger -> logError($GLOBALS['AccessNo'] . ', MaxSlotCheck::check_item_summon SELECT2 FAIL UID.' . $UID . ', sql : ' . $sql);
            return $resultFail;
        }

        $count_item = $row['count_item'];

        $sql = <<<SQL
        SELECT count(*) as count_summon
        FROM Inven_Summon
        WHERE UID = :UID and summon_status != 2
SQL;

        $db -> prepare($sql);
        $db -> bindValue(':UID', $UID, PDO::PARAM_INT);
        $db -> execute();
        $row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
        if (!isset($row) || is_null($row) || $row == 0) {
            $this -> logger -> logError($GLOBALS['AccessNo'] . ', MaxSlotCheck::check_item_summon SELECT3 FAIL UID.' . $UID . ', sql : ' . $sql);
            return $resultFail;
        }

        $count_summon = $row['count_summon'];

		//hansw costume slot add
        $sql = <<<SQL
        SELECT count(*) as count_costume
        FROM Inven_Item_All
        WHERE UID = :UID and item_equip <= 0 and item_type in(303, 304)
SQL;
        $db -> prepare($sql);
        $db -> bindValue(':UID', $UID, PDO::PARAM_INT);
        $db -> execute();
        $row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
        if (!isset($row) || is_null($row) || $row == 0) {
            $this -> logger -> logError($GLOBALS['AccessNo'] . ', MaxSlotCheck::check_item_summon SELECT4 FAIL UID.' . $UID . ', sql : ' . $sql);
            return $resultFail;
        }
        $count_costume = $row['count_costume'];

        if ($inven_summon_slot_size <= $count_summon or $inven_item_slot_size <= $count_item or $inven_costume_slot_size <= $count_costume) {

            // 보유한 inven 수량 max
            $result['ResultCode'] = 381;

        } else {

            $result['ResultCode'] = 100;

        }

        return $result;
    }

    function check_item($db, $UID) {
        $resultFail['ResultCode'] = 300;

		//hansw costume slot add
        $sql = <<<SQL
        SELECT inven_item_slot_size, inven_costume_slot_size
        FROM Inven_Info
        WHERE UID = :UID
SQL;

        $db -> prepare($sql);
        $db -> bindValue(':UID', $UID, PDO::PARAM_INT);
        $db -> execute();
        $row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
        if (!isset($row) || is_null($row) || $row == 0) {
            $this -> logger -> logError($GLOBALS['AccessNo'] . ', MaxSlotCheck::check_item SELECT1 FAIL UID.' . $UID . ', sql : ' . $sql);
            return $resultFail;
        }

        $inven_item_slot_size = $row['inven_item_slot_size'];
        $inven_costume_slot_size = $row['inven_costume_slot_size'];

        $sql = <<<SQL
        SELECT count(*) as count_item
        FROM Inven_Item_All
        WHERE UID = :UID and item_equip <= 0 and item_type != 303 and item_type != 304
SQL;

        $db -> prepare($sql);
        $db -> bindValue(':UID', $UID, PDO::PARAM_INT);
        $db -> execute();
        $row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
        if (!isset($row) || is_null($row) || $row == 0) {
            $this -> logger -> logError($GLOBALS['AccessNo'] . ', MaxSlotCheck::check_item SELECT2 FAIL UID.' . $UID . ', sql : ' . $sql);
            return $resultFail;
        }

        $count_item = $row['count_item'];

		//hansw costume slot add
        $sql = <<<SQL
        SELECT count(*) as count_costume
        FROM Inven_Item_All
        WHERE UID = :UID and item_equip <= 0 and item_type in(303, 304)
SQL;

        $db -> prepare($sql);
        $db -> bindValue(':UID', $UID, PDO::PARAM_INT);
        $db -> execute();
        $row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
        if (!isset($row) || is_null($row) || $row == 0) {
            $this -> logger -> logError($GLOBALS['AccessNo'] . ', MaxSlotCheck::check_item SELECT2 FAIL UID.' . $UID . ', sql : ' . $sql);
            return $resultFail;
        }

        $count_costume = $row['count_costume'];

        if ($inven_item_slot_size <= $count_item or $inven_costume_slot_size <= $count_costume) {

            // 보유한 inven 수량 max
            $result['ResultCode'] = 381;

        } else {

            $result['ResultCode'] = 100;

        }

        return $result;
    }

    function check_summon($db, $UID) {
        $resultFail['ResultCode'] = 300;

        $sql = <<<SQL
        SELECT inven_summon_slot_size
        FROM Inven_Info
        WHERE UID = :UID
SQL;

        $db -> prepare($sql);
        $db -> bindValue(':UID', $UID, PDO::PARAM_INT);
        $db -> execute();
        $row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
        if (!isset($row) || is_null($row) || $row == 0) {
            $this -> logger -> logError($GLOBALS['AccessNo'] . ', MaxSlotCheck::check_summon SELECT1 FAIL UID.' . $UID . ', sql : ' . $sql);
            return $resultFail;
        }

        $inven_summon_slot_size = $row['inven_summon_slot_size'];

        $sql = <<<SQL
        SELECT count(*) as count_summon
        FROM Inven_Summon
        WHERE UID = :UID and summon_status != 2
SQL;

        $db -> prepare($sql);
        $db -> bindValue(':UID', $UID, PDO::PARAM_INT);
        $db -> execute();
        $row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
        if (!isset($row) || is_null($row) || $row == 0) {
            $this -> logger -> logError($GLOBALS['AccessNo'] . ', MaxSlotCheck::check_summon SELECT3 FAIL UID.' . $UID . ', sql : ' . $sql);
            return $resultFail;
        }

        $count_summon = $row['count_summon'];

        if ($inven_summon_slot_size <= $count_summon) {

            // 보유한 inven 수량 max
            $result['ResultCode'] = 381;

        } else {

            $result['ResultCode'] = 100;

        }

        return $result;
    }

}
?>
