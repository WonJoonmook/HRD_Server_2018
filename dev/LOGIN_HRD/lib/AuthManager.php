<?php
require_once './common/Config.php';
require_once './common/DB.php';
require_once './lib/Logger.php';

class AuthManager {

    public function __construct() {
        $this -> logger = Logger::get();
        //$this->
    }

    function sdk_verify($getPostData) {
        $resultFail['ResultCode'] = 300;

        //$playerId = "AA79CBF9C97E43ACBBC864E4EA603103";

        //$gameToken = '0AB0016557B58180667952B188485AE5CD306CCB4C84CC3AE17B5EC56AAECFE1EC34810D50262F9D36DBAE13AF4227DA6EDFC2551949F5707ED8A1696A17ACF29039B4FE9BEB7A5534ECCA6BA118D4D4AB38370B997C0664FB025CB1D75700A1FE10376C9D64082862462898C2CB87FCF75FE8AB32EAE710F5E5D34574457A46CE2EA50D5947FF62B5CC498828BBC15B35DBC868E48215D84F9041928EC49FAB9C3098E76986F71731728D8F069BD271BA76C21220FEB19BEA3B07E5D762FA615DC575934196D3EE6D280435DBF154B19162651A70';

        $gameCode = 'seedofwar';

        // dev
        $IAPServer = 'https://cpdev-mobileauth.netmarble.net:1443/v2/verify';

        // real
        //$IAPServer = 'https://mobileauth.netmarble.net:1443/v2verify';

        $PlayerID = trim($getPostData['PlayerID']);

        $gameToken = trim($getPostData['token']);

        $post_data = http_build_query(array('playerId' => $PlayerID, 'gameToken' => $gameToken, 'gameCode' => $gameCode));

        // send  IAP SERVER
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
        curl_setopt($ch, CURLOPT_URL, $IAPServer);

        //POST
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // TIME OUT SECOND
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);

        $response = curl_exec($ch);
        $http_result_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        // CURL SUCEESS == 200
        // 100 CONTINUE
        if ($http_result_code != 200) {
            $this -> logger -> logError(', AuthManager::sdk_verify : IAP SERVER CURL CONNECTION ERROR PlayerID : ' . $PlayerID);
            return $resultFail;
        }

        // JSON RETURN CONVERT
        $IAP = json_decode($response, TRUE);

        if (isset($IAP['resultCode']) and $IAP['resultCode'] != 200) {
            $this -> logger -> logError(', AuthManager::sdk_verify : resultCode != 200 PlayerID : ' . $PlayerID);
            return $resultFail;
        }

        var_dump($IAP);

        //$teeee = json_encode($IAP);

        //var_dump($teeee);

        $iap_secretKey = null;

        $iap_aesInitVec = null;

        $iap_playerId = $IAP['verificationData']['gameTokenInfo']['playerId'];

        for ($i = 0; $i < count($IAP['verificationData']['gameTokenInfo']['cipherKeyList']['keyInfos']); $i++) {

            if ($IAP['verificationData']['gameTokenInfo']['cipherKeyList']['keyInfos'][$i]['cipherType'] == 'CIPHER_AES_128_CBC') {

                $iap_secretKey = $IAP['verificationData']['gameTokenInfo']['cipherKeyList']['keyInfos'][$i]['secretKey'];

                $iap_aesInitVec = $IAP['verificationData']['gameTokenInfo']['cipherKeyList']['keyInfos'][$i]['aesInitVec'];
                break;
            }

        }

        if ($iap_playerId == null or $iap_secretKey == null or $iap_aesInitVec == null) {
            $this -> logger -> logError(', AuthManager::sdk_verify : $iap_playerId == null or $iap_secretKey == null PlayerID : ' . $PlayerID);
            return $resultFail;
        }

        if ($PlayerID != $iap_playerId) {
            $this -> logger -> logError(', AuthManager::sdk_verify : $playerId != $iap_playerId PlayerID : ' . $PlayerID);
            return $resultFail;
        }

        switch($GLOBALS['REAL']) {

            case 0 :
                $GLOBALS['AccessNo'] = sprintf("%u", intval(crc32($PlayerID), 16) % 2) + 11;
                break;

            case 1 :
            case 2 :
            case 3 :
                $GLOBALS['AccessNo'] = sprintf("%u", intval(crc32($PlayerID), 16) % 1) + 11;
                break;

            case 4 :
                $GLOBALS['AccessNo'] = sprintf("%u", intval(crc32($PlayerID), 16) % 5) + 11;
                break;

                break;

            default :
                $this -> logger -> logError($GLOBALS['AccessNo'] . ', AuthManager::sdk_verify : switch FAIL $GLOBALS[REAL] : ' . $GLOBALS['REAL']);
                return $resultFail;
                break;
        }

        include_once './common/DB_Info.php';

        $db = new DB();

        ########################
        /*
         $sql = <<<SQL
         SELECT UID
         FROM User_Regist
         WHERE PlayerID = :PlayerID
         SQL;

         $db -> prepare($sql);
         $db -> bindValue(':PlayerID', $playerId, PDO::PARAM_INT);
         $db -> execute();
         $row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
         if (!isset($row) || is_null($row) || $row == 0) {
         $this -> logger -> logError($GLOBALS['AccessNo'] . ', AuthManager::sdk_verify : Qeury SELECT  FAIL $playerId : ' . $playerId . ", sql : " . $sql);
         return $resultFail;
         }

         $UID = $row['UID'];

         $AccessNo_UID = trim($GLOBALS['AccessNo'] . $row['UID']);
         */
        ########################

        include_once './lib/MemManager.php';

        $MemManager = new MemManager();
        //$MemManager-> __flush();

        $mem_key = trim("KEY_AUTH_" . $PlayerID);

        $temp = null;
        $temp['secretKey'] = $iap_secretKey;
        $temp['aesInitVec'] = $iap_aesInitVec;

        $MemManager -> set_expire($mem_key, $temp, 43200);

        $check_insert = $this -> update_auth_key($db, $PlayerID, $iap_secretKey, $iap_aesInitVec);
        if ($check_insert['ResultCode'] != 100) {
            $this -> logger -> logError($GLOBALS['AccessNo'] . ', AuthManager::sdk_verify : update_auth_key FAIL $UID : ' . $UID);
            return $resultFail;
        }

        $result['ResultCode'] = 100;
        $result['secretKey'] = $iap_secretKey;
        $result['aesInitVec'] = $iap_aesInitVec;
        //$result['UID'] = $AccessNo_UID;

        return $result;
    }

    public function AES_128_decrypt($getPostData, $key) {

        //var_dump($getPostData);exit;

        # --- DECRYPTION ---

        $key = pack('H*', $key);

        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);

        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);

        $ciphertext_dec = base64_decode($getPostData);

        # retrieves the IV, iv_size should be created using mcrypt_get_iv_size()
        $iv_dec = substr($ciphertext_dec, 0, $iv_size);

        # retrieves the cipher text (everything except the $iv_size in the front)
        $ciphertext_dec = substr($ciphertext_dec, $iv_size);

        # may remove 00h valued characters from end of plain text
        $plaintext_dec = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $ciphertext_dec, MCRYPT_MODE_CBC, $iv_dec);

        return trim($plaintext_dec);

        //echo $plaintext_dec . "\n";
        //exit ;

    }

    public function AES_128_encrypt($getPostData, $key) {
        # --- ENCRYPTION ---

        # the key should be random binary, use scrypt, bcrypt or PBKDF2 to
        # convert a string into a key
        # key is specified using hexadecimal
        //$key = pack('H*', "bcb04b7e103a0cd8b54763051cef08bc55abe029fdebae5e1d417e2ffb2a00a3");
        $key = pack('H*', $key);

        # show key size use either 16, 24 or 32 byte keys for AES-128, 192
        # and 256 respectively
        //$key_size = strlen($key);
        //echo "Key size: " . $key_size . "\n";

        //$plaintext = "This string was AES-256 / CBC / ZeroBytePadding encrypted.";

        # create a random IV to use with CBC encoding
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);

        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);

        # creates a cipher text compatible with AES (Rijndael block size = 128)
        # to keep the text confidential
        # only suitable for encoded input that never ends with value 00h
        # (because of default zero padding)
        $ciphertext = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $getPostData, MCRYPT_MODE_CBC, $iv);

        # prepend the IV for it to be available for decryption
        $ciphertext = $iv . $ciphertext;

        # encode the resulting cipher text so it can be represented by a string
        $ciphertext_base64 = base64_encode($ciphertext);

        return trim($ciphertext_base64);

        # === WARNING ===

        # Resulting cipher text has no integrity or authenticity added
        # and is not protected against padding oracle attacks.

    }

    function update_auth_key($db, $PlayerID, $iap_secretKey, $iap_aesInitVec) {
        $resultFail['ResultCode'] = 300;

        $sql = <<<SQL
         SELECT secretKey
         FROM Auth_info
         WHERE PlayerID = :PlayerID
SQL;

        $db -> prepare($sql);
        $db -> bindValue(':PlayerID', $PlayerID, PDO::PARAM_INT);
        $db -> execute();
        $row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
        if (!isset($row) || is_null($row) || $row == 0) {

            $sql = <<<SQL
        INSERT INTO Auth_info
        (PlayerID, secretKey, aesInitVec, update_date, reg_date)
        VALUES (:PlayerID, :secretKey, :aesInitVec, now(), now())
SQL;
            $db -> prepare($sql);
            $db -> bindValue(':PlayerID', $PlayerID, PDO::PARAM_INT);
            $db -> bindValue(':secretKey', $iap_secretKey, PDO::PARAM_INT);
            $db -> bindValue(':aesInitVec', $iap_aesInitVec, PDO::PARAM_INT);
            $row = $db -> execute();
            if (!isset($row) || is_null($row) || $row == 0) {
                $this -> logger -> logError($GLOBALS['AccessNo'] . ', AuthManager::update_auth_key : Qeury INSERT  FAIL $playerId : ' . $playerId . ', sql : ' . $sql);
                return $resultFail;
            }

        } else {

            $sql = <<<SQL
            UPDATE Auth_info SET 
            secretKey = :secretKey, aesInitVec = :aesInitVec, update_date = now()
            WHERE PlayerID = :PlayerID
SQL;

            $db -> prepare($sql);
            $db -> bindValue(':PlayerID', $PlayerID, PDO::PARAM_INT);
            $db -> bindValue(':secretKey', $iap_secretKey, PDO::PARAM_INT);
            $db -> bindValue(':aesInitVec', $iap_aesInitVec, PDO::PARAM_INT);
            $row = $db -> execute();
            if (!isset($row) || is_null($row) || $row == 0) {
                $this -> logger -> logError($GLOBALS['AccessNo'] . ', AuthManager::update_auth_key Qeury UPDATE  FAIL $playerId : ' . $playerId . ", sql : " . $sql);
                return $resultFail;
            }

        }

        $result['ResultCode'] = 100;

        return $result;
    }

    function get_secretKey_from_db($PlayerID) {
        $resultFail['ResultCode'] = 300;
        /*
         include_once './lib/ConvertManager.php';

         $ConvertManager = new ConvertManager();

         $ConvertResult = $ConvertManager -> convert_ID($AccessNo_UID);

         $GLOBALS['AccessNo'] = $ConvertResult['SERVER_ID'];

         $UID = $ConvertResult['ID'];
         */
        switch($GLOBALS['REAL']) {

            case 0 :
                $GLOBALS['AccessNo'] = sprintf("%u", intval(crc32($PlayerID), 16) % 2) + 11;
                break;

            case 1 :
            case 2 :
            case 3 :
                $GLOBALS['AccessNo'] = sprintf("%u", intval(crc32($PlayerID), 16) % 1) + 11;
                break;

            case 4 :
                $GLOBALS['AccessNo'] = sprintf("%u", intval(crc32($PlayerID), 16) % 5) + 11;
                break;

                break;

            default :
                $this -> logger -> logError(', AuthManager::get_secretKey_from_db : switch FAIL $GLOBALS[REAL] : ' . $GLOBALS['REAL']);
                return $resultFail;
                break;
        }

        include_once './common/DB_Info.php';

        $db = new DB();

        $sql = <<<SQL
         SELECT secretKey, aesInitVec
         FROM Auth_info
         WHERE PlayerID = :PlayerID
SQL;

        $db -> prepare($sql);
        $db -> bindValue(':PlayerID', $PlayerID, PDO::PARAM_INT);
        $db -> execute();
        $row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
        if (!isset($row) || is_null($row) || $row == 0) {
            $this -> logger -> logError($GLOBALS['AccessNo'] . ', AuthManager::get_secretKey_from_db select FAIL $PlayerID : ' . $PlayerID . ", sql : " . $sql);
            return $resultFail;
        }

        $result['ResultCode'] = 100;
        $result['secretKey'] = $row['secretKey'];
        $result['aesInitVec'] = $row['aesInitVec'];

        return $result;
    }

}
?>
