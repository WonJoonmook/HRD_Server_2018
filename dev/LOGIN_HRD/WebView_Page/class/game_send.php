<?php
include_once "../common/Config.php";

$userId = $_GET['userId'];
$send_array = null;
$send_array['Protocol'] = 'ReqUserReceive';
$send_array['Sess'] = 'server';
$send_array['Ver'] = '1.0.0';
$send_array['Type'] = 'A';
$send_array['Data']['userId'] = $userId;

if ($GLOBALS['REAL'] == 5) {
	$URL = "14.63.196.82:3081/WebView_Server/gateway.php";
} else {
	$URL = "14.63.196.82:1081/WebView_Server/gateway.php";
}



$send_json = json_encode($send_array);
// var_dump($send_json);

$header = array('Content-Type:application/json', 'charset:utf-8');

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $URL);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $send_json);
curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

$recv_json = curl_exec($ch);

curl_close($ch);

// sending data write log
$recv_data = array();

$result = json_decode($recv_json, TRUE);
switch($result['ResultCode']) {
	case 100:
		echo "<script>alert(\"지급되었습니다. 우편함을 확인 해주세요~\");
		document.location.href='http://base.hnjgames.xyz:3081/WebView_Page/main.php?userId=".$userId."';
		</script>";	
		break;
	case 101:
		echo "<script>alert(\"아직 획득한 아이템이 없어요 ㅠㅠ\");	
		document.location.href='http://base.hnjgames.xyz:3081/WebView_Page/main.php?userId=".$userId."';
			</script>";	
		break;
	case 300:
		echo "<script>alert(\"서버와의 통신이 원활하지 않습니다.\");	
		document.location.href='http://base.hnjgames.xyz:3081/WebView_Page/main.php?userId=".$userId."';
			</script>";	
		break;
	case 303:
		echo "<script>alert(\"수량이 부족하거나, 이미 지급되었습니다.\");	
		document.location.href='http://base.hnjgames.xyz:3081/WebView_Page/main.php?userId=".$userId."';
			</script>";	

		break;
	default:
		echo "<script>alert(\"서버로부터 응답이 없습니다. 다시 시도해 주세요.\");
		document.location.href='http://base.hnjgames.xyz:3081/WebView_Page/main.php?userId=".$userId."';
			</script>";	
		break;
}
/*
$link = 'http://14.49.38.192:6351/WebView/main.php?userId='.$userId;
header("Location: http://14.49.38.192:6351/WebView/main.php?userId=".$userId);
exit;
$link = 'http://14.49.38.192:6351/WebView/main.php?userId='.$userId;
goto_url($link);
*/
?>

