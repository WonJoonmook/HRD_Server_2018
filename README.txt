- 각 프로젝트의 설명
	개략적인 설명이면 될것 같습니다. ( 클라이언트(구글인증유무등등), 서버(결제플랫폼의 유무 또는 어느 플랫폼의 인증인지 등등) )
	또한 개발시 환경변수 등의 정보가 있다면 알려주시기 바랍니다. 
		유니티 프로젝트의 경우 Assets/README 에 개략적인 설명을 기재하였습니다.
		서버의 경우 최상위 폴더 내 README 파일에 기재하였습니다.
 
- 최신버전의 소스를 지정
	현재 중국시장의 출시를 위해 개발하셨던 최종버전의 소스코드를 지정하여 주시기 바랍니다.
		HRD161020 프로젝트 내의 HRD5_NewServer가 한국, 중국 모두의 최종버전의 클라이언트입니다.
		중국용은 Assets/_ManagerProject/SwitchProject에서 Project를 Android_YK로 변경하시면 됩니다. 

- 서버를 운용하기 위해 필요한 기초데이터가 있다면 해당 정보를 알려주시기 바랍니다. 
	개략적인 설명은 서버 프로젝트 REAME를 참고해주시기 바라며,
	막히는 부분이 있을 시 게임 클라이언트에서 Symbol define ( Player Settings -> Other Settings -> Scripting Define Symbols)에서 
	Test; 
	입력 시 Console창에서 서버의 어느 php와 프로토콜을 주고받는지 확인이 가능합니다.

- 개발하실때 개발툴이나 서드파티등의 정보도 함께 간략히 정리하여 주시면 소스파악에 한층 도움이 될것 같습니다. 
	개발툴은 유니티를 사용하였으며, 
	서드파티로는 
		자체 HNJPlugins(안드로이드 구글 결제, 로컬푸쉬),
		NGUI,
		Google Play Game Service(구글 연동 로그인),
		TapJoy(간단한 기획 로그 적재)
		를 사용하였습니다.




모든 소스는 /dev 폴더 내에 존재합니다

/dev/GAME_HRD
게임서버 관련 php파일들을 담고 있습니다
	/bin
	국내 서버 내 메모리 데이터는 웹상에서 다음과 같은 주소로 들어가면 세팅됩니다.
	14.63.196.82:3080/bin/call_set_apc.php ( 게임서버 A )
	14.63.196.82:4080/bin/call_set_apc.php ( 게임서버 B )
	( dev/GAME_HRD/bin/call_set_apc.php가 실행됨 )
	php apc함수를 이용하여 메모리데이터를 설정하였습니다.

	/classes
	게임 클라이언트와 주고받는 프로토콜들이 담겨있습니다.

	/common
	환경변수, 설정값들이 담겨있습니다.

	/data
	기획데이터, 스테이지 데이터등이 담겨있습니다.

/dev/LOGIN_HRD
로그인서버 관련 php들을 담고 있습니다
	/AsBd_Chinese
	게임 클라이언트에서 이 폴더 안의 데이터들을 다운로드받습니다.
	최소화 하여 
		로비 배경 ( 할로윈 시, 겨울 일 시 등 클라이언트 업데이트 없이 배경을 바꾸기 위함 )
		인앱상품 이미지, 텍스트 정보
		이벤트용 프리팹 ( 눈 오는 파티클 등 )
		Localization파일
		등을 담고 있습니다.

	/classes
	게임 클라이언트와 주고받는 프로토콜들이 담겨있습니다.

	/common
	환경변수, 설정값들이 담겨있습니다.	


/dev/QAWeb_JM
QA웹페이지 관련 php들을 담고 있습니다.
웹상에서 다음과 같은 주소에 한국 QA용 웹페이지가 있습니다.
http://14.63.196.82:100/index.php
(회원가입시 키값 6841)
이안에서 이벤트와 간단한 QA업무, 강림일정 추가등을 진행합니다.
QA웹페이지는 한국의 경우 1000번대 서버(QA서버)에서 진행합니다.


dev/LOGIN_HRD/lib/GoogleAuth.php에서 구글계정 인증을 진행합니다.
결제 또한 Google의 결제플랫폼을 사용합니다.
환경변수 값은 각 GAME or LOGIN_HRD/common 폴더 내에 위치합니다.


LOGIN_HRD/notice0,1,2.txt 를 수정 시 게임 클라이언트 내 로비화면에서 공지 텍스트가 뜨게 됩니다.


